Nombre y apellidos					Dni		Ventas Nac.		Ventas Inter.		Sueldo		Objetivos cumplidos
Juan Carlos Avellanez Galindez                    30999888L      	  3000,00		  6000,00		  1650,00		false
Ana García Martínez                               20887777K      	  2500,00		 14000,00		  3175,00		true
Pedro Galgez Rodríguez                            40776666M      	   500,00		 15500,00		  3175,00		true
María Eustaquia Sánchez                           50665555N      	  3200,00		  6200,00		  1720,00		false
Luis Martínez Gómez                               60554444T      	  2800,00		  5800,00		  1580,00		false
Elena Fernández García                            70443333J      	   800,00		  5400,00		  1200,00		false
David Urraquez Martínez                           80332222R      	  3100,00		 16700,00		  3805,00		true
Sofía López Rodríguez                             91221111S      	   900,00		  6100,00		  1355,00		false
Eustaquio Rodolfo Ruiz Pérez                      81230000A      	  2700,00		 15900,00		  3585,00		true
Laura Gómez Fernández                             91229999B      	  3300,00		  6300,00		  1755,00		false
