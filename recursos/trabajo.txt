
package AlarconLopezAlbertomobiliario_;

/**
 * Clase que representa un <code>enum</code> con los módulos que se pueden añadir
 * a un mueble personalizable.
 * 
 * @author Alberto Alarcón López
 */
public enum Modulo {
    
    /**
     * Balda
     */ 
    BALDA, 
    
    /**
     * Cajón
     */
    CAJON, 
    
    /**
     * Con puerta
     */
    CON_PUERTA, 
    
    /**
     * Cesto
     */
    CESTO, 
    
    /**
     * Zapatero
     */
    ZAPATERO, 
    
    /**
     * Bandeja
     */
    BANDEJA, 
    
    /**
     * Pantalonero
     */
    PANTALONERO, 
    
    /**
     * Con barra
     */
    CON_BARRA
}
package AlarconLopezAlbertomobiliario_;

/**
 * Clase que representa el concepto abstracto de un mueble de tipo almacenaje
 * @author Alberto Alarcón López
 */

public abstract class Almacenaje extends Mueble implements Personalizable{
    
    /**
     * Numero minimo de modulos que tiene un mueble
     */
    public static final int MIN_MODULOS = 1;
    /**
     * Numero máximo de modulos que pueden añadirse a un mueble
     */
    public static final int MAX_MODULOS = 20;
    
    /**
     * Anchura en centimetros del mueble
     */
    private double anchura;
    /**
     * Altura en centimetros del mueble
     */
    private double altura;
    /**
     * Numero máximo de modulos para esta instancia
     */
    private final int maxModulos;
    /**
     * Array de tipo Modulo de los modulos añadidos al mueble
     */
    private Modulo[] modulos;
    /**
     * Numero de módulos añadidos al mueble
     */
    protected int modulosAnyadidos;
    
    /**
     * Contructor que reutiliza el de la superclase Mueble y añade anchura, altura y maximo de modulos, también crea un array de modulos con el tamaño de maxModulos
     * @param precio Precio del mueble
     * @param descripcion Descripcion del mueble
     * @param anchura Ancho del mueble
     * @param altura Alto del mueble
     * @param maxModulos Número máximo de módulos que se pueden instalar en el mueble
     * @throws IllegalArgumentException maxModulos es mayor que el numero maximo de módulos (MAX_MODULOS)
     */
    public Almacenaje(double precio, String descripcion, double anchura, double altura, int maxModulos) throws IllegalArgumentException{
        super(precio, descripcion);        
        if (maxModulos < MIN_MODULOS || maxModulos > MAX_MODULOS){
            throw new IllegalArgumentException("ERROR: No se puede crear el mueble de Almacenaje. El número de módulos no está en el rango permitido: " + maxModulos);
        }else{
            this.anchura = anchura;
            this.altura = altura;
            this.maxModulos = maxModulos;
            this.modulos = new Modulo[maxModulos];
            this.modulos[0] = Modulo.BALDA;
            this.modulosAnyadidos++;
        }
    }

    /**
     * Devuelve la anchura del mueble
     * @return (double) Ancho del mueble
     */
    public double getAnchura() {
        return anchura;
    }
    
    /**
     * Devuelve la altura del mueble
     * @return (double) Alto del mueble
     */
    public double getAltura() {
        return altura;
    }

    /**
     * Devuelve el número máximo de módulos que se pueden añadir al mueble
     * @return (int) Número máximo de módulos que se pueden añadir al mueble
     */
    public int getMaxModulos() {
        return maxModulos;
    }
    
    /**
     * Devuelve cuantos módulos se han añadido al mueble
     * @return (int) Número de módulos añadidos al mueble
     */
    public int getModulosAnyadidos() {
        return modulosAnyadidos;
    }
    
    /**
     * Devuelve cuantos módulos mas se pueden añadir al mueble
     * @return (int) Número de módulos libre en el mueble 
     */
    public int getNumModulos(){
        return modulos.length - (modulosAnyadidos - 1);
    }
    
    /**
     * Devuelve una cadena con los módulos añadidos al mueble
     * @return  (String) Módulos añadidos al mueble
     */
    @Override
    public String obtenerModulos(){
       String mod = "[";
       for (int i = 0; i < this.modulos.length; i++){
           if (i == (this.modulos.length - 1)){
               mod += this.modulos[i];
           } else {
               mod += this.modulos[i] + ", ";
           }           
       }
       mod += "]";
       return mod;       
    }
    
    /**
     * Añade el módulo pasado como parametro al array de modulos del mueble
     * @param modulo Tipo de módulo que se quiere añadir 
     * @throws IllegalStateException El mueble ya tiene su máximo de módulos posible
     * @throws NullPointerException  El módulo pasado como parametro es nulo
     */
    @Override
    public void anyadirModulo(Modulo modulo) throws IllegalStateException, NullPointerException{
        if (modulo == null){
            throw new NullPointerException("Error: el módulo a añadir no puede ser nulo");
        } else if (this.modulosAnyadidos == maxModulos){
            throw new IllegalStateException(String.format("Error: no se puede añadir el módulo %s. El número de módulos no puede superar el máximo permitido: %d", modulo.toString(), maxModulos));
        } else {
            this.modulos[modulosAnyadidos] = modulo;
            this.modulosAnyadidos++;
        }        
    }

    /**
     * Extrae el último modulo del array de modulos y lo devuelve
     * @return Módulo que se ha eliminado del array de módulos
     * @throws IllegalStateException El array de módulos ya tiene el mínimo de módulos (MIN_MODULOS) posible
     */
    @Override
    public Modulo extraerModulo() throws IllegalStateException{
        if (this.modulosAnyadidos == 1){
            throw new IllegalStateException ("Error: no se puede quitar el módulo. El número de módulos no puede ser inferior a " + MIN_MODULOS);
        }else{
            Modulo devolucion = this.modulos[modulosAnyadidos - 1];
            this.modulos[modulosAnyadidos - 1] = null;
            modulosAnyadidos--;
            return devolucion;
        }        
    }
     
    /**
     * Modificia al metodo de la superclase Mueble y le añade anchura, altura, número máximo de modulos y los módulos que tiene instalados
     * @return (String) Listado de caracteristicas 
     */
    @Override
    public String toString(){
        String cadena = super.toString() + String.format(" Anchura: %.2f Altura: %.2f Módulos máximos: %d Módulos añadidos: %s", this.anchura, this.altura, this.maxModulos, this.obtenerModulos());
        return cadena;
    }    
}
package AlarconLopezAlbertomobiliario_;

/**
 * Clase que representa el concepto abstracto de un Mueble 
 * @author Alberto Alarcón López
 */
public abstract class Mueble {    
    /**
     * Precio mínimo del producto
     */
    public static final double MIN_PRECIO = 0.01;
    
    /**
     * Precio máximo del producto
     */   
    public static final double MAX_PRECIO = 10000.00;
       
    /**
     * Identificador del mueble, es un atributo de clase, autoincrementa con cada instancia
     */
    private static int identificador = 0;
    
    /**
     * Identificador del mueble, es unico y constante
     */
    private final int id;
    /**
     * Precio del mueble
     */
    protected double precio;
    /**
     * Descripción del mueble
     */
    private final String descripcion;
    
    /**
     * Constructor de 2 parametros 
     * @param precio (double)El precio debe estar entre MIN_PRECIO y MAX_PRECIO
     * @param descripcion (double) Pequeña descripcion del Mueble
     * @throws IllegalArgumentException Si el precio se encuentra fuera de rango
     */
    public Mueble(double precio, String descripcion) throws IllegalArgumentException{        
        if (precio < MIN_PRECIO || precio > MAX_PRECIO){
            throw new IllegalArgumentException("El precio no está en el rango permitido: " +  precio);
        }else{
            identificador++;
            this.precio = precio;
            this.descripcion = descripcion;
            this.id = identificador;
        }    
    }
    
    /**
     * Devuelve el identificador único del mueble
     * @return (int) Identificador del mueble
     */
    public int getId() {
        return this.id;
    }

    /**
     * Devuelve el precio del mueble
     * @return (double) Precio del mueble
     */
    public double getPrecio() {
        return precio;
    }

    /**
     * Devuelve la descripcion del mueble
     * @return (String) Pequeña descripción del mueble
     */
    public String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Devuelve una cadena con las caracteristicas del mueble
     * @return  (String) Listado de caracteristicas 
     */
    @Override
    public String toString(){        
        String cadena = "Tipo: "+ this.getClass().getSimpleName() + " Id: " + this.id + " Precio: " + this.precio + " Descripción: " + this.descripcion;        
        return cadena; 
    }   
}
package AlarconLopezAlbertomobiliario_;

/**
 * Clase que crea instancias de mueble de del tipo Mesa
 * @author Alberto Alarcón López
 */

public final class Mesa extends Mueble {
    
    /**
     * Número mínimo de comensales
     */
    public static int MIN_COMENSALES = 4;
    /**
     * Número máximo de comensales
     */
    public static int MAX_COMENSALES = 16;
    
    /**
     * Forma de la mesa
     */
    private String forma;
    /**
     * Número de comensales que caben en la Mesa
     */
    private int comensales;
    
    /**
     * Constructor que reutiliza el de la superclase Mueble y añade el tipo de mesa
     * @param precio (double) Precio de la mesa
     * @param descripcion (String) Descripción de la mesa
     * @param forma (String) Forma de la mesa
     * @param comensales (int) Número de comensales que caben en la mesa
     * @throws IllegalArgumentException Si el número de comensales es menor o mayor que el permitido
     */  
    public Mesa(double precio, String descripcion, String forma, int comensales) throws IllegalArgumentException{
        super(precio, descripcion);
        if (comensales < MIN_COMENSALES || comensales > MAX_COMENSALES){
            throw new IllegalArgumentException("ERROR: El numero de comensales debe estar entre 4 y 16: " + comensales);
        } else {
            this.forma = forma;
            this.comensales = comensales;
        }
    }

    /**
     * Devuelve el número de comensales que caben en la mesa
     * @return (int) Número de comensales que caben en la mesa
     */
    public int getComensales() {
        return comensales;
    } 
    
    /**
     * Devuelve la forma de la mesa
     * @return (String) Forma de la mesa
     */
    public String getForma() {
        return forma;
    }   
    
    /**
     * Modificia al metodo de la superclase Mueble y le añade la forma y el número de comensales de la mesa
     * @return (String) Listado de caracteristicas
     */
    @Override
    public String toString(){
        String cadena = String.format("%s Forma: %s N.Comensales:%s", super.toString(), this.forma, this.comensales);
        return cadena;
    }
}
package AlarconLopezAlbertomobiliario_;

/**
 * Clase que crea instancias de mueble de asiento del tipo Sillón
 * @author Alberto Alarcón López
 */
public final class Sillon extends Asiento implements Ajustable{
    
    /**
     * Posición del reposapies bajado
     */
    public static final int POS_BAJADO = 0;
    /**
     * Posición del reposapies subido
     */
    public static final int POS_SUBIDO = 1;
    
    /**
     * Posición actual del reposapies
     */
    private int posicion;
    
    /**
     * Contructor que reutiliza el de la superclase Mueble y Asiento y añade el reposapies siempre en posición bajado, 
     * el numero de plazas de asiento siempre será el mínimo de la clase Asiento
     * @param precio (double) Precio del sillón 
     * @param descripcion (String) Pequeña descripción del sillón
     * @param tapiceria (String) Detalle de la tapicería del sillón
     * @param color (String) Color de la tapiciería del sillon
     */
    public Sillon(double precio, String descripcion, String tapiceria, String color){
        super(precio, descripcion, Asiento.MIN_PLAZAS, tapiceria, color);
        this.posicion = POS_BAJADO;
    }
    
    /**
     * Devuelve la posicion actual del respaldo
     * @return (int) Posición del respaldo
     */
    @Override
    public int obtenerPosicion(){
        return this.posicion;
    }
    
    /**
     * Sube el reposapies
     * @return Posición del reposapies tras el cambio
     * @throws IllegalStateException El reposapies ya esta subido
     */
    @Override
    public int subirPosicion() throws IllegalStateException{
        if (this.posicion == POS_SUBIDO){
            throw new IllegalStateException("Error: no se pueden subir los pies del sillón. Ya están subidos");
        }else{
            this.posicion = POS_SUBIDO;
        }
        return this.posicion;
    }
    
    /**
     * Baja el reposapies
     * @return Posición del reposapies tras el cambio
     * @throws IllegalStateException El reposapies ya esta bajado
     */
    @Override
    public int bajarPosicion() throws IllegalStateException{
        if (this.posicion == POS_BAJADO){
            throw new IllegalStateException("Error: no se pueden bajar los pies del sillón. Ya están bajados");
        }else{
            this.posicion = POS_BAJADO;
        }
        return this.posicion;
    }    
    
    /**
     * Modificia al metodo de la superclase Mueble y Asiento y le añade la posicion actual del reposapies
     * @return (String) Listado de caracteristicas
     */
    @Override
    public String toString(){  
        String pos;
        if (this.posicion == 0){
            pos = "bajada";
        } else {
            pos = "subida";
        }
        String cadena = super.toString() + " Posición actual de los pies: " + pos;
        return cadena;
    }
}
package AlarconLopezAlbertomobiliario_;

/**
 * Interfaz que implementa cualquier clase cuyas instancias se puedan poner en varias posiciones
 * @author Alberto Alarcón López
 */
public interface Ajustable {
    /**
     * Devuelve la posicion actual del mueble
     * @return Posicion del respaldo
     */    
    int obtenerPosicion();
    
    /**
     * Permite subir una posicion el mueble
     * @return Posicion del mueble tras la modificiación
     * @throws IllegalStateException Si la posicion ya esta en el máximo posible
     */
    int subirPosicion() throws IllegalStateException;
    
    /**
     * Permite bajar una posicion el mueble
     * @return Posicion del mueble tras la modificación
     * @throws IllegalStateException  Si la posición ya esta en el minimo posible
     */
    int bajarPosicion() throws IllegalStateException;    
}
package AlarconLopezAlbertomobiliario_;

/**
 * Clase que crea instancias de mueble de asiento del tipo Silla
 * @author Alberto Alarcón López
 */

public final class Silla extends Asiento implements Ajustable {
    
    /**
     * Minima posicion del respaldo
     */
    public static final int MIN_POSICION = 1;
    /**
     * Máxima posicion del respaldo
     */
    public static final int MAX_POSICION = 4;
    
    /**
     * Posicion actual del respaldo
     */
    private int posicion;    
    
    /**
     * Constructor que reutiliza el de la superclase Mueble y Asiento y añade respaldo siempre en posicion mínima.
     * Las plazas para los muebles de clase Silla son siempre 1
     * @param precio (Double) Precio de la silla
     * @param descripcion (String) Pequeña descripción de la silla
     * @param tapiceria (String) Detalle de la tapicería de la silla
     * @param color (String) Color de la tapicería
     */    
    public Silla(double precio, String descripcion, String tapiceria, String color){
        super(precio, descripcion, 1, tapiceria, color);
        this.posicion = MIN_POSICION;
    }
    
    /**
     * Devuelve la posicion actual del respaldo
     * @return (int) Posición del respaldo
     */
    @Override
    public int obtenerPosicion(){
        return this.posicion;
    }
    
    /**
     * Sube una posición el respaldo
     * @return Posición del respaldo tras el cambio
     * @throws IllegalStateException El respaldo ya esta en la posición máxima y por tanto no puede subir mas
     */
    @Override
    public int subirPosicion() throws IllegalStateException{
        if (this.posicion == MAX_POSICION){
            throw new IllegalStateException(String.format("Error: no se puede subir a la posición %d, ya que la posición máxima es %d", this.posicion + 1, MAX_POSICION));
        }else{
            this.posicion++;
        }
        return this.posicion;
    }
    
    /**
     * Baja una posición el respaldo
     * @return Posición del respaldo tras el cambio
     * @throws IllegalStateException El respaldo ya esta en la posición mínima y por tanto no puede bajar mas
     */
    @Override
    public int bajarPosicion() throws IllegalStateException{
        if (this.posicion == MIN_POSICION){
            throw new IllegalStateException(String.format("Error: no se puede bajar a la posición %d, ya que la posición mínima es %d", this.posicion - 1, MIN_POSICION));
        }else{
            this.posicion--;
        }
        return this.posicion;
    }    
    
    /**
     * Modificia al metodo de la superclase Mueble y Asiento y le añade la posicion actual del respaldo
     * @return (String) Listado de caracteristicas
     */
    @Override
    public String toString(){        
        String cadena = super.toString() + " Posición actual del respaldo: " +  this.posicion;
        return cadena;
    }   
}
package AlarconLopezAlbertomobiliario_;

/**
 * Clase que crea instancias de mueble de asiento del tipo Sofá
 * @author Alberto Alarcó López
 */

public final class Sofa extends Asiento{
    
    /**
     * Composición del sofá. Es una cadena de texto del tipo "3+2" o "2+CHAISELONGE"
     */
    private String composicion;
    
    /**
     * Constructor que reutiliza el de la superclase Mueble y Asiento y añade la composicion del sofá
     * @param precio (double) Precio del sofá
     * @param descripcion (String) Pequeña descripción del sofá
     * @param plazas (int) Número de plazas del sofá
     * @param tapiceria (String) Detalle de la tapicería del sofá
     * @param color (String) Color de la tapicería del sofá
     * @param composicion (String) Composición del sofá, puede ser cadena vacía o null
     */
    public Sofa(double precio, String descripcion, int plazas, String tapiceria, String color, String composicion){
        super(precio, descripcion, plazas, tapiceria, color);        
        this. composicion = composicion;    
    }

    /**
     * Devuelve la composición del sofá
     * @return (String) Composición de sofá
     */
    public String getComposicion() {
        return composicion;
    }
    
    /**
     * Modificia al metodo de la superclase Mueble y Asiento y le añade la composición del sofá
     * @return (String) Listado de caracteristicas
     */
    @Override
    public String toString(){
        String cadena = super.toString() + " Composición: " + this.composicion;
        return cadena;
    }   
}
package AlarconLopezAlbertomobiliario_;

/**
 * Interfaz que implementa cualquier clase cuyas instancias dispongan de la capacidad de ser "personalizadas"
 * @author Alberto Alarcón López
 */
public interface Personalizable {
    /**
     * Devuelve una cadena con los modulos que se han añadido al mueble personalizable
     * @return  (String) Módulos añadidos al mueble
     */
    String obtenerModulos();
    
    /**
     * Añade al mueble un elemento de tipo Módulo
     * @param modulo (Modulo) Tipo de módulo, debe ser de los incluidos en la clase Módulo
     * @throws IllegalStateException Si el numero de módulos supera el máximo permitido para el mueble
     * @throws NullPointerException Si el tipo de módulo es nulo
     */    
    void anyadirModulo(Modulo modulo) throws IllegalStateException, NullPointerException;
    
    /**
     * Extrae un modulo del mueble personalizable y lo devuelve
     * @return (Modulo) Módulo extraido
     * @throws IllegalStateException Si no se pueden extraer mas módulos
     */
    Modulo extraerModulo() throws IllegalStateException;
}
package AlarconLopezAlbertomobiliario_;

/**
 * Clase que crea instancias de mueble de almacenaje del tipo Estantería
 * @author Alberto Alarcón López
 */
public final class Estanteria extends Almacenaje{
    
    /**
     * Tipo de estantería por ejemplo "de pared", "de suelo"
     */
    private String tipo;
    
    /**
     * Constructor que reutiliza el de la superclase Mueble y Almacenaje y añade el tipo de estantería
     * @param precio (double) Precio de la estantería
     * @param descripcion (String) Descripción de la estantería
     * @param anchura (double) Ancho de la estantería
     * @param altura (double) Alto de la estantería
     * @param maxModulos (int) Número máximo de modulos que admite la estantería
     * @param tipo (String) Tipo de estantería, puede ser null o cadena vacia
     */
    public Estanteria(double precio, String descripcion, double anchura, double altura, int maxModulos, String tipo){
        super(precio, descripcion, anchura, altura, maxModulos);
        this.tipo = tipo;
    }

    /**
     * Devuelve una cadena con el tipo de Estanteria 
     * @return (String) Tipo de estantería
     */
    public String getTipo() {
        return tipo;
    }    
        
    /**
     * Verifica que el modulo es de tipo BALDA y si es así llama al método anyadirModulo de la superclase Almacenaje para añadirlo
     * @param modulo (Modulo) Tipo de módulo, solo son válidos los enumerados en la clase Modulo
     * @throws IllegalArgumentException Si el módulo a añadir no es del tipo BALDA, que es el único permitido para Estanteria
     * @throws NullPointerException Si el módulo es nulo
     */
    @Override
    public void anyadirModulo(Modulo modulo) throws IllegalArgumentException, NullPointerException{
        if (modulo != Modulo.BALDA && modulo != null){ // He tenido que añadir la comprobacion de null para la excepcion de null la lance el metodo super
            throw new IllegalArgumentException(String.format("Error: no se puede añadir el módulo %s, ya que solo se permiten módulos de tipo BALDA", modulo));
        } else {
            super.anyadirModulo(modulo);
        }
    }
    
    /**
     * Modificia al metodo de la superclase Mueble y Almacenaje y le añade el tipo de estanteria 
     * @return (String) Listado de caracteristicas
     */
    @Override
    public String toString(){
        String cadena = super.toString() + " Tipo: " + this.tipo;
        return cadena;
    }
}
package AlarconLopezAlbertomobiliario_;

/**
 * Clase que representa el concepto abstracto de un mueble de tipo asiento
 * @author Alberto Alarcón López
 */
public abstract class Asiento extends Mueble{    
    /**
     * Numero mínimo de plazas
     */
    public static final int MIN_PLAZAS = 1;
    
    /**
     * Numero máximo de plazas
     */
    public static final int MAX_PLAZAS = 9;
    
    /**
     * Numero de plazas del asiento
     */
    private final int numPlazas;
    /**
     * Tapicería del asiento
     */
    private final String tapiceria;
    /**
     * Color de la tapicería del asiento
     */
    private final String color;
     
    /**
     * Contructor que reutiliza el de la superclase Mueble y añade plazas, tapicería y color
     * @param precio (double) Precio del asiento
     * @param descripcion (String) Descripción del asiento
     * @param plazas (int) Número de plazas del asiento
     * @param tapiceria (String) Detalle de la tapicería del asiento
     * @param color (String) Color de la tapicería del asiento
     * @throws IllegalArgumentException Si el número de plazas es menor o mayor que los permitidos 
     */
    public Asiento(double precio, String descripcion, int plazas, String tapiceria, String color) throws IllegalArgumentException{
        super(precio, descripcion);        
        if (plazas < MIN_PLAZAS || plazas > MAX_PLAZAS){
            throw new IllegalArgumentException("El número de plazas no está en el rango permitido: " + plazas);
        }else{
            this.numPlazas = plazas;
            this.tapiceria = tapiceria;
            this.color = color;
        }
    }

    /**
     * Devuelve el número de plazas del asiento
     * @return (int) Número de plazas del asiento
     */
    public int getNumPlazas() {
        return numPlazas;
    }

    /**
     * Devuelve una cadena con la tapicería del asiento
     * @return (String) Detalle de la tapicería del asiento
     */
    public String getTapiceria() {
        return tapiceria;
    }

    /**
     * Devuelve una cadena con el color del asiento
     * @return (String) Color de la tapicería del asiento
     */
    public String getColor() {
        return color;
    }
    
    /**
     * Modificia al metodo de la superclase Mueble y le añade Tapiceria, Color y Numero de plazas a la cadena
     * @return (String) Listado de caracteristicas
     */
    @Override
    public String toString(){        
        String cadena = super.toString() + " Tapicería: " + this.tapiceria + " Color: " + this.color + " Número de plazas: " + this.numPlazas;        
        return cadena;
    }    
}
/**
 * Proporciona las clases e interfaces necesarias para crear y manipular productos 
 * de una tienda de muebles de venta online.
 * Incorpora la clase abstracta <code>Mueble</code> junto con una serie de clases derivadas:
 * <code>Almacenaje</code>, <code>Estanteria</code>, <code>Armario</code>, 
 * <code>Asiento</code>, <code>Silla</code>, <code>Sillon</code>, <code>Sofa</code>,
 *  <code>Mesa</code>. 
 * También incluye la clase <code>Modulo</code>.
 * Además, contiene las interfaces <code>Ajustable</code> y <code>Personalizable</code>,
 * que son implementadas por algunas de estas clases.
 * @author Alberto Alarcón López
 */
package AlarconLopezAlbertomobiliario_;

package AlarconLopezAlbertomobiliario_;

/**
 * Clase que crea instancias de mueble de almacenaje del tipo Armario
 * @author Alberto Alarcón López
 */

public final class Armario extends Almacenaje{
    
    /**
     * Número minimo de puertas que puede tener un Armario
     */
    public static final int MIN_PUERTAS = 1;
    /**
     * Número máximo de puertas que puede tener un Armario
     */
    public static final int MAX_PUERTAS = 6;
    
    /**
     * Numero de puertas de la instancia
     */
    private int numPuertas;
    
    /**
     * Constructor que reutiliza el de la superclase Mueble y Almacenaje y añade el numero de puertas del Armario
     * @param precio Precio del armario
     * @param descripcion Descripción del armario
     * @param anchura Ancho del armario
     * @param altura Alto del armario
     * @param maxModulos Número máximo de módulos que pueden añadirse al armario
     * @param numPuertas Número de puertas del armario
     * @throws IllegalArgumentException 
     */
    public Armario(double precio, String descripcion, double anchura, double altura, int maxModulos, int numPuertas) throws IllegalArgumentException{
        super(precio, descripcion, anchura, altura, maxModulos);
        if (numPuertas < MIN_PUERTAS || numPuertas > MAX_PUERTAS){
            throw new IllegalArgumentException ("No se puede crear el Armario. El número de puertas no está en el rango permitido: " + numPuertas);
        } else {
            this.numPuertas = numPuertas;
        }
    }
   
    /**
     * Devuelve el número de puertas del armario
     * @return (int) Número de puertas del armario
     */
    public int getNumPuertas() {
        return numPuertas;
    }
            
    /**
     * Modificia al metodo de la superclase Mueble y Almacenaje y le añade el numero de puertas del Armario
     * @return (String) Listado de caracteristicas
     */
    @Override
    public String toString(){
        String cadena = super.toString() + " Número de puertas: " + this.numPuertas;
        return cadena;
    }    
}
