/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package repasoJun34;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;
import ut8_utilidades.*;

/**
 *
 * @author atecaiestrassierra
 */
public class Jun24Ejercicio4 {

    public static void main(String[] args) {
        String cadena = "";
        do {
            cadena = ES.leeCadena("Dime un texto compuesto por palabras usando sólo letras de la a a la z, minúsculas");
        } while (!cadena.matches("^[ ,a-z]*$"));


        Map<String, Integer> mapaOcurrencias = ColeccionUtil.sacaMapaPalabrasCadenaSeparadaPorBlancos(cadena);
        File f = null;
        do {
            cadena = ES.leeCadena("Dime un nombre de fichero binario...");
            try {
                f = new File(cadena);
            } catch (Exception e) {
                System.out.println("Error" + e.getMessage());
            }
        } while (f == null);

        try (ObjectOutputStream escribiendo_fichero = new ObjectOutputStream(new FileOutputStream(f))) {
            escribiendo_fichero.writeObject(mapaOcurrencias);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        //archivo.matches("^[a-zA-Z][a-zA-Z0-9]{0,19}(\.[a-zA-Z

        //ultimo paso
        TreeMap<String, Integer> miTreeMapLeido=null;
        try (ObjectInputStream leyendo_fichero = new ObjectInputStream(new FileInputStream(f))) {
            //Casting explícito al tipo 'Recetario' porque 'readObject' devuelve tipo 'Object'
            miTreeMapLeido = (TreeMap<String, Integer>) leyendo_fichero.readObject();
        } catch (IOException | ClassNotFoundException | ClassCastException e) {
            System.err.println(e.getMessage());
        }
        //Tengo miTreeMapLeido
        //El mapa no es coleccion, pero puedo sacar una enumeración de sus claves e iterar por ellas, sacando valor asociado
        Set conjuntoDeClaves=miTreeMapLeido.keySet();
        Iterator it=conjuntoDeClaves.iterator();
        
        while(it.hasNext())
        {
            String palabra=(String)(it.next());
            int n=miTreeMapLeido.get(palabra);
            System.out.println(palabra+ " -> "+n+ ((n==1)?"vez":"veces"));
        }
    }   



}
