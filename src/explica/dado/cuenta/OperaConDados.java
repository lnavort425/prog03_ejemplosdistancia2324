/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package explica.dado.cuenta;

import ejemplos.apuntes.distancia.Dado;

/**
 *
 * @author luisnavarro
 */
public class OperaConDados {
    public static void main(String[] args) {
        Dado d1,d2;
        System.out.printf("inicio: numero de dados %d, numero lanzamientos %d, suma dados %d",Dado.getNumeroDados(),Dado.getNumeroTiradasTodosDados(), Dado.getSumaTiradasTodosDados());
        d1=new Dado(6);
        d2=new Dado(12);
        for (int i=0;i<8;i++){
            d1.lanzar();
            d2.lanzar();
        }
        System.out.println("");
        System.out.printf("d1: numero de lanzamientos %d, suma puntos %d, suma caras %d",d1.getNumeroTiradas(), d1.getSumaTiradas(), d1.getNumeroCaras());
        System.out.println("");
        System.out.printf("d2: numero de lanzamientos %d, suma puntos %d, suma caras %d",d2.getNumeroTiradas(), d2.getSumaTiradas(), d2.getNumeroCaras());
        
    }
   
}
