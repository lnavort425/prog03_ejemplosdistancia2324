/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pr1damdut6.pr1damdut6ejemplos.trabajadoresProyectosAnalist;

/**
 *
 * @author luisnavarro
 */
public class Ingeniero extends Funcionario {

    private String titulo;
    private int cantidadCargas;

    public Ingeniero(String rut, String nombre,
            String direccion, int sueldo, String titulo, int cantidadCargas) {
        super(rut, nombre, direccion, sueldo, "ingeniero", 30);
        this.titulo = titulo;
        this.cantidadCargas = cantidadCargas;
    }

    public double calcularSueldo() {
        double bonoProyectos = this.calcularBonoProyectos();
        double sueldo = this.getSueldoBase() + 8000 * this.cantidadCargas + bonoProyectos;
        return sueldo;
    }
