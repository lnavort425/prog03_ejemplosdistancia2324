/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pr1damdut6.pr1damdut6ejemplos.trabajadoresProyectosAnalist;

/**
 *
 * @author luisnavarro
 */
public class Proyecto {

    private int codigo;
    private String nombre;
    private int monto;
    private int meses;
    private Funcionario ingeniero;
    private Funcionario analista;
    private Funcionario programador;

    public Proyecto(int codigo, String nombre, int monto, int meses) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.meses = meses;
        this.monto = monto;
        ingeniero = null;
        analista = null;
        programador = null;
    }
}
