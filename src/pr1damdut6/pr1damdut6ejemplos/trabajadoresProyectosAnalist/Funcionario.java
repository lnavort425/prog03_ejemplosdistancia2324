/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pr1damdut6.pr1damdut6ejemplos.trabajadoresProyectosAnalist;

/**
 *
 * @author luisnavarro
 */
public abstract class Funcionario {

    private String rut;
    private String nombre;
    private String direccion;
    private int sueldoBase;
    private String tipo;
    private int porcentajePorProyecto;
    private ListaProyectos listaProyectos;

    protected Funcionario(String rut, String nombre, String direccion, int sueldo, String tipo,
            int porcentaje) {
        this.rut = rut;
        this.nombre = nombre;
        this.direccion = direccion;
        this.sueldoBase = sueldo;
        this.tipo = tipo;
        this.porcentajePorProyecto = porcentaje;
        listaProyectos = new ListaProyectos(5);
    }

    protected double calcularBonoProyectos() {
        double suma = 0;
        int monto = 0;
        double bono;
        int meses;
        for (int i = 0;
                i < this.listaProyectos.getCantidadProyectos(); i++) {
            Proyecto p = listaProyectos.getProyectoI(i);
            monto = p.getMonto();
            meses = p.getMeses();
            bono = (monto / meses) * (this.porcentajePorProyecto / 100);
            suma = suma + bono;
        }
        return suma;
    }

    abstract public double calcularSueldo();
}
