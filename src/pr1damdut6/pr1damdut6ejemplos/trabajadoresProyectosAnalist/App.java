/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pr1damdut6.pr1damdut6ejemplos.trabajadoresProyectosAnalist;

/**
 *
 * @author luisnavarro
 */
public class App {
public static void main(String[] args) { SistemaProyectosFuncionarios sistema =
new SistemaProyectosFuncionariosImpl(); leerFuncionarios(sistema);
leerProyectos(sistema);
try{ asociarProyectosFuncionarios(sistema);
}catch(NullPointerException ex){ StdOut.println(ex.getMessage());
       }
       ListaProyectos listaProyectos =
sistema.obtenerProyectos(); desplegarProyectos(listaProyectos);
       ListaFuncionarios listaFuncionarios =
                           sistema.obtenerFuncionarios();
desplegarFuncionarios(listaFuncionarios);
StdOut.print("Ingresar rut funcionario para ver sus proyectos: ");
String rut = StdIn.readString(); try{
ListaProyectos listaProyectosFuncionario = sistema.obtenerProyectosFuncionario(rut); desplegarProyectos(listaProyectosFuncionario);
}catch(NullPointerException ex){ StdOut.println(ex.getMessage());
}
StdOut.print("Ingresar codigo proyecto para ver sus funcionarios: ");
int codigo = StdIn.readInt(); try{
       ListaFuncionarios listaFuncionariosProyecto =
         sistema.obtenerFuncionariosProyecto(codigo);
desplegarFuncionarios(listaFuncionariosProyecto);
}catch(NullPointerException ex){ StdOut.println(ex.getMessage());
}
StdOut.println("Lista de ingenieros"); ListaFuncionarios listaIngenieros =
sistema.obtenerIngenieros(); desplegarFuncionarios(listaIngenieros);
}
public static void desplegarFuncionarios ( ListaFuncionarios listaFuncionarios){
for(int i = 0; i<listaFuncionarios.getCantidadFuncionarios();i++){
       Funcionario funcionario =
                listaFuncionarios.getFuncionarioI(i);
StdOut.println("rut: "+funcionario.getRut()+
", nombre: "+ funcionario.getNombre()+
", direccion: "+funcionario.getDireccion()+ ", sueldo base: "+funcionario.getSueldoBase()+ ", sueldo: "+funcionario.calcularSueldo());
} }
/*pg 252 capitulo2POOenJava
public static void desplegarProyectos(ListaProyectos listaProyectos){
for(int i = 0; i<listaProyectos.getCantidadProyectos();i++){
       Proyecto proyecto =
                      listaProyectos.getProyectoI(i);
StdOut.println("codigo: "+proyecto.getCodigo()+ ", nombre: "+ proyecto.getNombre()+
", meses: " + proyecto.getMeses()+
", monto mensual: " + proyecto.getMonto()/proyecto.getMeses()+

", monto total:\"+ proyecto.getMonto());"
        + "} }"
        + ""
        + ""
public static void leerFuncionarios( SistemaProyectosFuncionarios sistema){
   boolean ingreso=sistema.ingresarAnalista("1", "1",
                                          "1", 1, 1);
   if (!ingreso){
       StdOut.println("No se ingreso el analista.
                                    No hay espacio");
}
*/
}