/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package pr1damdut6.pr1damdut6ejemplos.trabajadoresProyectosAnalist;

/**
 *
 * @author luisnavarro
 */
public interface SistemaProyectosFuncionarios {
public void ingresarAnalista(String rut,String nombre,
String direccion,int sueldoBase,int agnosExperiencia);
public boolean ingresarProgramador(String rut,
String nombre, String direccion, int sueldoBase, String lenguajeProgramacion, int horasExtra,
int nivelProgramador);
public boolean ingresarIngeniero(String rut,
String nombre, String direccion, int sueldoBase, String titulo,int cantidadCargas);
public boolean ingresarProyecto(int codigo, String nombre,int monto, int meses);
public void asociarFuncionarioProyecto (
int codigoProyecto, String rut);
public ListaProyectos obtenerProyectos();
public ListaFuncionarios obtenerFuncionarios();
public ListaFuncionarios obtenerFuncionariosProyecto( int codigo);
public ListaProyectos obtenerProyectosFuncionario( String rut);
public ListaFuncionarios obtenerIngenieros();
 }