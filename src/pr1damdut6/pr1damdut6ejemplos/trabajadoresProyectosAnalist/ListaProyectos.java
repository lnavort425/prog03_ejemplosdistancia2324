/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pr1damdut6.pr1damdut6ejemplos.trabajadoresProyectosAnalist;

/**
 *
 * @author luisnavarro
 */
public class ListaProyectos {

    private Proyecto[] lp;
    private int cantidadProyectos;
    private int max;

    public ListaProyectos(int max) {
        lp = new Proyecto[max];
        cantidadProyectos = 0;
        this.max = max;
    }

    public boolean ingresarProyecto(Proyecto Proyecto) {
        if (cantidadProyectos < max) {
            lp[cantidadProyectos] = Proyecto;
            cantidadProyectos++;
            return true;
        } else {
            return false;
        }

    }

   
public int getCantidadProyectos() {
        return cantidadProyectos;
    }

    public Proyecto getProyectoI(int i) {
        if (i >= 0 && i < cantidadProyectos) {
            return lp[i];
        } else {
            return null;
        }
    }

    public Proyecto buscarProyecto(int codigo) {
        int i;
        for (i = 0; i < cantidadProyectos; i++) {
            if (lp[i].getCodigo() == codigo) {
                break;
            }
        }
        if (i == cantidadProyectos) {
            return null;
        } else {
            return lp[i];
        }
    }
}
