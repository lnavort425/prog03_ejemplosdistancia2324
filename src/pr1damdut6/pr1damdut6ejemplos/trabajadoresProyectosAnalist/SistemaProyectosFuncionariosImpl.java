/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pr1damdut6.pr1damdut6ejemplos.trabajadoresProyectosAnalist;

/**
 *
 * @author luisnavarro
 */
public class SistemaProyectosFuncionariosImpl implements           SistemaProyectosFuncionarios{
private ListaProyectos listaProyectos; private ListaFuncionarios listaFuncionarios;
public SistemaProyectosFuncionariosImpl() { this.listaProyectos = new ListaProyectos(2); this.listaFuncionarios=new ListaFuncionarios(10);
}
public boolean ingresarAnalista(String rut,
String nombre,String direccion, int sueldoBase, int agnosExperiencia){
Funcionario elAnalista = new Analista_ (rut,nombre, direccion, sueldoBase, agnosExperiencia);
boolean ingreso = listaFuncionarios.ingresarFuncionario(elAnalista);
return ingreso; }
public void ingresarProgramador(String rut,String nombre, String direccion, int sueldoBase, String lenguajeProgramacion, int horasExtra, int nivelProgramador){
Funcionario elProgramador = new Programador (rut, nombre, direccion, sueldoBase, lenguajeProgramacion, horasExtra, nivelProgramador);
boolean ingreso = listaFuncionarios.ingresarFuncionario(elProgramador);
return ingreso; }
   
  public void ingresarIngeniero(String rut,String nombre, String direccion, int sueldoBase, String titulo, int cantidadCargas){
Funcionario elIngeniero = new Ingeniero(rut,nombre, direccion, sueldoBase, titulo, cantidadCargas);
boolean ingreso = listaFuncionarios.ingresarFuncionario(elIngeniero);
return ingreso; }
public void ingresarProyecto(int codigo,String nombre, int monto, int meses){
Proyecto proyecto= new Proyecto(codigo, nombre, monto, meses);
boolean ingreso = listaProyectos.ingresarProyecto(proyecto);
return ingreso;
public void asociarFuncionarioProyecto (
int codigoProyecto, String rut){
    Funcionario funcionario =
            listaFuncionarios.buscarFuncionario(rut);
    Proyecto proyecto =
       listaProyectos.buscarProyecto(codigoProyecto);
if (funcionario != null && proyecto != null){ if (funcionario instanceof Analista_){
           proyecto.setAnalista(funcionario);
       }
else{
if (funcionario instanceof Programador){
               proyecto.setProgramador(funcionario);
           }
else{ proyecto.setIngeniero(funcionario);
}
}
funcionario.getListaProyectos().ingresarProyecto(proyecto); }
else{
throw new NullPointerException("No existe
                  el proyecto y/o el funcionario");
} }
public ListaProyectos obtenerProyectos(){ if (listaProyectos != null){
return listaProyectos; }
else{
throw new NullPointerException("No existe la lista de proyectos\");
} }
public ListaFuncionarios obtenerFuncionarios(){ if (listaFuncionarios != null){
return listaFuncionarios; }
else{
throw new NullPointerException("No existe
} }
la lista de funcionarios");
public ListaProyectos obtenerProyectosFuncionario (String rut){
    Funcionario funcionario =
            listaFuncionarios.buscarFuncionario(rut);
if (funcionario != null){
return funcionario.getListaProyectos();
}
throw new NullPointerException(
                         "No existe el funcionario");
}
public ListaFuncionarios obtenerIngenieros(){ ListaFuncionarios listaIngenieros =
new ListaFuncionarios(10);
for(int i = 0; i<listaFuncionarios.getCantidadFuncionarios();i++){
       Funcionario funcionario =
                listaFuncionarios.getFuncionarioI(i);
if (funcionario instanceof Ingeniero){ listaIngenieros.ingresarFuncionario(funcionario);
} }
return listaIngenieros; }
public ListaFuncionarios obtenerFuncionariosProyecto(int codigo){
    Proyecto proyecto =
                listaProyectos.buscarProyecto(codigo);
if (proyecto != null){
ListaFuncionarios listaFuncionariosProyecto =
new ListaFuncionarios(3);
if(proyecto.getAnalista()!= null){ listaFuncionariosProyecto.ingresarFuncionario(
                                      proyecto.getAnalista());
}
if(proyecto.getIngeniero()!= null){
             listaFuncionariosProyecto.ingresarFuncionario(
                                      proyecto.getIngeniero());
}
if (proyecto.getProgramador()!= null){
             listaFuncionariosProyecto.ingresarFuncionario(
                                   proyecto.getProgramador());
}
return listaFuncionariosProyecto; }
else{
throw new NullPointerException("No existe el proyecto");
} }
}