/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pr1damdut6.pr1damdut6ejemplos.trabajadoresProyectosAnalist;

/**
 *
 * @author luisnavarro
 */
public class Programador extends Funcionario {

    private String lenguaje;
    private int horasExtra;
    private int nivel;

    public Programador(String rut, String nombre,
            String direccion, int sueldo, String lenguaje, int horasExtra, int nivel) {
        super(rut, nombre, direccion, sueldo, "programador", 20);
        this.lenguaje = lenguaje;
        this.horasExtra = horasExtra;
        this.nivel = nivel;
    }

    public double calcularSueldo() {
        double bonoProyectos = this.calcularBonoProyectos();
        double sueldo = this.getSueldoBase() + 5000 * horasExtra + 30000 * nivel + bonoProyectos;
        return sueldo;
    }
}
