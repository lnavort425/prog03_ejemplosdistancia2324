/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pr1damdut6.pr1damdut6ejemplos.trabajadoresProyectosAnalist;

/**
 *
 * @author luisnavarro
 */
public class ListaFuncionarios {

    private Funcionario[] lf;
    private int cantidadFuncionarios;
    private int max;

    public ListaFuncionarios(int max) {
        lf = new Funcionario[max];
        cantidadFuncionarios = 0;
        this.max = max;
    }

    public boolean ingresarFuncionario(Funcionario funcionario) {
        if (cantidadFuncionarios < max) {
            lf[cantidadFuncionarios] = funcionario;
            cantidadFuncionarios++;
            return true;
        } else {
            return false;
        }
    }

    public int getCantidadFuncionarios() {
        return cantidadFuncionarios;
    }

    public Funcionario getFuncionarioI(int i) {
        if (i >= 0 && i < cantidadFuncionarios) {
            return lf[i];
        } else {
            return null;
        }
    }

    public Funcionario buscarFuncionario(String rut) {
        int i;
        for (i = 0; i < cantidadFuncionarios; i++) {
            if (lf[i].getRut().equals(rut)) {
                break;
            }
        }
        if (i == cantidadFuncionarios) {
            return null;
        } else {
            return lf[i];
        }
    }
}
