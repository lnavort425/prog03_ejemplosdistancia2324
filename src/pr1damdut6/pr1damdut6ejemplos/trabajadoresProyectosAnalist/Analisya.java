/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pr1damdut6.pr1damdut6ejemplos.trabajadoresProyectosAnalist;

/**
 *
 * @author luisnavarro
 */
public class Analista_ extends Funcionario{
private int anosExperiencia;
public Analista_ (String rut, String nombre,
String direccion, int sueldo, int anosExperiencia){
super(rut,nombre,direccion,sueldo,"analista",25);
this. anosExperiencia = anosExperiencia; }
public int getAnosExperiencia() { return anosExperiencia;
}
public void setAnosExperiencia(int anosExperiencia) { this.anosExperiencia = anosExperiencia;
}
public double calcularSueldo(){
double bonoProyectos=this.calcularBonoProyectos(); double sueldo = this.getSueldoBase() + 5000 * anosExperiencia + bonoProyectos; return sueldo;
} }