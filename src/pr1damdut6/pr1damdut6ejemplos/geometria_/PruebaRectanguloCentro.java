/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pr1damdut6.pr1damdut6ejemplos.geometria_;

/**
 *
 * @author luisnavarro
 */
public class PruebaRectanguloCentro {

    public static void main(String[] args) {

        //Creo rectángulo rojo de vértice inferior izquierdo (1,1), color rojo , ancho 2 alto 2
        Rectangulo r1 = new Rectangulo(1, 1, "rojo", 2, 2);
        System.out.println("r1=" + r1 + "area=" + r1.area() + ", perímetro=" + r1.perimetro());
        //que me diga su vértice inf izdo
        Punto vii = r1.dimeVerticeInferiorIzquierdo();
        System.out.println("vertice inf izdo de r1=" + vii);
        vii.setX(0);
        //A mi, rectángulo, me han manipulado, cómo? cómo lo evito?
        System.out.println("r1=" + r1 + "area=" + r1.area() + ", perímetro=" + r1.perimetro());

    }
}
