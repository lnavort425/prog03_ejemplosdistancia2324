//package geometria;
package pr1damdut6.pr1damdut6ejemplos.geometria_;

public abstract class Figura {

    protected Punto centro;
    private String color;

    public Figura(double x, double y, String F) {
        centro = new Punto(x, y);
        this.color = color;
    }

    public double getXCentro() {
        return centro.getX();
    }

    public double getYCentro() {
        return centro.getY();
    }

    public String getColor() {
        return color;
    }

    public void setXCentro(double x) {
        centro.setX(x);
    }

    public void setYCentro(double y) {
        centro.setY(y);
    }

    public void setColor(String color) {
        this.color = color;
    }

    public abstract double perimetro();

    public abstract double area();

}
