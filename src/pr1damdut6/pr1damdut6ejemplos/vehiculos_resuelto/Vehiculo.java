/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vehiculos_resuelto;

/**
 *
 * @author luisnavarro
 */
public class Vehiculo {

    private String matricula;
    private String marca;
    private int annoFabricacion;

    public Vehiculo(String mat, String mar, int anno)
    {
        this.matricula=mat;
        this.marca=mar;
        this.annoFabricacion=anno;
    }
    public String toString(){
        return "Vehículo matrícula:"+getMatricula()+" , marca: "+getMarca()+" , año fabricación: "+getAnnoFabricacion();
    }
    /**
     * @return the matricula
     */
    public String getMatricula() {
        return matricula;
    }

    /**
     * @param matricula the matricula to set
     */
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return the annoFabricacion
     */
    public int getAnnoFabricacion() {
        return annoFabricacion;
    }

    /**
     * @param annoFabricacion the annoFabricacion to set
     */
    public void setAnnoFabricacion(int annoFabricacion) {
        this.annoFabricacion = annoFabricacion;
    }

}
