/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vehiculos_resuelto;

public class GestorVehiculos {

    private static int NUM_MAX_VEHICULOS = 50;
    //----------------------------------------------
    //          Declaración de variables 
    //----------------------------------------------
    //Constantes
    /**
     * Numero usado para crear una pizza {@value INTRODUCIR_PIZZA}
     */
    private final int INTRODUCIR_VEHICULOS = 1;
    /**
     * Numero usado para mostrar el pedido {@value MOSTRAR_PEDIDO}
     */
    private final int MOSTRAR_VEHICULOS = 2;
    /**
     * Numero usado para crear crear ticket y finalizar pedido
     * {@value FINALIZAR_PEDIDO}
     */
    private final int DAR_DATOS_Y_PRESUPUESTO = 3;
    /**
     * Numero usado para salir de la aplicación {@value SALIR}
     */
    private final int RESUMEN_PARQUE_VEHICULOS = 4;
    private final int SALIR = 5;
    //variables
    Vehiculo[] parqueVehiculos;  //guardo las pizzas en un array 
    private int numVehiculosLeidos = 0; //numero de autos que llevo metidos

    /**
     * Método que mostrará el menú de la aplicación y que lanzará los diferentes
     * métodos internos en función de lo se pida.
     */
    public static void main(String[] args) {
        GestorVehiculos g = new GestorVehiculos();
        g.gestionarVehiculos();
    }

    public GestorVehiculos() {
        //Por hacer
        parqueVehiculos = new Vehiculo[NUM_MAX_VEHICULOS];
        numVehiculosLeidos = 0;
    }

    public void gestionarVehiculos() {//Método que mostrará el menú de la aplicación y que lanzará los diferentes métodos internos en función de lo se pida
        //Variables        
        boolean terminar = false;

        // Variables de entrada
        int opcionElegida = 0;

        //----------------------------------------------
        //               Procesamiento 
        //----------------------------------------------
        do {
            //ES.leeOpciones("menú", ["introducir","ver","datos","resumen"])
            ES.msgln("\nMenú Gestión vehículos");
            ES.msgln("-------------------------");
            ES.msgln(INTRODUCIR_VEHICULOS + ".- Introducir vehículo.");
            ES.msgln(MOSTRAR_VEHICULOS + ".- Ver listado de vehiculos.");
            ES.msgln(this.DAR_DATOS_Y_PRESUPUESTO + ".- Dar datos y presupuestp de vehiculos.");
            ES.msgln(this.RESUMEN_PARQUE_VEHICULOS + ".- Resumen parque vehiculos.");
            opcionElegida = ES.leeEntero("elija una opción:", 1, 4); //Leeremos un número entero entre 1 y 4.

            switch (opcionElegida) {

                case INTRODUCIR_VEHICULOS: //Crearemos una pizza y la guardaremos.

                    // Llamar al método insertarPizzaEnPedido
                    insertarVehiculo();
                    break;

                case MOSTRAR_VEHICULOS: //Mostraremos las pizzas del pedido.
                    //Llamar al método mostrarPedido    
                    mostrarVehiculo();
                    break;

                case DAR_DATOS_Y_PRESUPUESTO: // Mostraremos las pizzas del pedido, el coste y prepararemos la aplicación para un nuevo pedido.
                    //Llamar al método finalizaPedido
                    darDatosPresupuesto();
                    break;
                case RESUMEN_PARQUE_VEHICULOS: // Mostraremos las pizzas del pedido, el coste y prepararemos la aplicación para un nuevo pedido.
                    //Llamar al método finalizaPedido
                    resumenParqueVehiculos();
                    break;

                case SALIR:
                    terminar = true;
                    break;
            }

        } while (!terminar);
        ES.msg("Finalizando aplicación...\n");
    }

    public void insertarVehiculo() {
        Vehiculo v = leerVehiculo();
        if (numVehiculosLeidos >= NUM_MAX_VEHICULOS) ;//throw exception o lo q sea
        parqueVehiculos[numVehiculosLeidos++] = v;
        ES.msg("En la posición " + numVehiculosLeidos + " he insertado: " + v.toString());

    }

    public void mostrarVehiculo() {
        ES.msg("Los vehiculos del parque son: ");
        for (int i = 0; i < numVehiculosLeidos; i++) {
            ES.msg(parqueVehiculos[i]);
        }
    }

    public Vehiculo leerVehiculo() {
        String s, mat, mar;
        int anno, ckm, deposito, carga;
        boolean esAuto = true;
        Vehiculo v;
        Auto auto;
        Camioneta camioneta;
        String[] a={"A","C"};
        s= ES.leeOpciones("Te voy a pedir los datos de un vehículo. Primero dime si es auto o camioneta.", a);
       /* ES.msg("Te voy a pedir los datos de un vehículo. Primero dime si es auto o camioneta.");
        do {
            s = ES.leeCadena("Auto (A) o Camioneta(C): ");
        } while (!(s.equals("A") || s.equals("C")));*/
        esAuto = (s.equals("A"));
        mat = ES.leeCadena("Dime matrícula ");
        mar = ES.leeCadena("Dime marca ");
        anno = ES.leeEntero("Dime año matriculación: ");
        if (esAuto) {
            ckm = ES.leeEntero("Dime lectura cuenta km: ");
            deposito = ES.leeEntero("Dime capacidad deposito combustible: ");
            v = new Auto(mat, mar, anno, ckm, deposito);
        } else {

            carga = ES.leeEntero("Dime capacidad de carga: ");
            v = new Camioneta(mat, mar, anno, carga);
        }

        return v;
    }



    public void darDatosPresupuesto() {

    }

    public void resumenParqueVehiculos() {

    }

}
