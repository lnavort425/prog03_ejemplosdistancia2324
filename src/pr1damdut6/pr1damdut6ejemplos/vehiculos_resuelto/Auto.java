/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vehiculos_resuelto;

/**
 *
 * @author luisnavarro
 */
public class Auto extends Vehiculo {

    /**
     * @return the cuentaKilometros
     */
    public int getCuentaKilometros() {
        return cuentaKilometros;
    }

    /**
     * @param cuentaKilometros the cuentaKilometros to set
     */
    public void setCuentaKilometros(int cuentaKilometros) {
        this.cuentaKilometros = cuentaKilometros;
    }

    /**
     * @return the capacidadDepositoCombustible
     int
    public double getCapacidadDepositoCombustible() {
        return capacidadDepositoCombustible;
    }

    /**
     * @param capacidadDepositoCombustible the capacidadDepositoCombustible to set
     */
    public void setCapacidadDepositoCombustible(int capacidadDepositoCombustible) {
        this.capacidadDepositoCombustible = capacidadDepositoCombustible;
    }
    private int cuentaKilometros;
    private int capacidadDepositoCombustible;
    public Auto(String mat, String mar, int anno, int ck, int cdc){
        super(mar, mar, anno);
        this.cuentaKilometros=ck;
        this.capacidadDepositoCombustible=cdc;
    }
    public String  toString(){
        return super.toString()+" cuenta_kilómetros="+this.cuentaKilometros+" , capacidad depósito="+capacidadDepositoCombustible;
    }
}
