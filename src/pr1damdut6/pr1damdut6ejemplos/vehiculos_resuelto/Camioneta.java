/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vehiculos_resuelto;

/**
 *
 * @author luisnavarro
 */
public class Camioneta extends Vehiculo{
    private int capacidadCarga;
    public Camioneta(String matricula, String marca,int annoFabric, int capacidadCargaKilos){
        super(matricula,marca,annoFabric);
        this.capacidadCarga=capacidadCargaKilos;
        
    }

    /**
     * @return the capacidadCarga
     */
    public int getCapacidadCarga() {
        return capacidadCarga;
    }

    /**
     * @param capacidadCarga the capacidadCarga to set
     */
    public void setCapacidadCarga(int capacidadCarga) {
        this.capacidadCarga = capacidadCarga;
    }
}
