/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_ficheros1;

import java.io.*;

/**
 *
 * @author luisnavarro
 */
public class LeeEscribeFichero {

    public static void main(String[] args) {

        try {
            File archivo = new File(ut8_ficherosutil.Constantes.RUTA_ENTRADA + "fich.txt");
            FileReader fr = new FileReader(archivo);
            BufferedReader br = new BufferedReader(fr);
            String linea = br.readLine();
            System.out.println(""+linea);
            ///
            FileWriter fichero = null;
            PrintWriter pw = null;
            fichero = new FileWriter(ut8_ficherosutil.Constantes.RUTA_ENTRADA + "fich2.txt");
            pw = new PrintWriter(fichero);
            pw.println("Linea de texto:"+linea);
            pw.flush();
            fichero.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
