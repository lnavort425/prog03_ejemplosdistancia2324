/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_ficheros1;

import java.io.*;

/**
 *
 * @author luisnavarro
 */
public class EscribeaFichero {

    public static void main(String[] args) {
        try {
            PrintWriter out = null;
            out = new PrintWriter(new FileWriter(Token.RUTA_SALIDA + "salida3.txt", true));

            BufferedReader br = new BufferedReader(
                    new InputStreamReader(System.in));
            String s;
            while (!(s = br.readLine()).equals("salir")) {
                out.println(s);
            }
            out.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
