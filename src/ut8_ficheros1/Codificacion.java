package ut8_ficheros1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
/**
 *Averigaur codificaci�n del archivo
 * 
 * @author JJBH
 */
public class Codificacion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        FileInputStream fichero;
        try {
            // Elegimos fichero para leer flujos de bytes "crudos"
            fichero = new FileInputStream("datos.txt");
            // InputStreamReader sirve de puente de flujos de byte a caracteres
            InputStreamReader unReader = new InputStreamReader(fichero);
            // Vemos la codificaci�n actual
            System.out.println(unReader.getEncoding());
        } catch (FileNotFoundException ex) {
            System.err.println(Codificacion.class.getName());
        }
    }
 
}
