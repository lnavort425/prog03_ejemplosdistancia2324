/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_ficheros1;

/**
 *
 * DateFormat df; writeString (file, getNombre(), 32); writeString (file,
 * getTelefono(), 16); writeString (file, getEmail(), 32); writeString (file,
 * getDireccion(), 64); df = DateFormat.getDateInstance(DateFormat.LONG);
 * writeString (file, df.format(getNacimiento()), 32); file.writeInt (
 * getGrupo() ); file.writeDouble ( getDeuda() ); }
 */
import java.text.DateFormat;
import java.io.Serializable;
import java.util.Date;

public class Contacto implements Serializable{

    private String nombre;
    private String telefono;
    private String email;
    private String direccion;
    private Date nacimientoM;
    private int grupo;
    private double deuda;

    /**
     * @return the nombre
     */
    public Contacto(String nombre, String telefono, String email, String direccion, Date nacimientoM, int grupo, double deuda){
        this.nombre = nombre;
        this.telefono = telefono;
        this.email = email;
        this.direccion = direccion;
        this.nacimientoM = nacimientoM;
        this.grupo = grupo;
        this.deuda = deuda;
    }
    public Contacto(){
        
    }
    public String toString(){
        return "nombre:"+nombre+"; telefono:"+telefono+": email:"+email+"; direccion:"+direccion+": nacimiento:"+nacimientoM+"; grupo:"+grupo+"; deuda: "+deuda;
    }
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the nacimientoM
     */
    public Date getNacimiento() {
        return nacimientoM;
    }

    /**
     * @param nacimientoM the nacimientoM to set
     */
    public void setNacimiento(Date nacimientoM) {
        this.nacimientoM = nacimientoM;
    }

    /**
     * @return the grupo
     */
    public int getGrupo() {
        return grupo;
    }

    /**
     * @param grupo the grupo to set
     */
    public void setGrupo(int grupo) {
        this.grupo = grupo;
    }

    /**
     * @return the deuda
     */
    public double getDeuda() {
        return deuda;
    }

    /**
     * @param deuda the deuda to set
     */
    public void setDeuda(double deuda) {
        this.deuda = deuda;
    }
}
