/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_ficheros1;

/**
 *
 * @author luisnavarro
 */
import java.io.*;

public class FicheroContactos {
    // Fichero de acceso aleatorio 

    private RandomAccessFile file;
    // Apertura del fichero 

    public void abrir()
            throws IOException {
        file = new RandomAccessFile(ut8_ficherosutil.Constantes.RUTA_SALIDA + "clientes.dat", "rw");
    }
    // Cierre del fichero 

    public void cerrar()
            throws IOException {
        if (file != null) {
            file.close();
        }
    }
    // Escribir un registro 
    // en la posición actual del cursor 

    public void escribir(Registro registro)
            throws IOException {
        if (file != null) {
            registro.write(file);
        }
    }
    // Escribir un registro en una posición cualquiera 

    public void escribir(Registro registro, int pos)
            throws IOException {
        if (file != null) {
            file.seek((pos - 1) * Registro.DIM);
            escribir(registro);
        }
    }
    // Leer del fichero el registro  
    // que se encuentra en la posición actual del cursor 

    public Registro leer() {
        Registro registro = null;
        if (file != null) {
            try {
                registro = new Registro();
                registro.read(file);
            } catch (Exception error) {
                registro = null;
            }
        }
        return registro;
    }
    // Leer del fichero un registro cualquierta 
    // (el parámetro indica la posición del registro) 

    public Registro leer(int pos)
            throws IOException {
        if (file != null) {
            file.seek((pos - 1) * Registro.DIM);
        }
        return leer();
    }
}
