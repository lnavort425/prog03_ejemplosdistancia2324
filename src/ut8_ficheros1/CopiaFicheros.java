/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_ficheros1;

import java.io.*;

/**
 *
 * @author luisnavarro
 */
public class CopiaFicheros {

    public CopiaFicheros() {

    }

    public static void main(String[] args) {
        try {
            String fIn = Token.RUTA_SALIDA + "salida3.txt";
            String fOut = Token.RUTA_SALIDA + "fichero3Copia.txt";
            File ficheroEntrada = new File(fIn);
            FileInputStream fisEntrada = new FileInputStream(ficheroEntrada);
            File ficheroSalida = new File(fOut);
            FileOutputStream fosSalida = new FileOutputStream(ficheroSalida);
            //InputStream isEntrada=new InputStream(fisEntrada);
            //OutputStream osSalida=new OutputStream(fosSalida);
            CopiaFicheros.copia(fisEntrada, fosSalida);
        } catch (FileNotFoundException fnfe) {
            System.out.println("" + fnfe.getMessage());
        }
    }

    public static void copia(InputStream fentrada, OutputStream fsalida) {

        try {
            int n = 0;

            byte[] buffer = new byte[256];
            while ((n = fentrada.read(buffer)) >= 0) {
                fsalida.write(buffer, 0, n);
            }

            fentrada.close();
            fsalida.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

}

/*
void copia(InputStream fentrada, OutputStream fsalida) {
        
        try {
            int n = 0;
            
            byte[] buffer = new byte[256];
            while((n = fentrada.read(buffer)) >= 0)
                fsalida.write(buffer,0,n) ;
            
            fentrada.close();
            fsalida.close();
        } catch(IOException ex){
            System.out.println(ex.getMessage());
        }
}

*/