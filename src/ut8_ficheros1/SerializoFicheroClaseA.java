/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_ficheros1;


// Java program to illustrate Serializable interface
import java.io.*;
  
// By implementing Serializable interface
// we make sure that state of instances of class A
// can be saved in a file.

  
public class SerializoFicheroClaseA {
    public static void main(String[] args)
        throws IOException, ClassNotFoundException
    {
        A a = new A(20, "GeeksForGeeks");
  
        // Serializing 'a'
        FileOutputStream fos
            = new FileOutputStream(Token.RUTA_SALIDA+"xyz.txt");
        ObjectOutputStream oos
            = new ObjectOutputStream(fos);
        oos.writeObject(a);
  
        // De-serializing 'a'
        FileInputStream fis
            = new FileInputStream(Token.RUTA_SALIDA+"xyz.txt");
        ObjectInputStream ois = new ObjectInputStream(fis);
        A b = (A)ois.readObject(); // down-casting object
  
        System.out.println(b.i + " " + b.s);
  
        // closing streams
        oos.close();
        ois.close();
    }
}

class A implements Serializable {
    int i;
    String s;
  
    // A class constructor
    public A(int i, String s)
    {
        this.i = i;
        this.s = s;
    }
}