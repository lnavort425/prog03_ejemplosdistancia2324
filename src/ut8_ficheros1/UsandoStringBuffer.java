/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_ficheros1;

/**
 *
 * @author luisnavarro
 */

public class UsandoStringBuffer {

    public static void main(String[] args) {
        pruebaDiferencia(10);
        pruebaDiferencia(100);
        pruebaDiferencia(10000);
        pruebaDiferencia(100000);
        pruebaDiferencia(1000000);
        pruebaDiferencia(10000000);
    }

  private static String metodo1(int n) {

    String resultado = "";

    for (int i = 0; i < n; i++)

      resultado+= i + ", ";

    return resultado;

  }

 

  private static String metodo2(int n) {

    StringBuffer buffer = new StringBuffer();

    for (int i = 0; i < n; i++)

      buffer.append(i).append(", ");

    return buffer.toString();

  }

   private static String metodo3(int n) {

    StringBuilder buffer = new StringBuilder();

    for (int i = 0; i < n; i++)

      buffer.append(i).append(", ");

    return buffer.toString();

  }


  public static void pruebaDiferencia(int cantidad) {

    int n = cantidad;

    long t1, t2;

 

    t1 = System.currentTimeMillis();

    metodo1(n);

    t2 = System.currentTimeMillis();

    System.out.println("método 1, String : con n="+n+"-->" + (t2 - t1) + "ms");

 

    t1 = System.currentTimeMillis();

    metodo2(n);

    t2 = System.currentTimeMillis();

    System.out.println("método 2, StringBuffer: con n="+n+"--> " + (t2 - t1) + "ms");
    
    
    t1 = System.currentTimeMillis();

    metodo3(n);

    t2 = System.currentTimeMillis();

    System.out.println("método 3, StringBuilder: con n="+n+"--> " + (t2 - t1) + "ms");

  }

}  