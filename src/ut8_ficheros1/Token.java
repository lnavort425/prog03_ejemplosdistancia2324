/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_ficheros1;

/**
 *
 * @author luisnavarro
 */
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;


public class Token {
//private static final String RUTA_SALIDA = "/Users/luisnavarro/OneDrive/IES_LN_IES_2023/Prepara_clases_Programacion/PR_UT8/ficherosalida/";
//private static final String RUTA_ENTRADA = "/Users/luisnavarro/OneDrive/IES_LN_IES_2023/Prepara_clases_Programacion/PR_UT8/ficheroentrada/";
public static final String RUTA_SALIDA = "/Users/luisnavarro/entregas/ut8salida/";
//public static final String RUTA_SALIDA = "c:\\users\\fulano\\ /Users/luisnavarro/entregas/ut8salida/";
public static final String RUTA_ENTRADA = "/Users/luisnavarro/entregas/ut8entrada/";

    public void contarPalabrasyNumeros(String nombreFichero) {

        StreamTokenizer sTokenizer = null;
        int contadorPalabras = 0, numeroDeNumeros = 0;

        try {

            sTokenizer = new StreamTokenizer(new FileReader(nombreFichero));

            while (sTokenizer.nextToken() != StreamTokenizer.TT_EOF) {

                if (sTokenizer.ttype == StreamTokenizer.TT_WORD)
                    contadorPalabras++;
                else if (sTokenizer.ttype == StreamTokenizer.TT_NUMBER)
                    numeroDeNumeros++;
            }

            System.out.println("Número de palabras en el fichero: " + contadorPalabras);
            System.out.println("Número de números en el fichero: " + numeroDeNumeros);

        } catch (FileNotFoundException ex) {
           System.out.println(ex.getMessage()) ;
        } catch (IOException ex) {
           System.out.println(ex.getMessage()) ;
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new Token().contarPalabrasyNumeros(RUTA_ENTRADA+"datos.txt");
    }
}

