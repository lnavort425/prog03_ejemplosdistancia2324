package ut8_ficheros1;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Clase Filro que implementa el interface FilenameFilter
 * @author José Javier Bermúdez Hernández 
 */
public class FiltroSoloEjecutable implements FilenameFilter {

    String extension ;

    /**
     * Constructor
     * @param extension Parámetro extensión del fichero
     */
    public FiltroSoloEjecutable(String extension) {
        this.extension = extension ;
    }

    @Override
    public boolean accept(File dir, String name) {
        return new File(dir+name).canExecute();//name.endsWith(extension) ;
    }
   
}