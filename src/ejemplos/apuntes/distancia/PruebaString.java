/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplos.apuntes.distancia;

/**
 *
 * @author luisnavarro
 */
public class PruebaString {

    public static void main(String[] args) {
        /*
        String s1 = null, s2 = "", s3 = "hola";
        String s4 = new String("hola");
        System.out.println("s3==s4?" + (s3 == s4));
        System.out.println("s3 y s4 iguales?" + s3.equalsIgnoreCase(s4));
        System.out.println("s1==s2?" + (s1 == s2));
         */
        // Declaramos y creamos una nueva cadena (declaración + instanciación + asignación)
        String cadenaOriginal = new String("Hola");

// Mostramos su contenido en pantalla
        System.out.println("Cadena original: " + cadenaOriginal);

// Le concatenamos la cadena " caracola" (aplicación del método concat)  
        String cadenaConcatenada = cadenaOriginal.concat(" caracola");
        cadenaOriginal.concat(" caracola");

// Volvemos a mostrar su contenido en pantalla        
        System.out.println("Cadena original: después de concat caracola:" + cadenaConcatenada);
        System.out.println("Cadena original: " + cadenaOriginal);
        System.out.println("Cadena tras la aplicación del método concat: " + cadenaOriginal.concat(" caracola"));
        //"hola, soy dfsf"
    }
}
