/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplos.apuntes.distancia;

/**
 *
 * @author luisnavarro
 */
public class AleatorioEjemplo {

    public static void main(String[] args) {
        double aleatorioReal;
        double aleatorioRealEscalado;
        int aleatorioEntero1;
        int aleatorioEntero2;

        aleatorioReal = Math.random();
        aleatorioRealEscalado = aleatorioReal * 25;
        aleatorioEntero1 = (int) aleatorioRealEscalado;
        aleatorioEntero2 = (int) Math.floor(aleatorioRealEscalado);
        System.out.println("Aleatorio real: " + aleatorioReal);
        System.out.println("Aleatorio real escalado al intervalo [0,25[: " + aleatorioRealEscalado);
        System.out.println("Aleatorio entero con casting (int): " + aleatorioEntero1);
        System.out.println("Aleatorio entero con Math.floor: " + aleatorioEntero2);
    }
}
