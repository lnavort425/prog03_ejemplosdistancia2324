/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplos.apuntes.distancia;

/**
 *
 * @author atecaiestrassierra
 */
public class EjemploMetodoEstaticos {
    public static void main(String[] args) {
        double r=8;
        //double r=Math.sqrt(8);
        double area=Math.PI*r*r;
        System.out.println("El área del círuclo de radio r="+r+" es "+area);
        
    }
}
