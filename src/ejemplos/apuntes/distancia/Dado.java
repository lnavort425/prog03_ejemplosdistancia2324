/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplos.apuntes.distancia;

import java.util.Random;

/**
 *
 * @author luisnavarro
 */
public class Dado {
   private static Random r=new Random();
   private int numeroCaras=6;
   private static int numeroDados=0;
   private static int sumaTiradasTodosDados=0;
   private static int numeroTiradasTodosDados=0;
   private int numeroTiradas=0;
   private int sumaTiradas=0;
   public Dado(int numCaras)
   {
       this.numeroCaras=numCaras;
       numeroDados++;
   }
   public int lanzar()
   {
       int valor=1+r.nextInt(getNumeroCaras());
       numeroTiradas++;
       numeroTiradasTodosDados++;
       sumaTiradas+=valor;
       sumaTiradasTodosDados+=valor;
       return valor;
            
   }

    /**
     * @return the numeroCaras
     */
    public int getNumeroCaras() {
        return numeroCaras;
    }

    /**
     * @return the numeroDados
     */
    public static int getNumeroDados() {
        return numeroDados;
    }

    /**
     * @return the sumaTiradasTodosDados
     */
    public static int getSumaTiradasTodosDados() {
        return sumaTiradasTodosDados;
    }

    /**
     * @return the numeroTiradasTodosDados
     */
    public static int getNumeroTiradasTodosDados() {
        return numeroTiradasTodosDados;
    }

    /**
     * @return the numeroTiradas
     */
    public int getNumeroTiradas() {
        return numeroTiradas;
    }

    /**
     * @return the sumaTiradas
     */
    public int getSumaTiradas() {
        return sumaTiradas;
    }

}
