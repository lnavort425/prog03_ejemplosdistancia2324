/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplos.apuntes.distancia;

/**
 *
 * @author luisnavarro
 */
public class Ejemplo1_OperaConRectanguloLuis {
    public static void main(String[] args) {
        Rectangulo2Luis r1;
        Rectangulo2Luis r2,r3;
        r1=new Rectangulo2Luis(0, 0,3,4);
        r2=new Rectangulo2Luis(3,4);
        r3=r2;
        System.out.println("iguales r1 y r2:"+r1.igual(r2));
        System.out.println("El mismo r1 y r2?:"+(r1==r2));
        System.out.println("El mismo r3 y r2?:"+(r3==r2));
    }
    
}
