/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplos.apuntes.distancia;

import java.awt.Rectangle;

/**
 *
 * @author atecaiestrassierra
 */
public class punto_5_2 {

    public static void main(String[] args) {
        Rectangle rec1 = new Rectangle(1, 2, 5, 4);
        Rectangle rec2 = new Rectangle(5, 3, 4, 6);
        System.out.println("rec1 contiene el punto (3,3): " + (rec1.contains(3, 3) ? "sí" : "no"));
        System.out.println("rec2 contiene el punto (3,3): " + (rec2.contains(3, 3) ? "sí" : "no"));
        System.out.println("rec2 contiene el punto (3,3): " + (rec2.contains(2, 2, 1, 1) ? "sí" : "no"));
        System.out.println("rec1 tiene alguna zona en común con rec2: " + (rec1.intersects(rec2) ? "sí" : "no"));
        Rectangle rec3 = rec1.intersection(rec2);
        System.out.println("rec1 inters. rec2="+rec1.intersection(rec2).toString());
        System.out.println("Rectángulo rec3 intersección de rec1 y rec2:");
        System.out.println("Ubicación: x=" + rec3.x + " y=" + rec3.y);
        System.out.println("Dimensiones: base= " + rec3.width + " altura= " + rec3.height);
        System.out.println("r3="+rec3);
        rec3.setLocation(2, 2);
        //rec3.contains(rec2);
        System.out.println("r3="+rec3);
    }
}
