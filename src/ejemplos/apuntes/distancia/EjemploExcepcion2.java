/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplos.apuntes.distancia;

/**
 *
 * @author luisnavarro
 */
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Ejercicio resuelto sobre manejo de excepciones.
 * El programa solicita que el usuario introduzca por teclado un n˙mero 
 * entre 0 y 100, debiendo gestionarse la entrada por medio de excepciones.
 *
 * @author JJBH
 */
public class EjemploExcepcion2 {
    public static void main(String[] args){
        int numero = -1 ;
        int intentos = 0 ;
        
        do{
            try{
                System.out.print("Introduzca un n˙mero entre 0 y 100: ");
                Scanner teclado = new Scanner(System.in) ;        
                numero = teclado.nextInt() ;
                if (numero<0 || numero>100)
                {
                    System.out.print("El n˙mero introducido no est· entre 0 y 100.\n");
                }
            } catch (ArithmeticException ex) {
                System.err.println("Error al intentar realizar la operaciÛn de divisÛn.");    
                 
            } catch (InputMismatchException ex) {
                System.err.println("Error de lectura de datos.");
                            
            }finally{
                intentos++;
            }            
        }while (numero < 0 || numero > 100);
        
        System.out.println("El n˙mero introducido es: " + numero);
        System.out.println("N˙mero de intentos: " + intentos);   
    }
}