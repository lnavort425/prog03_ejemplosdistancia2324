/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplos.apuntes.distancia;

import java.awt.Rectangle;

/**
 *
 * @author luisnavarro
 */
public class Ejemplo_p4_5 {

    public static void main(String[] args) {
        // Instanciamos un nuevo objeto de la clase Rectangle 
// y apuntamos a él mediante la variable r1
// Ubicación en (0,0) y dimiensiones 10 de base, 5 de altura   
        System.out.print("Creando objeto instancia de la clase Rectangle ");
        System.out.println("y referenciado desde la variable r1.");
        Rectangle r1 = new Rectangle(0, 0, 10, 5);
        //r1=new Rectangle(10,5);
        System.out.println();

// Mostramos los atributos o propiedades del objeto al que apunta r1
        System.out.println("Rectángulo r1:");
        System.out.println("Ubicación: x=" + r1.x + " y=" + r1.y);
        System.out.println("Dimensiones: base= " + r1.width + " altura= " + r1.height);


// Modificamos los atributos o propiedades del objeto al que apunta r1
// 1. Accediendo directamente a sus atributos y modificándolos (pues son públicos)
        r1.height = 20;
        r1.width = 100;
        System.out.println();
        System.out.println("Rectángulo r1 modificado mediante manipulación de atributos:");
        System.out.println("Ubicación: x=" + r1.x + " y=" + r1.y);
        System.out.println("Dimensiones: base= " + r1.width + " altura= " + r1.height);

// 2. Utilizando un método para modificar algunos de sus atributos: setSize
        r1.setSize(200, 50);
        double anchuraR1=r1.getWidth();
        anchuraR1=r1.width;
        r1.height=8;
        r1.setSize( (int)r1.getWidth(),8);
        System.out.println();
        System.out.println("Rectángulo r1 modificado mediante llamada a un método:");
        System.out.println("Ubicación: x=" + r1.x + " y=" + r1.y);
        System.out.println("Dimensiones: base= " + r1.width + " altura= " + r1.height);
        
        System.out.println("Imprimo r1.toString()"+r1.toString());
        
        Rectangle r8=r1;
        r8=new Rectangle(r1);
        
        int a=8;
        System.out.println("a="+a);
        r8.setSize(a, a);
        System.out.println("a="+a);
        System.out.println("r8 contiene el punto x=7 y=8?"+r8.contains(7, 8));
        
    }

}
