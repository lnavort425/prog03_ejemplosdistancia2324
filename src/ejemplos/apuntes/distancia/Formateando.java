/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplos.apuntes.distancia;


import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Probando el formateo en el tiempo, con Java
 * @author jjber
 */
public class Formateando {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        LocalDateTime fecha = LocalDateTime.now();
        
        DateTimeFormatter isoFecha = DateTimeFormatter.ISO_LOCAL_DATE;
        System.out.println("Sin formato: " + fecha);
        System.out.println("Con formato: " + fecha.format(isoFecha));
      
        DateTimeFormatter isoHora = DateTimeFormatter.ISO_LOCAL_TIME;
        System.out.println(fecha.format(isoHora));
        
        LocalTime hora = LocalTime.now();
        DateTimeFormatter f = DateTimeFormatter.ofPattern("'Son las' h 'y' mm");
        //System.out.println(hora);
        System.out.println(hora.format(f));
        
        
        LocalDateTime otraHora = LocalDateTime.now();
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("hh:MM");
        System.out.println("Hora:mes = " + otraHora.format(formato));
        DateTimeFormatter formato2 = DateTimeFormatter.ofPattern("hh:mm");
        System.out.println("Hora:minutos = " + otraHora.format(formato2));
    }
    
}