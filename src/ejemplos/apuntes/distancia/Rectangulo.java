/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplos.apuntes.distancia;

import java.awt.Rectangle;

/**
 *
 * @author luisnavarro
 */
public class Rectangulo extends Rectangle{
    public static void main(String[] args) {
        Rectangulo r=new Rectangulo();
        Rectangle r2=new Rectangle(1,1,2,3);
        Rectangulo r3=new Rectangulo(2,2,3,4);
        System.out.printf("r Mis coordenadas x=%d, y=%d",r.x,r.y);
        System.out.printf("r2 Mis coordenadas x=%d, y=%d",r2.x,r2.y);
        System.out.printf("r3 Mis coordenadas x=%d, y=%d, área=%d, perímetro=%d",r3.x,r3.y, r3.area(),r3.perimetro());
    }
    public Rectangulo(int x,int y,int a,int h)
    {
        super(x,y,a,h);
    }
    public Rectangulo()
    {
        super();
    }
    public int area(){
        return this.width*this.height;
    }
        public int perimetro(){
        return 2*this.width+2*this.height;
    }
}
