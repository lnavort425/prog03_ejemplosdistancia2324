/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplos.apuntes.distancia;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

/*
Escribe el código necesario para crear dos objetos de tipo rectángulo (instancias de la clase Rectangle) y asignar su referencia (lo que devuelve el operador new tras invocarse al constructor) a las variables r1 y r2:

el primer rectángulo estará situado en la posición (1,1), tendrá una base de 5 y una altura de 3;
el segundo rectángulo estára ubicado en la posición (2,0), tendrá una base de 2 y una altura de 2.
*/

/**
 *
 * @author atecaiestrassierra
 */
public class EjemploRectangulo2 {
    public static void main(String[] args) {
        Rectangle r1,r2,r3,r4,r5,r6;
        Point p=new Point(1, 1);
        int i=9;
        if (i==9) System.out.println("i es 9");
        Dimension d=new Dimension(5, 3);
        r3=new Rectangle(p, d);
        r1=new Rectangle(1,1,5,3);
        r2=new Rectangle(2,0,2,2);
        r4=r1;
        r5=new Rectangle(r1);
        r6=new Rectangle(8,4);
        System.out.println("r1="+r1);
        System.out.println("r2="+r2);
        System.out.println("r3="+r3);
        System.out.println("r1==r3? "+(r1==r3));
        System.out.println("r1==r4? r1 y r4 son el mismo?"+(r1==r4));
        System.out.println("r1 es igual que r3?"+r1.equals(r3));
        System.out.println("r6="+r6.toString());
    }
}
