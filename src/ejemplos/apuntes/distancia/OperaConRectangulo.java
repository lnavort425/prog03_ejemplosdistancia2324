/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplos.apuntes.distancia;

import ejemplos.apuntes.distancia.Rectangulo;
import java.awt.Rectangle;
import java.lang.*;

/**
 *
 * @author luisnavarro
 */
public class OperaConRectangulo {
    public static void main(String[] args) {
        
        Rectangulo r=new Rectangulo();
        Rectangle r2=new Rectangle(1,1,2,3);
        Rectangulo r3=new Rectangulo(2,2,3,4);
        System.out.printf("r Mis coordenadas x=%d, y=%d",r.x,r.y);
        System.out.printf("r2 Mis coordenadas x=%d, y=%d",r2.x,r2.y);
        System.out.printf("r3 Mis coordenadas x=%d, y=%d, área=%d, perímetro=%d",r3.x,r3.y, r3.area(),r3.perimetro());
        
        ///
        int i=0;
        i=8;
        Integer i2;
        String s="Hola";
        Rectangle r4=null;
        if (r4==null) System.out.println("r4 es null");
        r4=new Rectangle(2, 2, 3, 6);
        System.out.println("r4 contiene punto (3,3)"+r4.contains(3, 3));
        r4.height=4;
        r4.getHeight();
        r4.setLocation(3, 3);
        r4.translate(0, 3);
        r4=null;
        r4=new Rectangle(1, 1, 2, 2);
        new Rectangle (0, 0, 10, 5);
        Rectangle r6=new Rectangulo(1, 1, 1, 1);
        r6.getWidth();
        r6.setLocation(2, 2);
        r6=r4;
        
        
    }    
}
