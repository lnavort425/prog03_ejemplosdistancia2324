/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplos.apuntes.distancia;

/**
 *
 * @author luisnavarro
 */
public class Rectangulo2Luis {

    int x1, y1, x2, y2;
    int b, h;

    public Rectangulo2Luis(int p1x, int p1y, int b, int h) {
        this.x1 = p1x;
        this.y1 = p1y;
        this.x2 = p1x + b;
        this.y2 = p1y + h;
        this.b = b;
        this.h = h;
    }

    public Rectangulo2Luis(int b, int h) {
        this(0, 0, b, h);
    }

    public int calculaSuperficie() {
        return b * h;
    }

    public boolean igual(Rectangulo2Luis r2) {
        return r2.x1 == this.x1
                && r2.y1 == this.y1
                && r2.b == this.b
                && r2.h == this.h;
    }

    public void setSize(int b, int h) {
        this.b = b;
        this.h = h;
    }

}
