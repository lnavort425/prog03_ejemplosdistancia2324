/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplos.apuntes.distancia;

import java.awt.Rectangle;

/**
 *
 * @author luisnavarro
 */
public class Ejemplo1b_OperaConRectangle {
    public static void main(String[] args) {
        Rectangle r1,r2,r3,r4,r5,r6,r7,r8,r9;
        r1=new Rectangle(0, 0,3,4);
        r2=new Rectangle(3,4);
        r3=r2;
        System.out.println("iguales r1 y r2:"+r1.equals(r2));
        System.out.println("El mismo r1 y r2?:"+(r1==r2));
        System.out.println("El mismo r3 y r2?:"+(r3==r2));
        ///Voy a ver las propiedades de un rectángulo
        System.out.println("r1="+r1);
        System.out.println("r1.altura="+r1.height);
        System.out.println("r1.altura="+r1.getSize());
        System.out.println("r1.localizacion="+r1.getLocation());
        System.out.println("r1.altura="+r1.height);
        
        
        // Comprobamos si los rectángulos contienen al punto (3,3)
        System.out.println("r1 contiene el punto (2,2): " + (r1.contains(2, 2) ? "sí" : "no"));
        System.out.println("r2 contiene el punto (3,3): " + (r2.contains(3, 3) ? "sí" : "no"));
        
        
        //Opero con 
        r4=new Rectangle(1,2,5,4);
        r5=new Rectangle(5,3,4,6);
        // Comprobamos si ambos rectángulos tienen alguna zona en común (su intersección es no nula)
System.out.println ("r4tiene alguna zona en común con r5: " + 
        (r4.intersects(r5) ? "sí":"no"));
        r6=r4.intersection(r5);
        r7=r4.union(r5);
        r8=r5.intersection(r4);
        System.out.println(r4+" interseccion "+r5+"="+r6);
        System.out.println(r4+" union "+r5+"="+r7);
        System.out.println("r8==r6 ?"+(r8==r6));
        System.out.println("r8 es igual que r6?"+r8.equals(r6));
        r9=new Rectangle(r4);
        r9.translate(1, 1);
        r8.setLocation(0, 0);
        System.out.println("clono r9 de r4 y lo muevo (1,1).....r9="+r9);
    }
    
}
