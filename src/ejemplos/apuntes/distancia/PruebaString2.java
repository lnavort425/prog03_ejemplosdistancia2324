/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplos.apuntes.distancia;

/**
 *
 * @author atecaiestrassierra
 */
public class PruebaString2 {

    public static void main(String[] args) {
        // Declaramos, instanciamos y asignamos una nueva cadena
        String miVar = new String("Hola");

// Mostramos su contenido en pantalla
        System.out.println("Cadena inicial: " + miVar);

// Y le ahora asignamos a la misma variable el resultado de aplicar el método concat sobre la cadena inicial
        miVar = miVar.concat(" Antonio");

// Mostramos el nuevo contenido de la referencia, que ahora apunta a un nuevo objeto String 
        System.out.println("Cadena tras la aplicación del método concat: " + miVar);

    }
}
