package ut8_ChipBiciXML;

import java.io.*;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.util.ArrayList;
import java.util.List;

public class ChipBiciXML {

    /**
     * Ruta del archivo donde se lee y escribe la colección de objetos ChipBici
     */
    private String rutaArchivo;
    /**
     * Objeto Xstream que permite la L/E con archivos XML
     */
    private XStream xstream;

    /**
     * Método constructor
     * @param archivo Ruta del archivo donde se lee y escribe la colección de objetos ChipBici
     */
    public ChipBiciXML(String nombreArchivo) {
        super();
        this.rutaArchivo = nombreArchivo;
        xstream = new XStream();
        //Permite asignar privilegios para poder operar con los archivos XML
        xstream.allowTypesByWildcard(new String[] { 
            "Ejercicio3.**",
            "ChipBiciXML.**",
            "com.mydomain.utilitylibraries.**"
        });
    }

    /**
     * Método que escribe, en un archivo binario, una colección de objetos ChipBici serializables.
     * @param bicis Lista de objetos ChipBici serializables para almacenar en el archivo binario.
     */
    public void escribir(List bicis) {
        String xml = xstream.toXML(bicis); // serialize to XML 
        File fArchivo = new File(rutaArchivo);
        try (
                 PrintWriter pwArchivo = new PrintWriter(fArchivo);) {

            pwArchivo.print(xml);
            pwArchivo.close();
        } catch (FileNotFoundException e) {
            System.out.println("Error: archivo " + rutaArchivo + "·no encontrado.");
        }
    }

    /**
     * Método que lee, desde un archivo binario, una colección de objetos ChipBici serializados.
     * @return Lista de objetos ChipBici que estaba almacenada en el archivo binario.
     */
    public List leer() {
        String xml = "";
        String cadena = "";

        List bicis = null;
        File fArchivo = new File(rutaArchivo);

        try (BufferedReader brArchivo = new BufferedReader(new FileReader(fArchivo));) {

            xml=brArchivo.readLine();
            while ((cadena = brArchivo.readLine()) != null) {
                xml = xml+"\n"+ cadena;
            }
            brArchivo.close();
            System.out.println(""+xml);
            bicis = (List<ChipBici>)xstream.fromXML(xml); // deserialize from XML 

        } catch (FileNotFoundException e) {
            System.out.println("Error: archivo " + rutaArchivo + "·no encontrado.");
        } catch (IOException e) {
            System.out.printf("Error de entrada/salida: %s\n", e.getMessage());
        }catch (Exception e){
                System.out.printf("Error de entrada/salida: %s\n", e.getMessage());
}
        return bicis;
    }
}
