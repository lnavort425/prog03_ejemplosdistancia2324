/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_ficheros2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import ut8_ficherosutil.Constantes;

/**
 *
 * @author atecaiestrassierra
 */
public class GeneradorInformes {

    public static void main(String[] args) {

        try {
            //1. Abro  el fichero de empleados y leo el de coeficientes, abro el de informes.txt
            //2. para cada linea
            // 2.1 leo como String y separa campos por ; con split , 
            // 2.2 instancio el empleado con los campos 
            // 2.3. Calculo su linea de informe y la imprimo 
            //3. cerrar todos los ficheros
            ArrayList<Empleado> empleados = new ArrayList();
            String rutaFicheroEmpleados = "recursos/empleados.txt";
            String rutaFicheroCoeficientes = "recursos/coeficientes.txt";
            String rutaFicheroInformes = "recursos/informes2.txt";

            FileReader fr1 = new FileReader(rutaFicheroCoeficientes);//"datos.txt");
            BufferedReader entradaCoeficientes = new BufferedReader(fr1);
            String linea = entradaCoeficientes.readLine();
            String[] coeficientes = linea.split(";");
            int pvn = Integer.valueOf(coeficientes[0]);
            int pvi = Integer.valueOf(coeficientes[1]);
            int sueldofijo = Integer.valueOf(coeficientes[2]);
            int minimoObjetivos = Integer.valueOf(coeficientes[3]);
            FileWriter fw = new FileWriter(rutaFicheroInformes);

            PrintWriter salida = new PrintWriter(fw);//"datos.txt");   //se crea el fichero
            String cadena;

            fr1.close();
            fr1 = new FileReader(rutaFicheroEmpleados);
            String[] camposEmpleado;
            BufferedReader entradaEmpleados = new BufferedReader(fr1);
            String lineaEmpleado;
            while ((lineaEmpleado = entradaEmpleados.readLine()) != null) {
                camposEmpleado = lineaEmpleado.split(";");
                Empleado e = new Empleado(camposEmpleado[0], camposEmpleado[1], Integer.valueOf(camposEmpleado[2]), Integer.valueOf(camposEmpleado[2]));
                empleados.add(e);
                int ventas = 0; //método dado
                boolean cumplidoObjetivos = false;//método dado
                salida.format("%-40s%-20s%-20s%-20s%-20s%-20s\n", camposEmpleado[0], camposEmpleado[1], camposEmpleado[2], camposEmpleado[3], ventas, cumplidoObjetivos);
            }
            fr1.close();
            fw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
