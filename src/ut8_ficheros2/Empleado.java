/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_ficheros2;

/**
 *
 * @author atecaiestrassierra
 */
public class Empleado {
    private String nombre;
    private String dni;
    private int ventasNacionales;
    private int ventasInternacionales;
    private int sueldo;
    private boolean cumpleObjetivos;
    
    public Empleado(String n, String d, int vn, int vi){
        nombre=n;
        dni=d;
        ventasNacionales=vn;
        ventasInternacionales=vi;
    }
    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the dni
     */
    public String getDni() {
        return dni;
    }

    /**
     * @param dni the dni to set
     */
    public void setDni(String dni) {
        this.dni = dni;
    }

    /**
     * @return the ventasNacionales
     */
    public int getVentasNacionales() {
        return ventasNacionales;
    }

    /**
     * @param ventasNacionales the ventasNacionales to set
     */
    public void setVentasNacionales(int ventasNacionales) {
        this.ventasNacionales = ventasNacionales;
    }

    /**
     * @return the ventasInternacionales
     */
    public int getVentasInternacionales() {
        return ventasInternacionales;
    }

    /**
     * @param ventasInternacionales the ventasInternacionales to set
     */
    public void setVentasInternacionales(int ventasInternacionales) {
        this.ventasInternacionales = ventasInternacionales;
    }

    /**
     * @return the sueldo
     */
    public int getSueldo() {
        return sueldo;
    }

    /**
     * @param sueldo the sueldo to set
     */
    public void setSueldo(int sueldo) {
        this.sueldo = sueldo;
    }

    /**
     * @return the cumpleObjetivos
     */
    public boolean isCumpleObjetivos() {
        return cumpleObjetivos;
    }

    /**
     * @param cumpleObjetivos the cumpleObjetivos to set
     */
    public void setCumpleObjetivos(boolean cumpleObjetivos) {
        this.cumpleObjetivos = cumpleObjetivos;
    }
}
