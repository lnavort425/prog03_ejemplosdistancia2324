/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package profesor.alumno.clase;

/**
 *
 * @author luisnavarro
 */
public class Principal {
    public static void main(String[] args) {
        Profesor profesor1 = new Profesor("Sr. Gonzalez", "Matemáticas");
        Profesor profesor2 = new Profesor("Sra. Ramirez", "Programación");

        Curso curso1 = new Curso("Matemáticas", profesor1);
        Curso curso2 = new Curso("Programación", profesor2);

        Estudiante estudiante1 = new Estudiante("Ana");
        Estudiante estudiante2 = new Estudiante("Luis");

        curso1.addEstudiante(estudiante1);
        curso1.addEstudiante(estudiante2);
        curso2.addEstudiante(estudiante1);

        estudiante1.setNota(curso1, 8.5);
        estudiante1.setNota(curso2, 9.0);
        estudiante2.setNota(curso1, 7.5);

        estudiante1.mostrarNotas();
        estudiante2.mostrarNotas();

        Curso.listarTodosLosCursos();

        System.out.println("Total de profesores: " + Profesor.getTotalProfesores());
        System.out.println("Total de estudiantes: " + Estudiante.getTotalEstudiantes());
    }
}