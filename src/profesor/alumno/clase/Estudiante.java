/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package profesor.alumno.clase;

/**
 *
 * @author luisnavarro
 */
import java.util.HashMap;
import java.util.ArrayList;

public class Estudiante {
    private String nombre;
    private HashMap<Curso, Double> notas = new HashMap<>();
    private ArrayList<Curso> cursos = new ArrayList<>();
    private static int totalEstudiantes = 0;

    public Estudiante(String nombre) {
        this.nombre = nombre;
        totalEstudiantes++;
    }

    public void addCurso(Curso curso) {
        cursos.add(curso);
    }

    public void setNota(Curso curso, double nota) {
        notas.put(curso, nota);
    }

    public double getNota(Curso curso) {
        return notas.get(curso);
    }

    public String getNombre() {
        return nombre;
    }

    public static int getTotalEstudiantes() {
        return totalEstudiantes;
    }

    public void mostrarNotas() {
        System.out.println("Notas de " + nombre + ":");
        for (Curso curso : notas.keySet()) {
            System.out.println(curso.getNombre() + ": " + notas.get(curso));
        }
    }
}