/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package profesor.alumno.clase;

/**
 *
 * @author luisnavarro
 */
import java.util.ArrayList;

public class Curso {
    private String nombre;
    private Profesor profesor;
    private ArrayList<Estudiante> estudiantes = new ArrayList<>();
    private static ArrayList<Curso> todosLosCursos = new ArrayList<>();

    public Curso(String nombre, Profesor profesor) {
        this.nombre = nombre;
        this.profesor = profesor;
        todosLosCursos.add(this);
    }

    public void addEstudiante(Estudiante estudiante) {
        estudiantes.add(estudiante);
    }

    public String getNombre() {
        return nombre;
    }

    public Profesor getProfesor() {
        return profesor;
    }

    public static void listarTodosLosCursos() {
        System.out.println("Todos los cursos:");
        for (Curso curso : todosLosCursos) {
            System.out.println(curso.getNombre() + ", Profesor: " + curso.getProfesor().getNombre());
        }
    }
}