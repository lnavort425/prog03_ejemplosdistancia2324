/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package profesor.alumno.clase;

/**
 *
 * @author luisnavarro
 */
public class Profesor {
    private String nombre;
    private String materia;
    private static int totalProfesores = 0;

    public Profesor(String nombre, String materia) {
        this.nombre = nombre;
        this.materia = materia;
        totalProfesores++;
    }

    public String getNombre() {
        return nombre;
    }

    public String getMateria() {
        return materia;
    }

    public static int getTotalProfesores() {
        return totalProfesores;
    }
}