/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ut8_retocaArbolXML;

//import ChipBici.ChipBici;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.StringWriter;
import java.util.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;

/**
 * Programa para trabajar con arbol DOM.
 *
 * @profesorado
 */
public class retocaArbolXML {

    /**
     * Método principal.
     */
    public static void main(String[] args) {

        //----------------------------------------------
        //          Declaración de variables 
        //----------------------------------------------
        // Constantes
        // Variables de entrada        
        // Variables de salida
        // Variables auxiliares
        //String ruta = System.getProperty("user.dir") + "/recursos/BDBicis.xml";
        String ruta = ut8_ficheros1.Token.RUTA_SALIDA + "BDBicis.xml";
        try {
            //----------------------------------------------
            //                Abrir Archivo 
            //----------------------------------------------

            // 1º Creamos una nueva instancia de un fábrica de constructores de documentos.
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            // 2º A partir de la instancia anterior, fabricamos un constructor de documentos, que procesará el XML.
            DocumentBuilder db = dbf.newDocumentBuilder();
            // 3º Procesamos el documento (almacenado en un archivo) y lo convertimos en un árbol DOM.
            Document documento = db.parse(ruta);
            //                 Convertir a DOM 
            documento.getDocumentElement().normalize();

            //retocaArbolXMLmetodoChatgpt(documento);
            retocaArbolXMLmiMetodo(documento);

            FileWriter ficheroSalida;
            ficheroSalida = new FileWriter(new File(ruta + "Out4.xml"));
            // Escribo el array en el fichero
            //documento.getDocumentElement().normalize();
            DOMSource domSource = new DOMSource(documento);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);

            TransformerFactory tf = TransformerFactory.newInstance();
            tf.setAttribute("indent-number", 2);

            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.transform(domSource, result);
            String xml = writer.toString();
            ficheroSalida.write(xml);

            // Cerramos el fichero
            ficheroSalida.close();
        } catch (FileNotFoundException fnfe) {
            System.err.println("El fichero " + ruta + " no existe.");
        } catch (IOException ioe) {
            System.err.println("Error: falló la escritura en el fichero " + ruta);
        } catch (Exception ex) {
            System.out.println("¡Error! No se ha podido cargar el documento XML.");
        }

        System.out.println();
        System.out.println("Archivo cerrado y procesamento finalizado");
        System.out.println("---------");

        System.out.println();
        System.out.println("Fin del programa.");
    }

    public static void retocaArbolXMLmiMetodo(Document documento) {

        Element nodo = documento.getDocumentElement();
        // Lista de nodos de aero generadores
        NodeList nodeList = nodo.getChildNodes();
        // Avanzamos por la lista para presentar los conceptos
        for (int temp = 0; temp < nodeList.getLength(); temp++) {
            // Obtenemos un nodo
            Node nodoBici = nodeList.item(temp);
            // Verificamos que el nodo sea un elemento, para prevenir errores
            if (nodoBici.getNodeType() == Node.ELEMENT_NODE) {
                // Convertimos de Node a elemento
                Element elementoBici = (Element) nodoBici;
                NodeList nodeListIn = elementoBici.getChildNodes();
                Boolean cambio = false;
//                     Presentamos los datos de cada elemento del areogen
                for (int u = 0; u < nodeListIn.getLength(); u++) {
                    Node nodoElement = nodeListIn.item(u);
                    if (nodoElement.getNodeType() == Node.ELEMENT_NODE) {
                        if (nodoElement.getNodeName().equals("revision")) {
                            elementoBici.removeChild(nodoElement);
                            
                        }
                    }
                }

                Element elementoColor = documento.createElement("color");
                elementoColor.setTextContent("Blanco");
                elementoBici.appendChild(elementoColor);
            }
        }
    }

    public static void retocaArbolXMLmetodoChatgpt(Document doc) {
        // Obtener una lista de todos los elementos "distancia" del documento
        NodeList distanciaNodes = doc.getElementsByTagName("revision");
        List<Node> nodesToRemove = new ArrayList<>();

        // Eliminar cada elemento "distancia" de la lista
        for (int i = 0; i < distanciaNodes.getLength(); i++) {
            Node node = distanciaNodes.item(i);
            nodesToRemove.add(node);
        }

        for (Node node : nodesToRemove) {
            node.getParentNode().removeChild(node);
        }

        // Crear un nuevo elemento "color" con el valor "blanco"
        Element colorElement = doc.createElement("color");
        colorElement.appendChild(doc.createTextNode("blanco"));

        // Añadir el nuevo elemento "color" a cada elemento del documento
        NodeList chipBiciNodes = doc.getElementsByTagName("ChipBici");

        for (int i = 0; i < chipBiciNodes.getLength(); i++) {
            Element chipBiciElement = (Element) chipBiciNodes.item(i);
            chipBiciElement.appendChild(colorElement.cloneNode(true));
            System.out.println("algo hace con ChipBici");
        }
         chipBiciNodes = doc.getElementsByTagName("ChipBiciXML.ChipBici");

        for (int i = 0; i < chipBiciNodes.getLength(); i++) {
            Element chipBiciElement = (Element) chipBiciNodes.item(i);
            chipBiciElement.appendChild(colorElement.cloneNode(true));
            System.out.println("algo hace con ChipBiciXML");
        }

    }
}
