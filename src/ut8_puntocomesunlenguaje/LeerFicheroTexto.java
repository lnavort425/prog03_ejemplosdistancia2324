/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_puntocomesunlenguaje;

/**
 *
 * @author luisnavarro
 */
    

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LeerFicheroTexto {

    public static void main(String[] args) {
 
        FileReader fr = null;
        try {
            fr = new FileReader(ut8_ficherosutil.Constantes.RUTA_SALIDA+"datos.txt");
            BufferedReader entrada = new BufferedReader(fr);
            int car = entrada.read();         //se lee el primer carácter del fichero
            while (car != -1) {               //mientras no se llegue al final del fichero                        
                System.out.print((char) car); //se nuestra por pantalla
                car = entrada.read();         //se lee el siguiente carácter del fichero                          
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (fr != null) {
                    fr.close();
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());                                                               
            }
        }
    }
}