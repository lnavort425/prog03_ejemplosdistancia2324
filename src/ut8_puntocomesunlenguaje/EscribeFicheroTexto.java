/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_puntocomesunlenguaje;


import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
public class EscribeFicheroTexto {

    public static void main(String[] args) {
 
        Scanner sc = new Scanner(System.in);
        PrintWriter salida = null;
  
        try {
            salida = new PrintWriter(ut8_ficherosutil.Constantes.RUTA_SALIDA+"datos.txt");   //se crea el fichero
            String cadena;
            System.out.println("Introduce texto. Para acabar introduce la cadena FIN:");
            cadena = sc.nextLine();                             //se introduce por teclado una cadena de texto    
            while (!cadena.equalsIgnoreCase("FIN")) {
                salida.println(cadena);                         //se escribe la cadena en el fichero
                cadena = sc.nextLine();                         //se introduce por teclado una cadena de texto    
            }
            salida.flush();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());                                                                   
        } finally {         
            salida.close();
        }
    }
}
