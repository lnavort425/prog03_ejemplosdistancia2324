/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplo15oct23;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Impresión de la fecha
 *
 * @author JJBH
 */
public class Fechas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        LocalDate ahora = LocalDate.now();
        System.out.println(ahora);

        // Crea un objeto LocalDate
        LocalDate lt1 = LocalDate.parse("2019-12-26");

        // Escribe el resultado
        System.out.println("LocalDate : " + lt1);
        LocalDate lt2 = lt1.plusWeeks(4);
        System.out.println("es anterior" + lt2 + " a " + lt1 + "?" + lt2.isBefore(lt1));

        // El método of devuelve una nueva instancia de LocalTime con la hora,
        // minutos, segundos y nanosegundos.
        // Se lanza una excepción DateTimeExcepction si se proporciona algún
        // parámetro no válido
        LocalTime hora = LocalTime.of(20, 30, 45, 35);

        System.out.println(hora.toString());

        // Devuelve la instancia de la hora local del reloj del sistema
        System.out.println(LocalTime.now());

        // Crea un objeto LocalTime
        LocalTime localt = LocalTime.parse("10:25:45");

        // Escribe el resultado 
        System.out.println("LocalTime : " + localt);
        // Crea un objeto LocalDateTime
        LocalDateTime ltt = LocalDateTime.parse("2020-11-28T19:34:50.63");

        // Escribe resultado
        System.out.println("LocalDateTime : " + ltt);

        LocalDateTime fecha = LocalDateTime.now();

        DateTimeFormatter isoFecha = DateTimeFormatter.ISO_LOCAL_DATE;
        System.out.println("Sin formato: " + fecha);
        System.out.println("Con formato: " + fecha.format(isoFecha));

        DateTimeFormatter isoHora = DateTimeFormatter.ISO_LOCAL_TIME;
        System.out.println(fecha.format(isoHora));

        hora = LocalTime.now();
        DateTimeFormatter f = DateTimeFormatter.ofPattern("'Son las' h 'y' mm");
        //System.out.println(hora);
        System.out.println(hora.format(f));

        LocalDateTime otraHora = LocalDateTime.now();
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("hh:MM");
        System.out.println("Hora:mes = " + otraHora.format(formato));
        DateTimeFormatter formato2 = DateTimeFormatter.ofPattern("hh:mm");
        System.out.println("Hora:minutos = " + otraHora.format(formato2));
    }

}
