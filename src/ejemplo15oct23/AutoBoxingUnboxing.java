/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplo15oct23;

/**
 *
 * @author luisnavarro
 */
public class AutoBoxingUnboxing {
    
    public static void main(String[] args) { 

	// Contador de ciclos como objeto con asignacion primitivo
	Integer contador = 0;
	Integer i2=i2= Integer.valueOf("2345");//new Integer("2345");
	Integer i3=i2= Integer.valueOf(2345);//new Integer(2345);	// Parametros
	double kilometros = 2.5; 
	int tiempo = 2;
        //i2= Integer.getInteger("2345");
        i2= Integer.valueOf("2345");
        i2=Integer.valueOf(23);
        int n1=i2.intValue();
        int n2=i3;
        int n3=Integer.max(n1, n2);

        System.out.println(i2+"="+2345+"="+n2);
        System.out.println(i2+"="+2345+n2);
        System.out.println(i2+"="+2345+i2);      
        System.out.println(i2+"="+(2345+i2));
        System.out.println(i2+"="+(2345+n2));

        System.out.println(Integer.toBinaryString(45)+"-"+Integer.MAX_VALUE);

                    System.out.println(""+contador.toBinaryString(45));

	while(true) { 
	    // Utilizando auto-incremento sobre objeto Integer
	    System.out.print("Vuelta: " + contador++  + " "); 
	    // Operacion entre objeto Integer y primitivo double
	    System.out.print("Distancia: " + contador*kilometros + " ");
	    // Operacion entre objeto Integer y primitivo int
	    System.out.println("Tiempo: " + contador*tiempo + " ");
	    
	    // Comparacion sobre objeto Integer
	    if ( contador > 5) break; 
	}



    }
}
