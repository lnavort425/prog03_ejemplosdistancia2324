/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplo15oct23;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author luisnavarro
 */
public class LeeEntradaSystem {

    public static void main(String[] args) {
        //System.in instancia de InputStream: bytes
        //InputStreamReader convierte en caracteres
        //br lee línea
        try {
            InputStreamReader isr = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(isr);
            System.out.println("Dime cadena y numero:");
            String cadena = br.readLine();
            int numero = Integer.parseInt(br.readLine());
            System.out.println("cadena+numero:" + cadena + numero);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
