/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplo15oct23;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Probando el formateo en el tiempo, con Java
 * @author jjber
 */
public class Formateando {
        final static String formatoDeMiHora="dd/MM/yyyy hh:mm:ss";  
        final static DateTimeFormatter formatoDeMiHora2 = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss");
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

       
        LocalDateTime fecha = LocalDateTime.now();
        
        DateTimeFormatter isoFecha = DateTimeFormatter.ISO_LOCAL_DATE;
        System.out.println("Sin formato: " + fecha);
        System.out.println("Con formato: " + fecha.format(isoFecha));
      
        DateTimeFormatter isoHora = DateTimeFormatter.ISO_LOCAL_TIME;
        System.out.println(fecha.format(isoHora));
 
                DateTimeFormatter isoHora2 = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
        System.out.println(fecha.format(isoHora2));

        LocalTime hora = LocalTime.now();
        DateTimeFormatter f = DateTimeFormatter.ofPattern("'Son las' h 'y' mm");
        //System.out.println(hora);
        System.out.println(hora.format(f));
        
        
        LocalDateTime otraHora = LocalDateTime.now();
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("hh:MM");
        System.out.println("Hora:mes = " + otraHora.format(formato));
        DateTimeFormatter formato2 = DateTimeFormatter.ofPattern("hh:mm");
        System.out.println("Hora:minutos = " + otraHora.format(formato2));
    }
    
}