/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplo15oct23;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author luisnavarro
 */
public class claseSystem_entradaSalida {

    public static void main(String[] args) {
        try {
            InputStreamReader isr = new InputStreamReader(System.in);
            System.out.println("mete untexto, luego un entero");
            BufferedReader br = new BufferedReader(isr);
            String cadena = br.readLine();

            int numero = Integer.parseInt(br.readLine());
            System.out.println("hemos escrito:" + cadena + numero);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        catch (NumberFormatException nfe){
            nfe.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
