/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplo15oct23;

/**
 *
 * @author atecaiestrassierra
 */
public class EjemplosPrintf {

    public static void main(String[] args) {

        int a = 5, b = 15, c = 255;

        System.out.printf("a = %d b = %x c = %o", a, b, c);

        double x = 27.5, y = 33.75;

        System.out.printf("x = %f y = %g", x, y);

        System.out.printf("a = %3$d b = %1$x c = %2$o", a, b, c);
        int count = 0;
        for (int ch = 'a'; ch <= 'z'; ch++) {
            System.out.printf("      %1$4c%1$4x", ch);
            if (++count % 6 == 0) {
                System.out.printf("%n");
            }
            System.out.printf("      %1$4c%<4x", ch);
        }

    }
}
