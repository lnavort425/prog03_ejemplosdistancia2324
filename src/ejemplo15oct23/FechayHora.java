package ejemplo15oct23;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * Clase de ejmplo para mostrar algunos métodos de la clase LocalDateTime
 * @author JJBH
 */
public class FechayHora {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // Construir un LocalDateTime a partir de un LocalDate y un LocalTime
        LocalDate fecha = LocalDate.of(1989, 10, 21);
        LocalTime hora = LocalTime.now();
        LocalDateTime dateTime = LocalDateTime.of(fecha, hora);
        // Y escribirlo por consola
        System.out.printf("La hora es: %s%n", dateTime.toString()); 
        
        // Probamos a sumar y restar.
        LocalDateTime ahora = LocalDateTime.now(); 
        System.out.printf("La hora es: %s%n", ahora);   
        System.out.printf("Hace seis meses sería %s%n", LocalDateTime.now().minusMonths(6));  
        System.out.printf("La hora más 20 horas más sería: %s%n", ahora.plusHours(20));      
        System.out.printf("Y hace dos días: %s%n", ahora.minusDays(2)); 
        
        LocalTime localt = LocalTime.parse("10:25:45"); 
  
        // Escribe el resultado 
        System.out.println("LocalTime : " + localt); 
    }
    
}
