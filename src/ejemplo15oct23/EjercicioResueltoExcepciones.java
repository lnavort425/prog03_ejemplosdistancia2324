package ejemplo15oct23;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Ejercicio resuelto sobre manejo de excepciones. El programa solicita que el
 * usuario introduzca por teclado un n�mero entre 0 y 100, debiendo gestionarse
 * la entrada por medio de excepciones.
 *
 * @author JJBH
 */
public class EjercicioResueltoExcepciones {

    public static void main(String[] args) {
        int numero = -1;
        int intentos = 0;

        do {
            try {
                System.out.print("Introduzca un n�mero entre 0 y 100: ");
                Scanner teclado = new Scanner(System.in);
                numero = teclado.nextInt();
                if (numero < 0 || numero > 100) {
                    System.out.print("El n�mero introducido no est� entre 0 y 100.\n");
                }
                                intentos++;
                System.out.println("100/" + numero + "=" + 100 / numero);
/*            } catch (ArithmeticException ex) {
                System.err.println("Error de tipo matemático.");
*/
            } catch (InputMismatchException ex) {
                System.err.println("Me tiene que poner un numero entero.");
            /*  } catch (Exception e) {
                e.printStackTrace();
                System.out.println("" + e.getMessage());
*/
          } finally {
                intentos++;
            }
        } while (numero < 0 || numero > 100);

        System.out.println("El n�mero introducido es: " + numero);
        System.out.println("N�mero de intentos: " + intentos);
    }
}
