/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplo15oct23;

/**
 *
 * @author luisnavarro
 */

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.temporal.ChronoUnit;

/**
 * CronoUnit ejemplo.
 * @author Profesor
 */
public class EjemploCrono {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // Fecha del cumpleaños
        LocalDate cumple = LocalDate.of(1982, Month.NOVEMBER, 19);
        // Fecha de hoy
        LocalDate fechaHoy = LocalDate.now();
        
        // El método withYear devuelve una nueva copia de esta fecha con el 
        // campo 'year' cambiado al que se pasa como parámetro.
        LocalDate proximoCumple = cumple.withYear(fechaHoy.getYear());

        // Si tu cumple ha ocurrido ya este año, añadir al año
        if (proximoCumple.isBefore(fechaHoy) || proximoCumple.isEqual(fechaHoy)) {
            proximoCumple = proximoCumple.plusYears(1);
        }

        // Con la clase Period se puede obtener la diferencia entre dos fechas
        // o utilizarlo para modificar valores de alguna fecha
        Period periodo = Period.between(fechaHoy, proximoCumple);
        
        // Calcular la diferencia entre la feha de hoy y la del próximo cumple
        long per2 = ChronoUnit.DAYS.between(fechaHoy, proximoCumple);
        System.out.println("Faltan " + periodo.getMonths() + " meses, y " +
                   periodo.getDays() + " días hasta tu próximo cumpleaños. (" +
                   per2 + " días en total)");
    
    }
}