/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplo15oct23;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Clase para entender la captura de excepciones.
 *
 * @author JJBH
 */
public class EjemploExcepcion {
    public static void main(String[] args){
        
                
        System.out.println("Escriba un número entero: ");
        try {
            Scanner teclado = new Scanner(System.in) ;
            int numero = teclado.nextInt() ;
            System.out.println("El número tecleado es: " + numero);
        
        } catch (InputMismatchException ex2) {
            System.err.println("Error: formato no válido.");
        }  
    }
}