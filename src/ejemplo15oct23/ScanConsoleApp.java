/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplo15oct23;

import java.io.*;
import java.util.*;

/** Demonstrate the Scanner class for input of numbers.**/
public class ScanConsoleApp
{
  public static void main (String arg[]) {

    // Create a scanner to read from keyboard
    Scanner scanner = new Scanner (System.in);

    try {
      System.out.printf ("Input int (e.g. %4d): ",3501);
      int int_val = scanner.nextInt ();
      System.out.println (" You entered " + int_val +"\n");

      System.out.printf ("Input float (e.g. %5.2f): ", 2.43);
      float float_val = scanner.nextFloat ();
      System.out.println (" You entered " + float_val +"\n");

      System.out.printf ("Input double (e.g. %6.3e): ",4.943e15);
      double double_val = scanner.nextDouble ();
      System.out.println (" You entered " + double_val +"\n");

    }
    catch  (InputMismatchException e) {
      System.out.println ("Mismatch exception:" + e );
    }
  } // main

} // class ScanConsoleApp