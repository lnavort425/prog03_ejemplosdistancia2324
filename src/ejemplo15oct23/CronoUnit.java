/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplo15oct23;


import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * Ejemplo CronoUnit
 * @author Profe
 */
public class CronoUnit {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // Fecha actual
        LocalDate hoy = LocalDate.now();
        System.out.println("Fecha actual: " + hoy);
        
        // Añadir una semana a la fecha actual
        LocalDate semanaSig = hoy.plus(1, ChronoUnit.WEEKS);
        System.out.println("Una semana después: " + semanaSig);	   
	   
        // Añadir un mes a la fecha actual
        LocalDate mesSig = hoy.plus(1, ChronoUnit.MONTHS);
        System.out.println("Un mes después: " + mesSig);	  
	
        // Añadir un año
        LocalDate sigYear = hoy.plus(1, ChronoUnit.YEARS);
        System.out.println("Un año después: " + sigYear);

        // Añadir una década
        LocalDate sigDecada = hoy.plus(1, ChronoUnit.DECADES);
        System.out.println("Una década después: " + sigDecada);	  
        
    }
    
}