/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejemplo15oct23;

import static java.lang.Math.random;
import static java.lang.Math.cos;
//import java.lang.Math;
import java.time.LocalDateTime;
import java.util.Random;

/**
 *
 * @author luisnavarro
 */
public class PruebaRandom {
//En el import anterior round es un método de la conocida clase Math. Esto significa que podremos usarlo en nuestro código directamente, veamos un ejemplo:

    public static void main(String[] args) {
        System.out.println(random() * 10);
        System.out.println("cos/45)=" + cos(45));
        //System.out.println(random()*10);
        int pi = (int) Math.PI;
        double raizDos = Math.sqrt(2);
        Random semilla = new Random(System.currentTimeMillis());
        int aleatorio = semilla.nextInt(10, 20);
        for (int i = 0; i < 10; i++) {
            System.out.println("n=" + semilla.nextDouble(10, 20));
            System.out.println("=" +(20+( Math.random() * 20)));
        }

    }
}
