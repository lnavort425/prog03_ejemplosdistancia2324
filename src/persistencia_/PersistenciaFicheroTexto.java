/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package persistencia_;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author atecaiestrassierra
 *  
 * comentar orden de los campos #
 */
public class PersistenciaFicheroTexto extends PersistenciaFichero{
        private static String SEPARADOR_CAMPOS_ALUMNO="#";

    public PersistenciaFicheroTexto(String r) {
        super(r);
    }
        
        public ArrayList<Alumno> cargarDatos(){
            ArrayList<Alumno> listaAlumno=new ArrayList<Alumno>();//new ArrayList<Alumno>();
            //this.rutaFichero;//Binario pues lo heredo PersistenciaFichero pero sé que soy binario
            //Abrir el fichero binario, flujo de bytes hacer ObjectInputStream, flujo de objetos y leer un objeto....serializable
 
        FileReader fr = null;
  
        try {
            fr = new FileReader(this.rutaFichero);
            BufferedReader entrada = new BufferedReader(fr);
            String cadena = entrada.readLine();    //se lee la primera línea del fichero
            while (cadena != null) {               //mientras no se llegue al final del fichero                   
                //En cadena se supone que tengo los 4 campos atributos del alumno, separados por un separador
                //En una linea del fichero tendré: 45#Pepe#Pérez#true ---> 0:45 1:Pepe....
                String[] atributosAlumno=cadena.split(SEPARADOR_CAMPOS_ALUMNO);
                int id=Integer.getInteger(atributosAlumno[0]);
                boolean sexo=Boolean.getBoolean(atributosAlumno[3]);
                Alumno alumno=new Alumno(id,atributosAlumno[1],atributosAlumno[2],sexo);// Boolean.
                listaAlumno.add(alumno);
                cadena = entrada.readLine();       //se lee la siguiente línea del fichero                        
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (fr != null) {
                    fr.close();
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());                                                               
            }
        }
        
        return listaAlumno;
    }

       
   // public void guardarDatos(Recetario);
    public void guardarDatos(ArrayList<Alumno> al){
       
        String cadena;
        try (FileWriter fw = new FileWriter(this.rutaFichero); //try with resources
             PrintWriter salida = new PrintWriter(fw)){
              for (Alumno a : al) {
                     salida.println(""+a.getId()+SEPARADOR_CAMPOS_ALUMNO+a.getNombre()+SEPARADOR_CAMPOS_ALUMNO+a.getApellidos()+SEPARADOR_CAMPOS_ALUMNO+a.isEsHombre());
             }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());                                                                  
        }
    }
}
