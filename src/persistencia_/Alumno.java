/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package persistencia_;

import java.io.Serializable;

/**
 *
 * @author atecaiestrassierra
 */
public class Alumno implements Serializable {
    
    private static final long serialVersionUID = 1L;

    private int id;
    private String nombre;
    private String apellidos;
    private boolean esHombre;//true si es hombre
    public Alumno(  int id,     String nombre,     String apellidos,     boolean esHombre){
        this.id=id;
        this.nombre=nombre;
        this.apellidos=apellidos;
        this.esHombre=esHombre;
               
    }
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the apellidos
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * @param apellidos the apellidos to set
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * @return the esHombre
     */
    public boolean isEsHombre() {
        return esHombre;
    }

    /**
     * @param esHombre the esHombre to set
     */
    public void setEsHombre(boolean esHombre) {
        this.esHombre = esHombre;
    }
    
}
