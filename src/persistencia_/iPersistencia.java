/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package persistencia_;

import java.util.ArrayList;

/**
 *
 * @author atecaiestrassierra
 */
public interface iPersistencia {
    public ArrayList<Alumno> cargarDatos();
   // public void guardarDatos(Recetario);
    public void guardarDatos(ArrayList<Alumno> al);
}
