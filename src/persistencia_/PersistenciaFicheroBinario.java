/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package persistencia_;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author atecaiestrassierra
 */
public class PersistenciaFicheroBinario extends PersistenciaFichero{

    public PersistenciaFicheroBinario(String r) {
        super(r);
    }
    
    
        public ArrayList<Alumno> cargarDatos(){
            ArrayList<Alumno> listaAlumno=null;//new ArrayList<Alumno>();
            //this.rutaFichero;//Binario pues lo heredo PersistenciaFichero pero sé que soy binario
            //Abrir el fichero binario, flujo de bytes hacer ObjectInputStream, flujo de objetos y leer un objeto....serializable
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(this.rutaFichero))) {
            listaAlumno = (ArrayList<Alumno>) ois.readObject();
        } catch (IOException | ClassNotFoundException| ClassCastException e ) {
            System.out.println(""+e.getMessage());
            e.printStackTrace();
        }

            return listaAlumno;
        }
   // public void guardarDatos(Recetario);
    public void guardarDatos(ArrayList<Alumno> al){
         try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(this.rutaFichero))) {
            oos.writeObject(al); //Escribo una lista de alumnos, q es serializable
        } catch (IOException e) {
            System.out.println("Ha habido un error al guardar lista de alumnos en fichero binario:"+e.getMessage());
            e.printStackTrace();
        }
       
    }
}
