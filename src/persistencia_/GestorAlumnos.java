/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package persistencia_;

/**
 *
 * @author luisnavarro
 */
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class GestorAlumnos {

    /**
     * Nombre del archivo de base de datos local.
     */
    private static final String DB_NAME = "proyectobase.h2db";
    /**
     * URL para la conexión a la base de datos.
     */
    /**
     * Opciones de conexión.
     */
    private static final String CU_PARAMS = ";MODE=MySQL;AUTO_RECONNECT=TRUE";
    private static final String CONNECTION_URL = "jdbc:h2:./" + DB_NAME + CU_PARAMS;
    private static final String usuario = "";
    private static final String clave = "";
    private static final String tabla = "ALUMNO";
    /**
     * Driver a utilizar para conectarse a la base de datos.
     */

    private static Scanner scanner = new Scanner(System.in);
    private static ArrayList<Alumno> listaAlumnos = new ArrayList<>();
    private static PersistenciaFicheroBinario persistenciaBinaria = new PersistenciaFicheroBinario("alumnos.bin");
    private static PersistenciaFicheroTexto persistenciaTexto = new PersistenciaFicheroTexto("alumnos.txt");
    private static PersistenciaJDBC persistenciaTabla = new PersistenciaJDBC(CONNECTION_URL, usuario, clave, tabla);

    public static void main(String[] args) {
        int opcion;
        do {
            System.out.println("\n--- Menú de Gestión de Alumnos ---");
            System.out.println("1. Agregar un alumno");
            System.out.println("2. Guardar alumnos en fichero binario");
            System.out.println("3. Cargar alumnos desde fichero binario");
            System.out.println("4. Guardar alumnos en fichero texto");
            System.out.println("5. Cargar alumnos desde fichero texto");
            System.out.println("6. Guardar alumnos en tabla Alumno");
            System.out.println("7. Cargar alumnos desde tabla Alumno");
            System.out.println("8. Salir");
            System.out.print("Seleccione una opción: ");
            opcion = Integer.parseInt(scanner.nextLine());

            switch (opcion) {
                case 1:
                    agregarAlumno();
                    break;
                case 2:
                    guardarAlumnosFicheroBinario(listaAlumnos);
                    break;
                case 3:
                    listaAlumnos = cargarAlumnosFicheroBinario();
                    break;
                case 4:
                    guardarAlumnosFicheroTexto(listaAlumnos);
                    break;
                case 5:
                    listaAlumnos = cargarAlumnosFicheroTexto();
                    break;
                case 6:
                    guardarAlumnosTablaAlumno(listaAlumnos);
                    break;
                case 7:
                    listaAlumnos = cargarAlumnosTablaAlumno();
                    break;
                case 8:
                    System.out.println("Saliendo del sistema...");
                    break;
                default:
                    System.out.println("Opción inválida, por favor intente de nuevo.");
            }
        } while (opcion != 4);
    }

    private static void agregarAlumno() {
        System.out.print("Ingrese ID del alumno: ");
        int id = Integer.parseInt(scanner.nextLine());
        System.out.print("Ingrese nombre del alumno: ");
        String nombre = scanner.nextLine();
        System.out.print("Ingrese apellidos del alumno: ");
        String apellidos = scanner.nextLine();
        System.out.print("Ingrese sexo del alumno (true para masculino, false para femenino): ");
        boolean sexo = Boolean.parseBoolean(scanner.nextLine());
        listaAlumnos.add(new Alumno(id, nombre, apellidos, sexo));
        System.out.println("Alumno agregado correctamente.");
    }

    private static void guardarAlumnosFicheroBinario(ArrayList<Alumno> l) {
        persistenciaBinaria.guardarDatos(listaAlumnos);
        System.out.println("Alumnos guardados en fichero binario correctamente.");
    }

    private static ArrayList<Alumno> cargarAlumnosFicheroBinario() {
        ArrayList<Alumno> lista = persistenciaBinaria.cargarDatos();
        System.out.println("Alumnos cargados desde fichero binario.");
        return lista;
    }

    private static void guardarAlumnosFicheroTexto(ArrayList<Alumno> l) {
        persistenciaBinaria.guardarDatos(listaAlumnos);
        System.out.println("Alumnos guardados en fichero binario correctamente.");
    }

    private static ArrayList<Alumno> cargarAlumnosFicheroTexto() {
        ArrayList<Alumno> lista = persistenciaTexto.cargarDatos();
        System.out.println("Alumnos cargados desde fichero binario.");
        return lista;
    }

    private static void guardarAlumnosTablaAlumno(ArrayList<Alumno> l) {
        persistenciaBinaria.guardarDatos(l);
        System.out.println("Alumnos cargados desde fichero binario.");
    }

    private static ArrayList<Alumno> cargarAlumnosTablaAlumno() {
        ArrayList<Alumno> lista = persistenciaBinaria.cargarDatos();
        System.out.println("Alumnos cargados desde fichero binario.");
        return lista;
    }
}
