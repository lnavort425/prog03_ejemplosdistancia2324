package persistencia_;

import java.util.ArrayList;
import java.util.Scanner;
import persistencia_.PersistenciaFicheroBinario;
import persistencia_.PersistenciaFicheroTexto;
import persistencia_.PersistenciaJDBC;

public class GestorListaAlumnos {

    private ArrayList<Alumno> alumnos;
    private Scanner scanner;

    public GestorListaAlumnos() {
        alumnos = new ArrayList<>();
        scanner = new Scanner(System.in);
    }

    public void mostrarMenu() {
        String opcion;
        do {
            ES2.msg("\nGestión de Alumnos");
            ES2.msg("1. Añadir Alumno");
            ES2.msg("2. Cargar Alumnos desde fichero binario");
            ES2.msg("3. Cargar Alumnos desde fichero de texto");
            ES2.msg("4. Cargar Alumnos desde base de datos");
            ES2.msg("5. Guardar Alumnos en fichero binario");
            ES2.msg("6. Guardar Alumnos en fichero de texto");
            ES2.msg("7. Guardar Alumnos en base de datos");
            ES2.msg("8. Salir");
            System.out.print("Seleccione una opción: ");
            opcion = scanner.nextLine();

            switch (opcion) {
                case "1":
                    agregarAlumno();
                    break;
                case "2":
                    cargarAlumnos(new PersistenciaFicheroBinario("alumnos.dat"));
                    break;
                case "3":
                    cargarAlumnos(new PersistenciaFicheroTexto("alumnos.txt"));
                    break;
                case "4":
                    cargarAlumnos(new PersistenciaJDBC("jdbc:h2:./proyectobase.h2db", "", "", "ALUMNOS"));
                    break;
                case "5":
                    guardarAlumnos(new PersistenciaFicheroBinario("alumnos.dat"));
                    break;
                case "6":
                    guardarAlumnos(new PersistenciaFicheroTexto("alumnos.txt"));
                    break;
                case "7":
                    guardarAlumnos(new PersistenciaJDBC("jdbc:h2:./alumnosdb", "sa", "", "ALUMNOS"));
                    break;
                case "8":
                    ES2.msg("Saliendo...");
                    break;
                default:
                    ES2.msg("Opción no válida.");
            }
        } while (!opcion.equals("8"));
    }

    private void cargarAlumnos(iPersistencia persistencia) {
        ArrayList<Alumno> cargados = persistencia.cargarDatos();
        if (cargados != null) {
            alumnos = cargados;
            ES2.msg("Alumnos cargados correctamente.");
        } else {
            ES2.msg("No se pudieron cargar los alumnos.");
        }
    }

    private void guardarAlumnos(iPersistencia persistencia) {
        persistencia.guardarDatos(alumnos);
        ES2.msg("Alumnos guardados correctamente.");
    }

    public static void main(String[] args) {
        GestorListaAlumnos gestor = new GestorListaAlumnos();
        gestor.mostrarMenu();
    }
    
    private void agregarAlumno() {
        int id=ES2.leeEntero("Introduzca el ID entre 0 y 100: ",0,100);

        System.out.print("Introduzca el nombre: ");
        String nombre = scanner.nextLine();
        System.out.print("Introduzca los apellidos: ");
        String apellidos = scanner.nextLine();
        System.out.print("Es hombre (true/false): ");
        boolean esHombre = Boolean.parseBoolean(scanner.nextLine());

        Alumno alumno = new Alumno(id, nombre, apellidos, esHombre);
        alumnos.add(alumno);
        ES2.msg("Alumno añadido.");
    }

    
    
    
    
    
    
    public void gestionarDatos(iPersistencia persistencia){
        ArrayList<Alumno> lista=persistencia.cargarDatos();
        //hago las operaciones que sea con lista Alumnos
        
        persistencia.guardarDatos(lista);
    }
    
    
    
    
    
    
}
