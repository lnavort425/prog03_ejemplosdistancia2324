package persistencia_;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Esta clase define algunos métodos para entrada de datos, de forma que se
 * capturen los errores de entrada sin abortar, en caso de que el usuario
 * introduzca datos no válidos. También incluye un par de métodos para
 * simplificar la escritura de mensajes en pantalla, abreviando la escritura de
 * las sentencias System.out.println() y System.out.print(). Se hace uso también
 * de la sobrecarga de métodos, que es algo que se había visto con los
 * constructores, pero sin entrar en detalle. Así, podemos tener tres métodos
 * para leer enteros, que se llaman todos igual, pero que se diferencian en su
 * lista de parámetros.
 *
 * @author Profesor
 */
public class ES2 {

    /**
     * Este método sirve para leer desde teclado cualquier número entero en el
     * rango de números del tipo int. La lectura se estará repitiendo hasta que
     * el valor suministrado esté en ese rango.
     *
     * @return El número entero leído.
     */
    private static boolean estaStringEnArray(String[] opciones, String s) {
        boolean esta = false;
        for (int i = 0; i < opciones.length; i++) {
            if (opciones[i].equals(s)) {
                return true;
            }
        }
        return false;
    }

    public static String leeOpciones(String mensaje, String[] opciones) {
        String s = "";

        do {
            msgln(mensaje);
            msgln("Teclea una de las siguientes opciones");
            for (int i = 0; i < opciones.length; i++) {
                msg(opciones[i]);
            }
            s = leeCadena();
        } while (!(estaStringEnArray(opciones, s)));
        return s;
    }

    public static String leeFicheroNotas(String grupoAlumnos) throws FileNotFoundException {
        
            switch (grupoAlumnos) {
                case "4A":
                    return "6,6,0,4,10,8\n7,7,7,4,2,4\n"
                            + "4,7,1,2,3,5\n"
                            + "5,10,5,8,5,7\n"
                            + "6,3,4,2,1,6\n"
                            + "4,8,8,8,8,9\n"
                            + "2,4,0,4,5,4\n"
                            + "3,1,8,9,8,7\n"
                            + "5,3,2,8,7,9\n"
                            + "7,3,1,2,8,4\n"
                            + "6,8,2,6,10,4\n"
                            + "3,3,7,5,6,9\n"
                            + "1,7,10,2,1,2\n"
                            + "3,5,3,4,8,9\n"
                            + "2,3,1,1,2,10\n"
                            + "3,1,2,6,3,9\n"
                            + "2,9,9,7,0,8\n"
                            + "8,9,8,8,2,0\n"
                            + "2,4,3,7,6,1\n"
                            + "8,0,5,4,7,1\n"
                            + "8,8,5,7,1,6\n"
                            + "8,0,6,4,10,1\n"
                            + "5,6,9,1,2,7\n"
                            + "5,6,7,8,6,4\n"
                            + "0,7,9,4,9,5\n"
                            + "6,4,9,2,8,5\n"
                            + "5,1,3,1,10,3\n"
                            + "6,6,10,0,2,8\n"
                            + "4,7,9,5,3,8\n"
                            + "2,5,6,6,8,1";
                case "3B":
                    return "1,1,0,4,10,8\n7,7,7,4,2,4\n"
                            + "4,7,1,2,3,5\n"
                            + "5,1,5,8,5,7\n"
                            + "6,3,4,2,1,6\n"
                            + "4,8,8,8,8,9\n"
                            + "2,4,0,4,5,4\n"
                            + "3,1,8,9,8,7\n"
                            + "5,3,2,8,7,9\n"
                            + "7,3,1,2,8,4\n"
                            + "6,8,2,6,10,4\n"
                            + "3,3,7,5,6,9\n"
                            + "1,7,1,2,1,2\n"
                            + "3,5,3,4,8,9\n"
                            + "2,3,10,10,2,10\n"
                            + "3,1,2,6,3,9\n"
                            + "2,9,9,7,0,8\n"
                            + "8,9,8,8,2,0\n"
                            + "2,4,3,7,6,1\n"
                            + "8,0,5,4,7,1\n"
                            + "8,8,5,7,1,6\n"
                            + "8,0,6,4,10,1\n"
                            + "5,6,9,1,2,7\n"
                            + "5,6,7,8,6,4\n"
                            + "0,7,9,4,9,5\n"
                            + "6,4,9,2,8,5\n"
                            + "5,1,3,1,10,3\n"
                            + "6,6,10,0,2,8\n"
                            + "4,7,9,5,3,8\n"
                            + "2,5,6,6,8,1";
                case "2A":
                    throw new FileNotFoundException("El grupo indicado no está disponible:" + grupoAlumnos);
                default:
                    return "6,6,0,4,10,8\n7,7,7,4,2,4\n"
                            + "9,7,1,2,3,5\n"
                            + "9,10,5,8,5,7\n"
                            + "9,3,4,2,1,6\n"
                            + "9,8,8,8,8,9\n"
                            + "9,4,0,4,5,4\n"
                            + "9,1,8,9,8,7\n"
                            + "5,3,2,8,7,9\n"
                            + "7,3,1,2,8,4\n"
                            + "6,8,2,6,10,4\n"
                            + "3,3,7,5,6,9\n"
                            + "1,7,10,2,1,2\n"
                            + "3,5,3,4,8,9\n"
                            + "2,3,1,1,2,10\n"
                            + "3,1,2,6,3,9\n"
                            + "2,9,9,7,0,8\n"
                            + "8,9,8,8,2,0\n"
                            + "2,4,3,7,6,1\n"
                            + "8,0,5,4,7,1\n"
                            + "8,8,5,7,1,6\n"
                            + "8,0,6,4,10,1\n"
                            + "5,6,9,1,2,7\n"
                            + "5,6,7,8,6,4\n"
                            + "0,7,9,4,9,5\n"
                            + "6,4,9,2,8,5\n"
                            + "5,1,3,1,10,3\n"
                            + "6,6,10,0,2,8\n"
                            + "4,7,9,5,3,8\n"
                            + "2,5,6,6,8,1";
            }

        }
    




    

    

    public static int leeEntero() {
        boolean leido = false;
        int numero = 0;
        Scanner teclado = null;
        do {
            try {
                teclado = new Scanner(System.in);
                numero = teclado.nextInt();
                leido = true;
            } catch (Exception e) {
                ES2.msgln("Error: No es un número entero válido. ");
            }

        } while (!leido);
        return numero;
    }

    /**
     * Este método sirve para escribir el mensaje indicado para solicitar la
     * introducción de un dato desde teclado, y espera la introducción de
     * cualquier número entero en el rango de números del tipo int, que será el
     * dato leído. La lectura se estará repitiendo hasta que el valor
     * suministrado esté en ese rango.
     *
     * @param mensaje Es el mensaje que se muestra para solicitar la
     * introducción del número.
     * @return El número entero leído.
     */
    public static int leeEntero(String mensaje) {
        int numero = 0;
        boolean leido = false;
        Scanner teclado = null;
        do {
            ES2.msgln(mensaje);
            try {
                teclado = new Scanner(System.in);
                numero = teclado.nextInt();
                leido = true;
            } catch (Exception e) {
                ES2.msgln("Error: No es un número entero válido. ");
            }
        } while (!leido);
        return numero;
    }

    /**
     * Este método sirve para leer desde teclado cualquier número entero en el
     * rango de números del tipo int que además sea mayor o igual que el valor
     * mínimo indicado como parámetro. La lectura se estará repitiendo hasta que
     * el valor suministrado esté en ese rango.
     *
     * @param minimo Es el valor más pequeño dentro del rango de los números
     * enteros tipo int que se aceptará como válido.
     * @return El número entero leído.
     */
    public static int leeEntero(int minimo) {
        int numero = 0;
        boolean leido = false;
        Scanner teclado = null;
        do {
            try {
                teclado = new Scanner(System.in);
                numero = teclado.nextInt();
                if (numero >= minimo) {
                    leido = true;
                } else {
                    ES2.msgln("Error: Debe ser un número entero mayor o igual que " + minimo + ". ");
                }
            } catch (Exception e) {
                ES2.msgln("Error: No es un número entero válido. ");
            }
        } while (!leido);
        return numero;
    }

    /**
     * Este método sirve para escribir el mensaje indicado para solicitar la
     * introducción de un dato desde teclado, y espera la introducción de
     * cualquier número entero en el rango de números del tipo int que además
     * sea mayor o igual que el valor mínimo indicado como parámetro. La lectura
     * se estará repitiendo hasta que el valor suministrado esté en ese rango.
     *
     * @param mensaje Es el mensaje que se muestra para solicitar la
     * introducción del número.
     * @param minimo Es el valor más pequeño dentro del rango de los números
     * enteros tipo int que se aceptará como válido.
     * @return El número entero leído.
     */
    public static int leeEntero(String mensaje, int minimo) {
        int numero = 0;
        boolean leido = false;
        Scanner teclado = null;
        do {
            ES2.msgln(mensaje);
            try {
                teclado = new Scanner(System.in);
                numero = teclado.nextInt();
                if (numero >= minimo) {
                    leido = true;
                } else {
                    ES2.msgln("Error: Debe ser un número entero mayor o igual que " + minimo + ".");
                }
            } catch (Exception e) {
                ES2.msgln("Error: No es un número entero válido. ");
            }

        } while (!leido);
        return numero;
    }

    /**
     * Este método sirve para leer desde teclado cualquier número entero en el
     * rango de números del tipo int que además sea mayor o igual que el valor
     * mínimo indicado como primer parámetro y menor o igual que el valor m?ximo
     * indicado como segundo parámetro. La lectura se estará repitiendo hasta
     * que el valor suministrado esté en ese rango.
     *
     * @param minimo Es el valor más pequeño dentro del rango de los números
     * enteros tipo int que se aceptará como válido.
     * @param maximo Es el valor más alto dentro del rango de los números
     * enteros tipo int que se aceptará como válido.
     * @return El número entero leído.
     * @throws IllegalArgumentException Lanza excepci?n si el parámetro mínimo
     * es mayor que el mínimo.
     */
    public static int leeEntero(int minimo, int maximo) throws IllegalArgumentException {
        int numero = 0;
        boolean leido = false;
        Scanner teclado = null;

        // Si el valor mínimo es mayor que el m?ximo, lanzamos una excepción
        if (minimo <= maximo) {
            do {
                try {
                    teclado = new Scanner(System.in);
                    numero = teclado.nextInt();
                    if (numero >= minimo && numero <= maximo) {
                        leido = true;
                    } else {
                        ES2.msgln("Error: Debe ser un número entero mayor o igual que " + minimo + " y menor o igual que " + maximo + ". ");
                    }
                } catch (Exception e) {
                    ES2.msgln("Error: No es un número entero válido. ");
                }
            } while (!leido);
        } else {
            throw new IllegalArgumentException("Rango imposible. El valor mínimo no puede ser mayor que el valor m?ximo");
        }
        return numero;
    }

    /**
     * Este método sirve para escribir el mensaje indicado para solicitar la
     * introducción de un dato desde teclado, y espera la introducción de
     * cualquier número entero en el rango de números del tipo int que además
     * sea mayor o igual que el valor mínimo indicado como primer parámetro y
     * menor o igual que el valor m?ximo indicado como segundo parámetro. La
     * lectura se estará repitiendo hasta que el valor suministrado esté en ese
     * rango.
     *
     * @param mensaje Es el mensaje que se muestra para solicitar la
     * introducción del número.
     * @param minimo Es el valor más pequeño dentro del rango de los números
     * enteros tipo int que se aceptar? como válido.
     * @param maximo Es el valor más alto dentro del rango de los números.
     * enteros tipo int que se aceptará como válido.
     * @return El número entero leído.
     * @throws IllegalArgumentException Lanza excepci?n si el parámetro mínimo
     * es mayor que el mínimo.
     */
    public static int leeEntero(String mensaje, int minimo, int maximo) throws IllegalArgumentException {
        int numero = 0;
        boolean leido = false;
        Scanner teclado = null;

        // Si el valor mínimo es mayor que el m?ximo, lanzamos una excepción
        if (minimo <= maximo) {
            do {
                ES2.msgln(mensaje);
                try {
                    teclado = new Scanner(System.in);
                    numero = teclado.nextInt();
                    if (numero >= minimo && numero <= maximo) {
                        leido = true;
                    } else {
                        ES2.msgln("Error: Debe ser un número entero mayor o igual que " + minimo + " y menor o igual que " + maximo + ". ");
                    }
                } catch (Exception e) {
                    ES2.msgln("Error: No es un número entero válido. ");
                }
            } while (!leido);
        } else {
            throw new IllegalArgumentException("Rango imposible. El valor mínimo no puede ser mayor que el valor m?ximo");
        }
        return numero;
    }
    public static double leeDouble(  String mensaje, double minimo, double maximo) throws IllegalArgumentException {
        double numero = 0;
        boolean leido = false;
        Scanner teclado = null;

        // Si el valor mínimo es mayor que el m?ximo, lanzamos una excepción
        if (minimo <= maximo) {
            do {
                ES2.msgln(mensaje);
                try {
                    teclado = new Scanner(System.in);
                    numero = teclado.nextDouble();
                    if (numero >= minimo && numero <= maximo) {
                        leido = true;
                    } else {
                        ES2.msgln("Error: Debe ser un número entero mayor o igual que " + minimo + " y menor o igual que " + maximo + ". ");
                    }
                } catch (Exception e) {
                    ES2.msgln("Error: No es un número double válido. ");
                }
            } while (!leido);
        } else {
            throw new IllegalArgumentException("Rango imposible. El valor mínimo no puede ser mayor que el valor m?ximo");
        }
        return numero;
    }
    public static double leeDouble(  String mensaje, double min)
    {
        return leeDouble(mensaje, min, Double.MIN_VALUE);
    }
        public static double leeDouble(  String mensaje)
    {
        return leeDouble(mensaje, Double.NEGATIVE_INFINITY, Double.MAX_VALUE);
    }
    /**
     * Este método lee una cadena de caracteres desde teclado, y comprueba que
     * efectivamente la lectura se ha producido correctamente, mandando un
     * mensaje de error en caso de que haya fallado.
     *
     * @return La cadena de caracteres leído desde el teclado.
     */
    public static String leeCadena() {
        Scanner teclado = new Scanner(System.in);
        String cadena = "";
        try {
            cadena = teclado.nextLine();
        } catch (Exception e) {
            ES2.msgln("Error: Ha fallado la entrada de datos.");
        }
        return cadena;
    }

    /**
     * Este método sirve para escribir el mensaje indicado para solicitar la
     * introducción de un dato desde teclado, espera la introducción de una
     * cadena de caracteres desde teclado, y comprueba que efectivamente la
     * lectura se ha producido correctamente, mandando un mensaje de error en
     * caso de que haya fallado.
     *
     * @param mensaje Es el mensaje que se env?a solicitando la introducción de
     * la cadena de texto.
     * @return La cadena de caracteres leído desde el teclado.
     */
    public static String leeCadena(String mensaje) {
        Scanner teclado = new Scanner(System.in);
        String cadena = "";
        try {
            ES2.msgln(mensaje);
            cadena = teclado.nextLine();
        } catch (Exception e) {
            ES2.msgln("Error: Ha fallado la entrada de datos.");
        }
        return cadena;
    }

    /**
     * Este método hace exactamente lo mismo que el método System.out.print(),
     * pero tiene la ventaja de que su escritura es más corta y ahorra algo de
     * trabajo en programas donde la sentencia System.out.print() se use mucho.
     *
     * @param entero Es es el valor entero de tipo int a imprimir como texto.
     */
    public static void msg(int entero) {
        System.out.print(entero);
    }

    /**
     * Este método hace exactamente lo mismo que el método System.out.print(),
     * pero tiene la ventaja de que su escritura es más corta y ahorra algo de
     * trabajo en programas donde la sentencia System.out.print() se use mucho.
     *
     * @param enteroLargo Es es el valor entero de tipo long a imprimir como
     * texto.
     */
    public static void msg(long enteroLargo) {
        System.out.print(enteroLargo);
    }

    /**
     * Este método hace exactamente lo mismo que el método System.out.print(),
     * pero tiene la ventaja de que su escritura es más corta y ahorra algo de
     * trabajo en programas donde la sentencia System.out.print() se use mucho.
     *
     * @param real Es es el valor real de tipo float a imprimir como texto.
     */
    public static void msg(float real) {
        System.out.print(real);
    }

    /**
     * Este método hace exactamente lo mismo que el método System.out.print(),
     * pero tiene la ventaja de que su escritura es más corta y ahorra algo de
     * trabajo en programas donde la sentencia System.out.print() se use mucho.
     *
     * @param realLargo Es es el valor entero de tipo double a imprimir como
     * texto.
     */
    public static void msg(double realLargo) {
        System.out.print(realLargo);
    }

    /**
     * Este método hace exactamente lo mismo que el método System.out.print(),
     * pero tiene la ventaja de que su escritura es más corta y ahorra algo de
     * trabajo en programas donde la sentencia System.out.print() se use mucho.
     *
     * @param cadenaAImprimir Es la cadena de texto que hay que escribir en el
     * dispositivo de salida estándar.
     */
    public static void msg(String cadenaAImprimir) {
        System.out.print(cadenaAImprimir);
    }

    public static void msg(Object objetoAImprimir) {
        System.out.print(objetoAImprimir.toString());
    }

    /**
     * Este método hace exactamente lo mismo que el método System.out.println(),
     * cuando se invoca sin parámetros, pero tiene la ventaja de que su
     * escritura es más corta y ahorra algo de trabajo en programas donde la
     * sentencia System.out.println() se use mucho.
     */
    public static void msgln() {
        System.out.println();
    }

    /**
     * Este método hace exactamente lo mismo que el método System.out.println(),
     * pero tiene la ventaja de que su escritura es más corta y ahorra algo de
     * trabajo en programas donde la sentencia System.out.println() se use
     * mucho.
     *
     * @param entero Es es el valor entero de tipo int a imprimir como texto.
     */
    public static void msgln(int entero) {
        System.out.println(entero);
    }

    /**
     * Este método hace exactamente lo mismo que el método System.out.println(),
     * pero tiene la ventaja de que su escritura es más corta y ahorra algo de
     * trabajo en programas donde la sentencia System.out.println() se use
     * mucho.
     *
     * @param enteroLargo Es es el valor entero de tipo long a imprimir como
     * texto.
     */
    public static void msgln(long enteroLargo) {
        System.out.println(enteroLargo);
    }

    /**
     * Este método hace exactamente lo mismo que el método System.out.println(),
     * pero tiene la ventaja de que su escritura es más corta y ahorra algo de
     * trabajo en programas donde la sentencia System.out.println() se use
     * mucho.
     *
     * @param real Es es el valor real de tipo float a imprimir como texto.
     */
    public static void msgln(float real) {
        System.out.println(real);
    }

    /**
     * Este método hace exactamente lo mismo que el método System.out.println(),
     * pero tiene la ventaja de que su escritura es más corta y ahorra algo de
     * trabajo en programas donde la sentencia System.out.println() se use
     * mucho.
     *
     * @param realLargo Es es el valor entero de tipo double a imprimir como
     * texto.
     */
    public static void msgln(double realLargo) {
        System.out.println(realLargo);
    }

    /**
     * Este método hace exactamente lo mismo que el método System.out.println(),
     * pero tiene la ventaja de que su escritura es más corta y ahorra algo de
     * trabajo en programas donde la sentencia System.out.println() se use
     * mucho.
     *
     * @param cadenaAImprimir Es la cadena de texto que hay que escribir en el
     * dispositivo de salida estándar.
     */
    public static void msgln(String cadenaAImprimir) {
        System.out.println(cadenaAImprimir);
    }

    public static void msgln(Object objetoAImprimir) {
        System.out.println(objetoAImprimir.toString());
    }

    /**
     * Lee desde teclado una respuesta para una pregunta de tipo Sí o No,
     * mostrando por pantalla la pregunta en cuestión. Sólo se admiten como
     * respuestas S, s, N o bien n.
     *
     * @param mensaje Es el mensaje que se env?a con la pregunta cuya respuesta
     * se espera que sea S o N.
     * @return la cadena "S" si se ha respondido "S" o "s" y la cadena "N" si se
     * ha respondido "N" o "n".
     */
    public static String leeRespuesta(String mensaje) {
        boolean correcta = false;
        String cadena = "";
        Scanner teclado = null;
        do {
            ES2.msgln(mensaje);
            try {
                teclado = new Scanner(System.in);
                cadena = teclado.nextLine();
                if (cadena != null && cadena.length() == 1 && ((cadena.equalsIgnoreCase("S")) || (cadena.equalsIgnoreCase("N")))) {
                    correcta = true;
                } else {
                    ES2.msgln("Error: Solo se admite como respuesta un único carácter, que debe ser 's', 'S', 'n' o 'N'.");
                }
            } catch (Exception e) {
                ES2.msgln("Error: Ha fallado la entrada de datos.");
            }
        } while (!correcta);
        return cadena.toUpperCase();
    }

    /**
     * Lee un carácter de teclado.
     *
     * @param mensaje Mensaje a mostrar por pantalla
     * @return El carácter leído por teclado.
     */
    public static char leeCaracter(String mensaje) {

        Scanner teclado = null;
        teclado = new Scanner(System.in);
        ES2.msgln(mensaje);
        String cadena = teclado.next();
        char caracter = cadena.charAt(0);
        return caracter;
    }

}//class ES
