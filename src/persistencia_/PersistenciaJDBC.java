/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package persistencia_; 

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import static java.lang.System.err;
import static java.lang.System.out;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.stream.Collectors;
import org.h2.tools.Server;

/**
 *
 * @author atecaiestrassierra
 */
public class PersistenciaJDBC extends Persistencia {

    //Este driver lo pongo fijo pues siempre voy a usar el mismo SGBD, y las clases las tengo en un jar que he añadido a librerias previamente
    private static final String DRIVER = "org.h2.Driver";
    private static Connection con = null;
    String url, usuario, clave, tabla;
    private static String ESTRUCTURA_DB="resources/creaTablaAlumnoRellena.sql";
    //la url es: jdbc:h2:./proyectobase.h2db/;MODE=MySQL;AUTO_RECONNECT=TRUE

    public PersistenciaJDBC(String url, String usuario, String clave, String tabla) {
        this.url = url;
        this.usuario = usuario;
        this.clave = clave;
        this.tabla = tabla;

    }
    public void desconectar(){
        try{
             con.close();
        } catch (SQLException e)
        {
            System.out.println("Problema al cerrar la conexión a:"+url+"; el código sql es:"+e.getErrorCode()+"Mensaje"+e.getMessage());
        }
    }
    public void conectar() {
        boolean driverCargado = false;
        try {
            Class.forName(DRIVER).getDeclaredConstructor().newInstance();
            driverCargado = true;
        } catch (ClassNotFoundException e) {
            err.printf("No se encuentra el driver de la base de datos (%s)\n", DRIVER);
        } catch (InstantiationException | IllegalAccessException ex) {
            err.printf("No se ha podido iniciar el driver de la base de datos (%s)\n", DRIVER);
        } catch (IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
            err.printf("No se ha podido iniciar el driver de la base de datos (%s)\n", DRIVER);
        }
         if (driverCargado) {
            // Conectamos con la base de datos.
            // El try-with-resources asegura que se cerrará la conexión al salir.
            String[] wsArgs = {"-baseDir", System.getProperty("user.dir"), "-browser"};
            try (  Connection conexion = DriverManager.getConnection(url, usuario, clave)) {

                // Iniciamos el servidor web interno (consola H2 para depuraciones)
                Server sr = Server.createWebServer(wsArgs);
                sr.start();

                // Presentamos información inicial por consola
                out.println("¡¡Atención!!");
                out.println();
                out.println("Mientras tu aplicación se esté ejecutando \n"
                        + "puedes acceder a la consola de la base de datos \n"
                        + "a través del navegador web.");
                out.println();
                out.println("Página local: " + sr.getURL());
                out.println();
                out.println("Datos de acceso");
                out.println("---------------");
                out.println("Controlador: " + DRIVER);
                out.println("URL JDBC: " + url);
                out.println("Usuario: (no indicar nada)");
                out.println("Password: (no indicar nada)");

                // Creamos las tablas y algunos datos de prueba si no existen
                // y continuamos
                if (createTables(conexion)) {
                    con=conexion;
                    // Insertar los datos en las tablas de la BD
                    //insertarDatosTablas(con);
                  // Esperar tecla
                    ES2.leeCadena("Antes de terminar, puedes acceder a la "
                            + "consola de H2 para ver y modificar la base de "
                            + "datos. Pulsa cualquier tecla para salir.");

                } else {
                    System.err.println("Problema creando las tablas.");

                }

                sr.stop();
                sr.shutdown();

            } catch (SQLException ex) {
                err.printf("No se pudo conectar a la base de datos (%s)\n");
                System.out.println("Ha habido un problema: "+ex.getMessage()+", y el código SQL:"+ex.getErrorCode()+ " y sqlState="+ex.getSQLState());
                
            }
        }

    }
    
        public ArrayList<Alumno> cargarDatos(){
            ArrayList<Alumno> listaAlumno=new ArrayList<Alumno>();
            //Tengo una conexion con
            //Tengo el nombre de la tabla y la documentación de Alumno
            //Creo un statement, hago select a la tabla ALUMNO y la recorro con un cursor, de cada fila saco los campos del alumno, creo 
            //alumno y lo añado a la lista
        try ( Statement consulta = con.createStatement()) {
            if (consulta.execute("SELECT * FROM "+tabla)) {
                 System.out.println("---Leyendo de"+url+"----"+tabla);
               System.out.println("------------- Listado de alumnos ---------------");
                System.out.println("Código Nombre                 Apellido  Sexo     ");
                System.out.println("------ ------------------------ --------- ----------");
                //java.sql.Date
                //java.util.Date
                ResultSet resultados = consulta.getResultSet();
                while (resultados.next()) {
                    int id = resultados.getInt("ID");
                    String nombre = resultados.getString("NOMBRE");
                    String apellidos = resultados.getString("APELLIDOS");
                    boolean sexo = (resultados.getString("SEXO")).equalsIgnoreCase("H");
                    //resultados.
                    System.out.printf("%6d %-22s %11s %10s \n", id, nombre, apellidos, sexo);
                    Alumno a=new Alumno(id, nombre, apellidos, sexo);
                    listaAlumno.add(a);
                }
            } else {
                System.out.println("No hay actividades en la base de datos");
            }
        } catch (SQLException ex) {
            System.err.printf("Se ha producido un error al ejecutar la consulta SQL.");
            System.err.printf("LISTANDO ASIGNACIONES de proyectos.\n" + ex.getLocalizedMessage() + "\nmensaje" + ex.getMessage() + "\nsqlState" + ex.getSQLState() + "\nerror code" + ex.getErrorCode() + "\n" + ex);
        }


            return listaAlumno;
        }
   // public void guardarDatos(Recetario);
    public void guardarDatos(ArrayList<Alumno> al){
        for (Alumno a: al){
                   try {
                       /*
                Statement consulta2;
                consulta2 = con.createStatement();
                consulta2.execute("select max(codigo) from asignacion");
                ResultSet resultados = consulta2.getResultSet();
                while (resultados.next()) {
                    maxCodAsignacion = resultados.getInt(1);
                }
*/
                PreparedStatement consulta;
                consulta = con.prepareStatement("insert into "+tabla+" (id,nombre,apellidos,sexo) values(?,?,?,?);");//"update asignacion set fecha= ?  where cod_empleado= ? and cod_proyecto= ? ;");

                consulta.setInt(0, a.getId());
                consulta.setString(1, a.getNombre());//Int(3, cod_proyecto);
                consulta.setString(2, a.getApellidos());
                consulta.setString(3, a.isEsHombre()?"H":"M");
                //consulta.setInt(1, maxCodAsignacion+1);
                int numFilasActualizadas = consulta.executeUpdate();
            } catch (SQLException ex) {
                System.err.printf("problema insertando nueva asignacion.\n" + ex.getLocalizedMessage() + "\nmensaje" + ex.getMessage() + "\nsqlState" + ex.getSQLState() + "\nerror code" + ex.getErrorCode() + "\n" + ex);

            }
        }
    }

    
    public static boolean createTables (Connection con) {
        boolean ok = false ;
        
        try (Statement st=con.createStatement())  {                   
            String sqlScript=loadResourceAsString(ESTRUCTURA_DB);
            if (sqlScript!=null) {
                st.execute(sqlScript);                  
                ok = true;
            } else {
                System.out.printf("Problema cargando el archivo: %s \n",ESTRUCTURA_DB);
                System.out.printf("Para ejecutar este proyecto no puede usarse 'Run File'\n");
            }
            
        } catch (SQLException ex) {
            System.err.printf("Problema creando la estructura de la base de datos.\n");
        }
        return ok;        
    }        
    
    /**
      * Carga un recurso que estará dentro del JAR como cadena de texto.
      * @param resourceName Nombre del recurso dentro del JAR.
      * @return Cadena que contiene el contenido del archivo.
      */
    public static String loadResourceAsString(String resourceName) {
        String resource = null;
        InputStream is = PersistenciaJDBC.class.getResourceAsStream(resourceName);
        if (is != null) {
            try (InputStreamReader isr = new InputStreamReader(is);  BufferedReader br = new BufferedReader(isr);) {
                resource = br.lines().collect(Collectors.joining("\n"));
            } catch (IOException ex) {
                System.err.printf("Problema leyendo el recurso como cadena: %S\n ", resourceName);
            }
        }
        return resource;
    }
    
}
