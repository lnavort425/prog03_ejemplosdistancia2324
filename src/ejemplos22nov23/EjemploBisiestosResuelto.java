/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */



package ejemplos22nov23;

import java.time.LocalDate;
import java.time.Month;
import java.util.Scanner;

/**
 *
 * @author luisnavarro
 */
public class EjemploBisiestosResuelto {
    public static void main(String[] args) {
        //Pongamos que hacemos reuni´´on último sábado de mes, queremos saber las fechas que caerán un año dado y la diferencia en semanas con la última
        //Si es sábado y día bisiesto fiesta del siglo;
        //Si es bisiesto y no sábado, fiesta cuatrienal
        Scanner teclado=new Scanner(System.in);
        System.out.println("Dime un año:");
        int anno=teclado.nextInt();
        //Para cada mes, supongamos que.....
        for(int i=1;i<13;i++)
        {
            int longitudMes = LocalDate.of(anno, i, 1).lengthOfMonth();
            int diaSemana=LocalDate.of(anno, i, longitudMes).getDayOfWeek().getValue();
            //int diasHastaSabado=((7-(diaSemana+1)%7)%7);
            int diasHastaSabado=(((diaSemana+1)%7));
            LocalDate sabadoMes=LocalDate.of(anno,i,longitudMes-diasHastaSabado);
            System.out.println("longitudMes="+longitudMes+" "+diaSemana+"-"+diasHastaSabado+"Último sábado de mes:"+sabadoMes);
            if (sabadoMes.isLeapYear()&&i==2)
            {
                if (diasHastaSabado==0) System.out.println("Fiesta del siglo");
                else
                {
                    LocalDate sabadoCuatrienal=sabadoMes.plusWeeks(1);
                    System.out.println("Sabado cuatrienal="+sabadoCuatrienal);
                }
            }
        }
    }
}
