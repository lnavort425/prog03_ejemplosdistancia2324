/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */



package ejemplos22nov23;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.util.Scanner;

/**
 *
 * @author luisnavarro
 */
public class EjemploBisiestos {
    public static void main(String[] args) {
        //Pongamos que hacemos reunión último sábado de mes, queremos saber las fechas que caerán un año dado y la diferencia en semanas con la última
        //Si es sábado y día bisiesto fiesta del siglo;
        //Si es bisiesto y no sábado, fiesta cuatrienal
        Scanner teclado=new Scanner(System.in);
        System.out.println("Dime un año");
        int anio=teclado.nextInt();
        for(int mes=1;mes<13;mes++)
        {
            
            int diasDeEsteMes=LocalDate.of(anio, mes, 1).lengthOfMonth();
            LocalDate fiestaMes=null;
            for (int j=1; j<diasDeEsteMes; j++){
                LocalDate presuntoSabado=LocalDate.of(anio, mes, j);
                if (presuntoSabado.getDayOfWeek()==DayOfWeek.SATURDAY)
                    fiestaMes=presuntoSabado;
                System.out.println("La fiesta este mes es:"+fiestaMes);
            LocalDate ultimoDiaMes=LocalDate.of(anio, mes, diasDeEsteMes);
            int diasHastaSabado=(7-(ultimoDiaMes.getDayOfWeek()+1)%7);
            LocalDate ultimoSabadoMes=LocalDate.of(anio, mes, diasDeEsteMes-diasHastaSabado);
            //(ultimoDiaMes.getDayOfWeek()-DayOfWeek.SATURDAY-1)
            }
        }
    }
}
