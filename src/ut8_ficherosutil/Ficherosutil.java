/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_ficherosutil;

import ut8_ficheros1.Contacto;
import ut8_ficheros1.CopiaFicheros;
import ut8_ficheros1.FicheroContactos;
import ut8_ficheros1.Filtro;
import ut8_ficheros1.Registro;
import ut8_ficheros1.Token;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StreamTokenizer;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author luisnavarro. Ejemplos hechos:
 *      //Ficherosutil.escribeFicherodesdeTeclado(Constantes.RUTA_SALIDA+"test2");
        //Ficherosutil.leeFicheroTextoaConsola(Constantes.RUTA_SALIDA+"test2");
        //Ficherosutil.contarPalabrasyNumeros(Constantes.RUTA_SALIDA+"datos.txt");
        //Ficherosutil.leerBinarioConBuffer("test.bin");
        //probarAgenda();
        //probarFiltro("txt");
        escribirLeerSerialzable();
 */
public class Ficherosutil {

    public static void escribeFicherodesdeTeclado(String nombreFichero) {

        Scanner sc = new Scanner(System.in);
        PrintWriter salida = null;

        try {
            salida = new PrintWriter(Constantes.RUTA_SALIDA + nombreFichero);//"datos.txt");   //se crea el fichero
            String cadena;
            System.out.println("Introduce texto. Para acabar introduce la cadena FIN:");
            cadena = sc.nextLine();                             //se introduce por teclado una cadena de texto    
            while (!cadena.equalsIgnoreCase("FIN")) {
                salida.println(cadena);                         //se escribe la cadena en el fichero
                cadena = sc.nextLine();                         //se introduce por teclado una cadena de texto    
            }
            salida.flush();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } finally {
            salida.close();
        }
    }

    public static void leeFicheroTextoaConsola(String nombreFichero) {

        FileReader fr = null;
        try {
            fr = new FileReader(Constantes.RUTA_SALIDA + nombreFichero);//"datos.txt");
            BufferedReader entrada = new BufferedReader(fr);
            int car = entrada.read();         //se lee el primer carácter del fichero
            while (car != -1) {               //mientras no se llegue al final del fichero                        
                System.out.print((char) car); //se nuestra por pantalla
                car = entrada.read();         //se lee el siguiente carácter del fichero                          
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (fr != null) {
                    fr.close();
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public static void contarPalabrasyNumeros(String nombreFichero) {

        StreamTokenizer sTokenizer = null;
        int contadorPalabras = 0, numeroDeNumeros = 0;

        try {

            sTokenizer = new StreamTokenizer(new FileReader(nombreFichero));

            while (sTokenizer.nextToken() != StreamTokenizer.TT_EOF) {

                if (sTokenizer.ttype == StreamTokenizer.TT_WORD) {
                    contadorPalabras++;
                } else if (sTokenizer.ttype == StreamTokenizer.TT_NUMBER) {
                    numeroDeNumeros++;
                }
            }

            System.out.println("Número de palabras en el fichero: " + contadorPalabras);
            System.out.println("Número de números en el fichero: " + numeroDeNumeros);

        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void leoDelTecladoStringBuilder() {
        // Cadena donde iremos almacenando los caracteres que se escriban
        StringBuilder str = new StringBuilder();
        char c;
        // Por si ocurre una excepci�n ponemos el bloque try-cath
        try {
            // Mientras la entrada de terclado no sea Intro
            while ((c = (char) System.in.read()) != '\n') {
                // A�adir el character le�do a la cadena str
                str.append(c);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        // Escribir la cadena que se ha ido tecleando
        System.out.println("Cadena introducida: " + str);
    }

    static void copiaFlujoBytes(InputStream fentrada, OutputStream fsalida) {

        try {
            int n = 0;

            byte[] buffer = new byte[256*4];
            while ((n = fentrada.read(buffer)) >= 0) {
                fsalida.write(buffer, 0, n);
            }

            fentrada.close();
            fsalida.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    static void copiaFlujoCaracteres() {
        try {
            PrintWriter out = null;
            out = new PrintWriter(new FileWriter("c:\\salida.txt", true));
            //InputStreamReader lee el flujo de bytes y lo decodifica a caracteres. Actua de puente de flujo de bytes a flujo de caracteres.
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(System.in));
            String s;
            while (!(s = br.readLine()).equals("salir")) {
                out.println(s);
            }
            out.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

   
    public static void leerBinarioConBuffer(String nombreFichero) {
        int tama;

        try {
            // Creamos un nuevo objeto File, que es la ruta hasta el fichero desde
            File f = new File(Constantes.RUTA_ENTRADA + "test.bin");

            // Construimos un flujo de tipo FileInputStream (un flujo de entrada desde
            // fichero) sobre el objeto File. Estamos conectando nuestra aplicación
            // a un extremo del flujo, por donde van a salir los datos, y "pidiendo"
            // al Sistema Operativo que conecte el otro extremo al fichero que indica
            // la ruta establecida por el objeto File f que habíamos creado antes. De
            FileInputStream flujoEntrada = new FileInputStream(f);
            BufferedInputStream fEntradaConBuffer = new BufferedInputStream(flujoEntrada);

            // Escribimos el tamaño del fichero en bytes.
            tama = fEntradaConBuffer.available();
            System.out.println("Bytes disponibles: " + tama);

            // Indicamos que vamos a intentar leer 50 bytes del fichero.
            System.out.println("Leyendo 50 bytes....");

            // Creamos un array de 50 bytes para llenarlo con los 50 bytes
            // que leamos del flujo (realmente del fichero)*/
            byte bytearray[] = new byte[50];

            // El método read() de la clase FileInputStream recibe como parámetro un
            // array de byte, y lo llena leyendo bytes desde el flujo.
            // Devuelve un número entero, que es el número de bytes que realmente se
            // han leído desde el flujo. Si el fichero tiene menos de 50 bytes, no
            // podrá leer los 50 bytes, y escribirá un mensaje indicándolo.
            if (fEntradaConBuffer.read(bytearray) != 50) {
                System.out.println("No se pudieron leer 50 bytes");
            }

            // Usamos un constructor adecuado de la clase String, que crea un nuevo
            // String a partir de los bytes leídos desde el flujo, que se almacenaron
            // en el array bytearray, y escribimos ese String.
            System.out.println(new String(bytearray, 0, 50));

            // Finalmente cerramos el flujo.Es importante cerrar los flujos
            // para liberar ese recurso. Al cerrar el flujo, se comprueba que no
            // haya quedado ningún dato en el flujo sin que se haya leído por la aplicación. */
            fEntradaConBuffer.close();

            // Capturamos la excepción de Entrada/Salida. El error que puede
            // producirse en este caso es que el fichero no esté accesible, y
            // es el mensaje que enviamos en tal caso.
        } catch (IOException e) {
            System.err.println("No se encuentra el fichero");
        }
    }
    public static void copiaFichero(String fIn, String fOut){
                try {
            //String fIn = Token.RUTA_SALIDA + "salida3.txt";
            //String fOut = Token.RUTA_SALIDA + "fichero3Copia.txt";
            File ficheroEntrada = new File(fIn);
            FileInputStream fisEntrada = new FileInputStream(ficheroEntrada);
            File ficheroSalida = new File(fOut);
            FileOutputStream fosSalida = new FileOutputStream(ficheroSalida);
            //InputStream isEntrada=new InputStream(fisEntrada);
            //OutputStream osSalida=new OutputStream(fosSalida);
            copiaFlujoBytes(fisEntrada, fosSalida);
        } catch (FileNotFoundException fnfe) {
            System.out.println("" + fnfe.getMessage());
        }

    }
    
    public static void probarAgenda() {
        Registro contacto = new Registro();
        FicheroContactos agenda;
        agenda = new FicheroContactos();
        Registro contacto1 = new Registro("pepe", "677778899", "pepe@dfsg.es", "pez, 38", new Date(2001, 11, 4), 5, 4325);
        Registro contacto2 = new Registro("juan", "666778899", "juan@dfsg.es", "ballesta, 3", new Date(2011, 11, 4), 4, 42222);
        try {
            /*
            agenda.abrir();
            agenda.escribir(contacto1);
            agenda.escribir(contacto2);
            agenda.cerrar();
        */
            agenda.abrir();
                contacto = agenda.leer();
            
            while (contacto != null){
                System.out.println(""+contacto.getDireccion());
                contacto = agenda.leer();
                
            }
            agenda.cerrar();

            agenda.abrir();
            contacto = agenda.leer(2);
            contacto.setNombre("JC");
            agenda.escribir(contacto, 2);
            agenda.cerrar();
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }

    public static void probarFiltro(String extension) {

        try {
            File fichero = new File(Constantes.RUTA_ENTRADA);//".");
            String[] listadeArchivos = fichero.list();
            System.out.println("Lista de archivos del directorio activo sin filtrar (incluyendo Carpetas).");
            int numarchivos = listadeArchivos.length;
            if (numarchivos < 1) {
                System.out.println("No hay archivos que listar");
            } else {
                for (int conta = 0; conta < listadeArchivos.length; conta++) {
                    if ((new File(listadeArchivos[conta])).isDirectory()) {
                        System.out.println("Directorio: " + listadeArchivos[conta]);
                    } else {
                        System.out.println("Archivo: " + listadeArchivos[conta]);
                    }
                }
            }

            listadeArchivos = fichero.list(new Filtro(extension));//".odt"));
            numarchivos = listadeArchivos.length;
            System.out.println("Lista de archivos del directorio activo APLICANDO FILTRO " + extension + "(incluyendo Carpetas).");
            if (numarchivos < 1) {
                System.out.println("No hay archivos que listar");
            } else {
                for (int conta = 0; conta < listadeArchivos.length; conta++) {
                    if ((new File(listadeArchivos[conta])).isDirectory()) {
                        System.out.println("Directorio: " + listadeArchivos[conta]);
                    } else {
                        System.out.println("Archivo: " + listadeArchivos[conta]);
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("Error al buscar en la ruta indicada");
            System.out.println(ex.getMessage());
        }

    }

    public static void escribirLeerSerialzable() {

        FileOutputStream fichero = null;
        /*
        try {
            fichero = new FileOutputStream(new File(Constantes.RUTA_SALIDA + "contacto.ser"));

            // Abrir fichero para escribir en el, en la ruta que me interesa
            ObjectOutputStream ficheroSalida;
            ficheroSalida = new ObjectOutputStream(fichero);
            // Escribo el array en el fichero
            Contacto contacto1 = new Contacto("pepe", "677778899", "pepe@dfsg.es", "pez, 38", new Date(2001, 11, 4), 5, 4325);
            Contacto contacto2 = new Contacto("juan", "666778899", "juan@dfsg.es", "ballesta, 3", new Date(2011, 11, 4), 4, 42222);
            Contacto[] socios = new Contacto[2];
            socios[0] = contacto1;
            socios[1] = contacto2;
            System.out.println("Voy a escribir array de contactos");
            ficheroSalida.writeObject(socios);
            // También escribo dos datos que me interesan
            System.out.println("Voy a escribir grupo");
            ficheroSalida.writeInt(socios[0].getGrupo());
            ficheroSalida.writeInt(socios[1].getGrupo());

            // Cerramos el fichero
            ficheroSalida.close();

            System.out.println("El archivo " + fichero + " guardado con éxito...");
        } catch (FileNotFoundException fnfe) {
            System.out.println("Error: El fichero no existe. ");
        } catch (IOException ioe) {
            System.out.println(ioe+"Error: falló la escritura en el fichero " + fichero);
        }
*/
        // Pruebo ahora a poner a null el array e intento recuperar de fichero
        Contacto[] socios = new Contacto[2];
        Contacto socio;
        try {
            // Abrimos el fichero para lectura
            FileInputStream fichero2 = new FileInputStream(new File(Constantes.RUTA_SALIDA + "contacto.ser"));
            ObjectInputStream ficheroEntrada = new ObjectInputStream(fichero2);

            // Recuperar el objeto array del fichero
            socios = (Contacto[]) ficheroEntrada.readObject();

            for (int contador = 0; contador < socios.length; contador++) {
                socio = socios[contador];
                System.out.println(socio);
            }

            ficheroEntrada.close();
            System.out.println("El Archivo " + Constantes.RUTA_SALIDA + "contacto.ser" + " se ha cargado con éxito....");

        } catch (ClassNotFoundException cnfe) {
            System.out.println("No se pudo acceder a la clase adecuada para revertir la Serialización al leer del fichero.");
        } catch (FileNotFoundException fnfe) {
            System.out.println("El fichero " + Constantes.RUTA_SALIDA + "contacto.ser" + " no existe.");
        } catch (IOException ioe) {
            System.out.println("Error de Entrada/Salida: Falló la lectura del fichero. La aplicación sigue funcionando sin haber cargado los datos del archivo, para permitir crearlo de nuevo.");
        }
    }
}
