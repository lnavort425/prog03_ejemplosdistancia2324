/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_serializacion;

/**
 *
 * @author luisnavarro
 */
import java.io.*;
public class SerializacionApp {
    public static void main(String[] args) {
        //Creamos el objeto
        Empleado empleado1=new Empleado("Fernando", "Ureña", 23, 800);
        Empleado empleado2=new Empleado("Antonio", "Lopez", 35, 1000);
        try(ObjectOutputStream oos=new ObjectOutputStream(new FileOutputStream(ut8_ficherosutil.Constantes.RUTA_SALIDA+"empleados.ddr"))){
            //Escribimos en un fichero
            oos.writeObject(empleado1);
            oos.writeObject(empleado2);
        }catch(IOException e){
        }
    }
}