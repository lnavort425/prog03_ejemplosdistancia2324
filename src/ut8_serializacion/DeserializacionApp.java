/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_serializacion;
import java.io.*;

/**
 *
 * @author luisnavarro
 */
public class DeserializacionApp {
    

    public static void main(String[] args) {
        try(ObjectInputStream ois=new ObjectInputStream(new FileInputStream(ut8_ficherosutil.Constantes.RUTA_SALIDA+"empleados.ddr"))){
            //Cuando no haya mas objetos saltara la excepcion EOFException
            while(true){
                Empleado aux=(Empleado)ois.readObject();
                System.out.println(aux.getNombre());
                System.out.println(aux.getApellido());
                System.out.println(aux.getEdad());
                System.out.println(aux.getSalario());
                System.out.println("");
            }
        }catch(ClassNotFoundException e){
        }catch(EOFException e){
        }catch(IOException e){
        }
    }
}