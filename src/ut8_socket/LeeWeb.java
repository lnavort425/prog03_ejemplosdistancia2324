/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_socket;
import java.io.*;
import java.net.*;

public class LeeWeb {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("example.com", 80);
        InputStream inputStream = socket.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }

        reader.close();
        inputStream.close();
        socket.close();
    }
}