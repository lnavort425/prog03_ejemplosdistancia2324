/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_ejemplosClase;

import java.io.*;
import java.util.Date;


/**
 *
 * @author luisnavarro
 */
public class Prueba {

    public static void main(String[] args) throws IOException {
        //Ficherosutil.escribeFicherodesdeTeclado("test2.txt");
        //Ficherosutil.leeFicheroTextoaConsola(Constantes.RUTA_SALIDA+"test2.txt");
        //Ficherosutil.contarPalabrasyNumeros(Constantes.RUTA_SALIDA+"datos.txt");
        //Ficherosutil.leoDelTecladoStringBuilder();
        //Ficherosutil.leerBinarioConBuffer("test.bin");
        //probarAgenda();
        //probarFiltro("txt");
        //Ficherosutil.escribirLeerSerialzable();
        Ficherosutil.copiaFlujoCaracteres(new InputStreamReader(System.in), new FileWriter("salida2.txt", true));
        Ficherosutil.copiarDeReaderaOutputStreamconCodificacion(new FileReader("entrada.txt"), new FileOutputStream("archivo_salida.txt"), "UTF-8");
        Ficherosutil.copiaFlujoBytes(new FileInputStream(new File(Constantes.RUTA_SALIDA+"entrada.txt")), new FileOutputStream(Constantes.RUTA_SALIDA+"archivo_salida2.txt"));
    }

}
