/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_ejemplosClase;

import ut8_socket.*;
import ut8_ficheros1.Token;
import java.net.Socket;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Date;

public class LeeWebGoogle {

    public static void main(String[] args) throws Exception {
        try {
            Socket socket = new Socket("www.google.com", 80);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            out.println("GET / HTTP/1.1");
            out.println("Host: www.google.com");
            out.println("");
            out.flush();
            String inputLine;

            //He cerrado el flujo de salida con el que haago la petición http, tengo el de entrada con la respuesta,abro otro de salida para fichero
            PrintWriter out2 = null;
            Date d = new Date();
            out2 = new PrintWriter(new FileWriter(Token.RUTA_SALIDA + "webGoogle" + d + ".html", true));

            while ((inputLine = in.readLine()) != null) {
                System.out.println(inputLine);
                out2.println(inputLine);
            }
            out2.close();
            in.close();
            out.close();
            socket.close();
        } catch (Exception e) {
            System.out.println(e.getMessage() + "Excepción" + e);
        }
    }
}
