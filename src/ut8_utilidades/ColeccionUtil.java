/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_utilidades;
import java.util.*;
/**
 *
 * @author luisnavarro
 */
public class ColeccionUtil {
    public static void imprimirColeccion(Collection c){
        Iterator it=c.iterator();
        while(it.hasNext())
            System.out.print(it.next());
        System.out.println("");
    }
    public static void imprimirMapa(Map m){
        //creo keyset iterador, iterando imprimo clave y valor asociado
            Set conjuntoDeClaves=m.keySet();
        Iterator it=conjuntoDeClaves.iterator();
        
        while(it.hasNext())
        {
            String palabra=(String)(it.next());
            int n=(Integer)m.get(palabra);
            System.out.println(palabra+"-"+n);
        }
        
    }
    public static Map<String, Integer> sacaMapaPalabrasCadenaSeparadaPorBlancos(String cadena) {
        Map<String, Integer> mapaOcurrencias = new TreeMap<String, Integer>();
        String[] arrayPalabras = cadena.split(" ");
        /*
        for (String palabra : arrayPalabras) {
        //int n=mapaOcurrencias.geget(palabra);
        mapaOcurrencias.put(palabra, mapaOcurrencias.getOrDefault(palabra, 0)+1 );
        }*/
        for (String i : arrayPalabras) {
            if (mapaOcurrencias.containsKey(i)) {
                Integer Numero = mapaOcurrencias.get(i) + 1;
                mapaOcurrencias.put(i, Numero);
            } else {
                mapaOcurrencias.put(i, 1);
            }
        }
        return mapaOcurrencias;
    }
}
