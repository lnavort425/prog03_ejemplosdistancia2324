/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut8_xml;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.Document;
import java.io.*;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 *
 * @author luisnavarro
 */
public class DeXMLaDOM {

    public static void main(String[] args) {
        try {
            // 1º Creamos una nueva instancia de un fábrica de constructores de documentos.
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            // 2º A partir de la instancia anterior, fabricamos un constructor de documentos, que procesará el XML.
            DocumentBuilder db = dbf.newDocumentBuilder();
            // 3º Procesamos el documento (almacenado en un archivo) y lo convertimos en un árbol DOM.
            // Document documento = db.parse(ficheros1.Token.RUTA_ENTRADA + "pizzas-schema.xml");
            Document documento = db.parse(ut8_ficheros1.Token.RUTA_ENTRADA + "catalogoCDs.xml");//pizzas-schema.xml");
            System.out.println(documento.toString());
            System.out.println("He leido el xml y transformado a doc, veo los elementos del doc.....");
            recorreArbolDom(documento);
            eliminarElementosConTag(documento, "formato");
            recorreArbolDom(documento);
            //Añado...
            Element direccionTag = documento.createElement("DIRECCION_ENTREGADA");
            Text direccionTxt = documento.createTextNode("C/Perdida S/N");
            direccionTag.appendChild(direccionTxt);
            documento.getDocumentElement().appendChild(direccionTag);
            recorreArbolDom(documento);
            domAFichero(documento);
        } catch (Exception ex) {
            System.out.println("¡Error! No se ha podido cargar el documento XML." + ex);
        }
        //domAFichero(documento);
    }

    public static void eliminarElementosConTag(Document documento, String tag) {
        NodeList listaNodos3 = documento.getElementsByTagName(tag);
        for (int i = 0; i < listaNodos3.getLength(); i++) {
            Element elemento = (Element) listaNodos3.item(i);
            Element padre = (Element) elemento.getParentNode();
            padre.removeChild(elemento);
        }
    }

    public static void domAFichero(Document doc) {
        try {
            // 1º Creamos una instancia de la clase File para acceder al archivo donde guardaremos el XML.
            File f = new File(ut8_ficheros1.Token.RUTA_SALIDA + "res2.xml");
            //2º Creamos una nueva instancia del transformador a través de la fábrica de transformadores.
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            //3º Establecemos algunas opciones de salida, como por ejemplo, la codificación de salida.
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            //4º Creamos el StreamResult, que intermediará entre el transformador y el archivo de destino.
            StreamResult result = new StreamResult(f);
            //5º Creamos el DOMSource, que intermediará entre el transformador y el árbol DOM.
            DOMSource source = new DOMSource(doc);
            //6º Realizamos la transformación.
            transformer.transform(source, result);
        } catch (TransformerException ex) {
            System.out.println("¡Error! No se ha podido llevar a cabo la transformación.");
        }
    }

    public static void recorreArbolDom(Document doc) {
        NodeList listaNodos = doc.getDocumentElement().getChildNodes();
        for (int i = 0; i < listaNodos.getLength(); i++) {
            Node nodo = listaNodos.item(i);
            switch (nodo.getNodeType()) {
                case Node.ELEMENT_NODE:
                    Element elemento = (Element) nodo;
                    System.out.println("Etiqueta:" + elemento.getTagName());
                    System.out.println("Etiqueta:" + nodo.getTextContent());
                    break;
                case Node.TEXT_NODE:
                    Text texto = (Text) nodo;
                    System.out.println("Texto:" + texto.getWholeText());//+":"+texto.getNodeValue()+":"+texto.getTextContent());
                    break;
            }
        }
    }
}
