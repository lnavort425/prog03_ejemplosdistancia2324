CREATE TABLE IF NOT EXISTS ALUMNO(
    ID BIGINT NOT NULL PRIMARY KEY auto_increment,
    NOMBRE VARCHAR(200) NOT NULL,
    APELLIDOS VARCHAR(200) NOT NULL,
    SEXO VARCHAR(1) NOT NULL --H para hombre M para mujer
);                    

INSERT INTO ALUMNO(ID, NOMBRE, APELLIDOS, SEXO) VALUES
(1, 'Pepe', 'Perez', 'H'),
(2, 'Antonio', 'López', 'H'),
(3, 'María', 'Castaño', 'M'),
(4, 'Marta', 'García', 'M');
         
         