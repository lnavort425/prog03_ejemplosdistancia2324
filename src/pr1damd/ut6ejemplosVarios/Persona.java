/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pr1damd.ut6ejemplosVarios;

import java.text.SimpleDateFormat;
import java.time.LocalDate;

/**
 * Clase abastracta Persona Clase que representa a una persona. No es
 * instanciable.
 */
public abstract class Persona implements Imprimible {

    protected String nombre;
    protected String apellidos;
    protected String poblacion;
    protected LocalDate fechaNacimiento;

    // Constructores
    // -------------
    /**
     * Constructor de la clase Persona
     *
     * @param nombre Nombre de la persona
     * @param apellidos Apellidos de la persona
     * @param fechaNacimiento Fecha de nacimiento de la persona
     */
    public Persona(String nombre, String apellidos,
            LocalDate fechaNacimiento) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     * Getter del atributo nombre
     *
     * @return Nombre de la persona
     */
    protected String getNombre() {
        return nombre;
    }

    /**
     * Getter del atributo apellidos
     *
     * @return Apellidos de la persona
     */
    protected String getApellidos() {
        return apellidos;
    }

    /**
     * Getter de la fecha de nacimiento
     *
     * @return Fecha de nacimiento de la persona
     */
    protected LocalDate getFechaNacimiento() {
        return this.fechaNacimiento;
    }

    /**
     * Setter del nombre
     *
     * @param nombre Nombre de la persona
     */
    protected void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Setter de los apellidos
     *
     * @param apellidos Apellidos de la persona
     */
    protected void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * ¨Setter de la fecha de nacimiento
     *
     * @param fechaNacim Fecha de nacimiento de la persona
     */
    protected void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     * Representación en forma de String del contenido del objeto Persona
     *
     * @return String que representa el contenido del objeto Persona
     */

    @Override
    public String toString() {
        StringBuilder resultado = new StringBuilder();
        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        String Stringfecha = formatoFecha.format(this.fechaNacimiento.atStartOfDay());
        System.out.println("");
        resultado.append("Nombre: ").append(this.nombre);
    resultado.append("Apellidos: ").append(this.apellidos);
    resultado.append("Fecha de nacimiento: ").append(Stringfecha);
    return resultado.toString();
    }
    /**
 * Declaración del método abstracto saludar de la clase Persona.
 * Este método en realidad no es instanciable pues aquí no se implementa.
 * @return String que representaría el saludo de una Persona
 */
//protected abstract String saludar (); 
public int getNumAtributos () {
    // La clase persona tiene tres atributos
    int resultado= 3;
    return resultado;
} 
public String[] getNombreAtributos() {
     String[] resultado= new String [3];
     // Escribimos el nombre de los tres atributos en las posiciones del array
     resultado[0]= "nombre";
     resultado[1]= "apellidos";
     resultado[2]= "poblacion";
     return resultado;
}
public String[] getContenidoArray() {
     String[] resultado= new String[3] ;
     // Escribimos el contenido de los tres atributos en las posiciones del array
     resultado[0]= this.nombre ;
     resultado[1]= this.apellidos ;
     resultado[2]= this.poblacion ;
     return resultado ;
} 

public String getContenidoString() {
     String[] arrayNombres = this.getNombreAtributos() ;
     String[] arrayContenidos = this.getContenidoArray() ;
     int numAtributos = this.getNumAtributos() ;
     StringBuilder resultado = new StringBuilder();

     // Llave de inicio
     resultado.append("{") ;
     // Grueso de los elementos de la lista (todos menos el último)
     int i ;
     for (i=0; i<numAtributos-1; i++) {
          resultado.append((arrayNombres[i] + "=" + arrayContenidos[i] + ", "));
     }

     // Último elemento (sin coma al final)
     resultado.append((arrayNombres[numAtributos-1] + "=" + arrayContenidos[numAtributos-1]));
     // Llave de de fin        
     resultado.append("}") ;

     return resultado.toString();
} 

}
