/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package pr1damd.ut6ejemplosVarios;

/**
 *
 * @author luisnavarro
 */
public interface  Imprimible {
    int         getNumAtributos ();
    String[]    getNombreAtributos ();
    String[]    getContenidoArray (); 
    String      getContenidoString ();
}