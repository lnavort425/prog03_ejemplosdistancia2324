/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pr1damd.ut6ejemplosVarios;

import java.time.LocalDate;

/**
 * Clase que representa a un alumno. Hereda de la clase Persona.
 */
public class Alumno extends Persona {

    protected String grupo;
    protected double notaMedia;

    // Constructor
    // -----------
    /**
     * Constructor de la clase Alumno
     *
     * @param nombre Nombre del alumno
     * @param apellidos Apellidos del alumno
     * @param fechaNacimiento Fecha de nacimiento del alumno
     * @param grupo Grupo al que pertenece el alumno
     * @param notaMedia Nota media del alumno
     */
    public Alumno(String nombre, String apellidos,
            LocalDate fechaNacimiento, String grupo, double notaMedia) {
        super(nombre, apellidos, fechaNacimiento);
        this.grupo = grupo;
        this.notaMedia = notaMedia;
    }

    /**
     * Getter del grupo
     *
     * @return Grupo al que pertenece el alumno
     */
    public String getGrupo() {
        return grupo;
    }

    /**
     * Getter de la nota media
     *
     * @return Nota Nota media del alumno
     */
    public double getNotaMedia() {
        return notaMedia;
    }

    /**
     * Setter del grupo
     *
     * @param grupo Grupo al que pertenece el alumno
     */
    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    /**
     * Setter de la nota media
     *
     * @param notaMedia Nota media del alumno
     */
    public void setNotaMedia(double notaMedia) {
        this.notaMedia = notaMedia;
    }

    /**
     * Representación en forma de String del contenido del objeto Alumno
     * Aprovecha el método toString de la clase Persona mediante una llamada a
     * super.toString(). Es decir, se está ampliando la funcionalidad de la
     * clase Persona.
     *
     * @return
     */
    @Override
    public String toString() {
        StringBuilder resultado;

        // Llamada al método “toString” de la superclase
        resultado = new StringBuilder(super.toString());

        // A continuación añadimos la información “especializada” de esta subclase
        resultado.append("Grupo: ").append(this.grupo);
        resultado.append("Nota media: ").append(String.format("%6.2f", this.notaMedia));

        return resultado.toString();
    }
    
    /**
 * Redefinición del método abstracto saludar de la clase Persona
 * en la clase Alumno
 * @return String que representa el saludo de un Alumno
 */
public String saludar () {
    StringBuilder resultado= new StringBuilder ();
    resultado.append("Hola, soy el alumno ").append(this.nombre).append(" ");
    resultado.append(this.apellidos).append(" y estoy en el grupo ").append(this.grupo);

    return resultado.toString();
}
 public int getNumAtributos () {
   // La clase Alumno tiene dos atributos más los que tenga Persona
   int resultado= super.getNumAtributos() + 2 ;
   return resultado ;  
}
 public String[] getNombreAtributos () {
     String[] resultado = new String [this.getNumAtributos()] ;
     String[] arrayNombresAtributosPersona = super.getNombreAtributos() ;
     // Incluimos primero los nombres de los atributos de Persona
     int i;
     for (i=0; i<arrayNombresAtributosPersona.length; i++)
        resultado[i]= arrayNombresAtributosPersona[i];
     // Incluimos a continuación los nombres de los atributos específicos de Alumno
     resultado[i]= "grupo";
     resultado[i+1]= "notaMedia";

     return resultado;
    }


    public String[] getContenidoArray () {
      String[] resultado= new String [this.getNumAtributos()];
      String[] arrayAtributosPersona= super.getContenidoArray();
      // Incluimos primero los atributos de Persona
      int i;
      for (i=0; i<arrayAtributosPersona.length; i++)
         resultado[i]= arrayAtributosPersona[i];
      // Incluimos a continuación los atributos específicos de Alumno
      resultado[i]= this.grupo ;
      resultado[i+1]= Double.toString(this.notaMedia) ;

      return resultado ;
}  
    
}
