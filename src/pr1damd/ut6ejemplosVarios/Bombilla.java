/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pr1damd.ut6ejemplosVarios;

/**
 *
 * @author luisnavarro
 */
public final class Bombilla extends Dispositivo {

    // Atributos públicos constantes (rangos y valores por omisión)
    public static final int MIN_INTENSIDAD = 0;
    public static final int MAX_INTENSIDAD = 10;

    // Atributos de estado (variables)
    private int intensidad;         // Intensidad actual de la bombilla
    private int numVecesManipulada; // Número de veces que ha sido manipulada (encendida o apagada) de manera "efectiva"
//En cuanto al constructor, podría quedar así:

public Bombilla(String descripcion, int ubicacion) throws IllegalArgumentException {
    super(descripcion, ubicacion);
    this.intensidad = Bombilla.MIN_INTENSIDAD;
    this.numVecesManipulada = 0;
}
public Bombilla(String descripcion, int ubicacion, int intensidadInicial) throws IllegalArgumentException {
    super(descripcion, ubicacion); // Primero llamamos al constructor de la superclase para realizar las comprobaciones e inicializaciones genéricas
    // A continuación, llevamos a cabo las comprobaciones específicas de esta clase
    if (intensidadInicial<Bombilla.MIN_INTENSIDAD || intensidadInicial>Bombilla.MAX_INTENSIDAD) {
        throw new IllegalArgumentException (String.format("Intensidad inicial no válida: %d", intensidadInicial));
    }
    // Si todo ha ido bien, entonces llevamos a cabo las asignaciones específicas    
    this.intensidad= intensidadInicial;
    this.numVecesManipulada = 0;
}
}