/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pr1damd.ut6ejemplosVarios;

/**
 *
 * @author luisnavarro
 */
public class PruebaBombilla {

    public static void main(String[] args) {
        int[] valoresPrueba = {-1, 0, 2, 12, 15, -1, 7, -5, 0, 9};
        for (int intensidad : valoresPrueba) {
            try {
                Bombilla b = new Bombilla("b1", 5, intensidad);
                System.out.printf("Bombilla creada correctamente: %s\n", b.toString());
            } catch (IllegalArgumentException ex) {
                System.out.printf("Error: %s\n", ex.getMessage());
            }
        }
    }
}
