/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pr1damd.ut6ejemplosVarios;

/**
 *
 * @author luisnavarro
 */
public abstract class Dispositivo {
    
    // Atributos estáticos constantes y públicos ("configuración" de la clase)
    public static final int MIN_UBICACION = 1; // Mínima ubicación
    public static final int MAX_UBICACION = 10; // Máxima ubicación

    // Atributos estáticos variables ("estado" de la clase)
    private static int nextId = 1;   // Contador de identificadores

    // Atributos de objeto constantes 
    private final int id;
    private final String descripcion;

    // Atributos de objeto variables ("estado" del objeto)
    private int ubicacion;
//Respecto al constructor, podría quedar así:

public Dispositivo(String descripción, int ubicacion) throws IllegalArgumentException {
    if (ubicacion < Dispositivo.MIN_UBICACION || ubicacion > Dispositivo.MAX_UBICACION) {
        throw new IllegalArgumentException (String.format("Ubicación no válida: %d", ubicacion));
    } else {
        this.id = Dispositivo.nextId++;
        this.descripcion = descripción;
        this.ubicacion = ubicacion;
    }
}
}