/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pr1damd.ut6ejemplosVarios;

import java.time.LocalDate;

/**
 *
 * @author luisnavarro
 */
public class EjecutaEjemplos {
    public static void main(String[] args) {
        // Declaración de objetos
Alumno alumno;
Profesor profesor;
 
// Creación de objetos (llamada a constructores)
alumno= new Alumno ("Juan", "Torres", LocalDate.parse ("1995-03-22"), "1DAM-B", 7.5);
profesor= new Profesor  ("Antonio", "Campos", LocalDate.parse ("1975-08-16"), "Electricidad", 2000);
 
// Utilización del método saludar
System.out.println (alumno.saludar());
System.out.println (profesor.saludar());


//----------otro
   
        // Creación de objetos alumno y profesor
        Alumno alumno1= new Alumno ("Juan", "Torres Mula", LocalDate.parse ("1995-08-16"), "1DAM-B", 7.5);
        Profesor profesor1= new Profesor("Antonio", "Campos Pin", LocalDate.parse ("1975-08-16"), "Informática", 2000);
      
        // Obtención del contenido del objeto alumno a través de los métodos del interfaz Imprimible
        int numAtributosAlumno= alumno1.getNumAtributos();
        String[] nombreAtributosAlumno= alumno1.getNombreAtributos();
        String[] contenidoAtributosAlumno= alumno1.getContenidoArray();
        String stringContenidoAlumno= alumno1.getContenidoString();
        
        // Obtención del contenido del objeto profesor a través de los métodos del interfaz Imprimible
        int numAtributosProfesor= profesor1.getNumAtributos();
        String[] nombreAtributosProfesor= profesor1.getNombreAtributos();
        String[] contenidoAtributosProfesor= profesor1.getContenidoArray();
        String stringContenidoProfesor= profesor1.getContenidoString();
                
        // Impresión en pantalla del contenido del objeto alumno a través de las estructuras obtenidas
        System.out.printf ("Contenido del objeto alumno: %s\n", stringContenidoAlumno);
        
        // Impresión en pantalla del contenido del objeto alumno a través de las estructuras obtenidas
        System.out.printf ("Contenido del objeto profesor: %s\n", stringContenidoProfesor);

        
    }
}
