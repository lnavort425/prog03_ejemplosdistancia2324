/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pr1damd.ut6ejemplosVarios;

/**
 *
 * @author luisnavarro
 */
public class CuentaCorrienteV1 {
    private double saldo;
    private int id=0;
    private static int numeroCuentas=0;
    private String titular;
    public CuentaCorrienteV1(){
        titular="desconocido";
        saldo=0;
    }
    public CuentaCorrienteV1(String titular, double saldo){
        titular=titular;
        this.saldo=saldo;
        this.id=++numeroCuentas;
    }
    public CuentaCorrienteV1(double saldo) {
 
        this("desconocido", saldo);
        //this.id=++numeroCuentas;

    }
    public void ingresa(double i)
    {
        saldo+=i;
    }
    public void extrae(double e) throws IllegalArgumentException
    {
        if (e>saldo) throw new IllegalArgumentException("Se ha intentado extraer más del saldo que hay.");
        saldo-=e;
    }
    public void tranferir(CuentaCorrienteV1 destino, double cantidad) throws IllegalArgumentException
    {
        this.extrae(cantidad);
        //saldo-=cantidad;
        destino.ingresa(cantidad);
    }

    /**
     * @return the saldo
     */
    public double getSaldo() {
        return saldo;
    }
    
    /**
     * @return the id
     */
    public  int getId() {
        return id;
    }

    /**
     * @return the titular
     */
    public  String getTitular() {
        return titular;
    }

    /**
     * @param aTitular the titular to set
     */
    protected  void setTitular(String aTitular) {
        titular = aTitular;
    }
    
}
