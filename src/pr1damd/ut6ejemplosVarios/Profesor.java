/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pr1damd.ut6ejemplosVarios;

import java.time.LocalDate;

/**
 * Clase Profesor
 * Clase que contiene los atributos que representan a un profesor
 */
public class Profesor extends Persona {
    String especialidad;
    double salario; 

    // Constructor
    // -----------
    
    /**
     * Constructor de la clase Persona
     * @param nombre            Nombre del profesor
     * @param apellidos         Apellidos del profesor
     * @param fechaNacimiento   Fecha de nacimiento del profesor
     * @param especialidad      Especialidad del profesor
     * @param salario           Salario del profesor
     */
    public Profesor (String nombre, String apellidos, 
            LocalDate fechaNacimiento, String especialidad, double salario) {
        super (nombre, apellidos, fechaNacimiento);
        this.especialidad= especialidad;
        this.salario= salario;            
    }


    /** 
     * Getter de la especialidad del profesor
     * @return Especialidad del profesor
     */
    public String getEspecialidad (){
        return especialidad;
    }

    /**
     * Getter del salario del profesor
     * @return Salario del profesor
     */
    public double getSalario (){
        return salario;
    }

    /**
     * Setter del salario del profesor
     * @param salario Salario del profesor
     */
    public void setSalario (double salario){
        this.salario= salario;
    }

    /**
     * Setter de la especialidad del profesor
     * @param especialidad Especialidad del profesor
     */
    public void setESpecialidad (String especialidad){
        this.especialidad= especialidad;
    }
    /**
 * Redefinición del método abstracto saludar de la clase Persona
 * en la clase Profesor
 * @return String que representa el saludo de un Profesor
 */
public String saludar () {
    StringBuilder resultado= new StringBuilder ();
    resultado.append("Hola, mi nombre es profesor ").append(this.nombre).append(" ");
    resultado.append(this.apellidos).append(" y soy de la especialidad de ").append(this.especialidad);

    return resultado.toString();
}
  // Método getNumAtributos
        @Override
        public int getNumAtributos () {
            // La clase Profesor tiene dos atributos más los que tenga Persona
            int resultado= super.getNumAtributos() + 2;
            return resultado;
        }
        
        // Método getNombresAtributos
        @Override
        public String[] getNombreAtributos () {
            String[] resultado= new String [this.getNumAtributos()];
            String[] arrayNombresAtributosPersona= super.getNombreAtributos();
            // Incluimos primero los nombres de los atributos de Persona
            int i;
            for (i=0; i<arrayNombresAtributosPersona.length; i++)
                resultado[i]= arrayNombresAtributosPersona[i];
            // Incluimos a continuación los nombres de los atributos específicos de Profesor
            resultado[i]= "especialidad";
            resultado[i+1]= "salario";

            return resultado;
        }

        // Método getContenidoArray 
        @Override
        public String[] getContenidoArray () {
            String[] resultado= new String [this.getNumAtributos()];
            String[] arrayAtributosPersona= super.getContenidoArray();
            // Incluimos primero los atributos de Persona
            int i;
            for (i=0; i<arrayAtributosPersona.length; i++)
                resultado[i]= arrayAtributosPersona[i];
            // Incluimos a continuación los atributos específicos de Alumno
            resultado[i]= this.especialidad;
            resultado[i+1]= Double.toString(this.salario);

            return resultado;
        }

        // Método getContenidoString 
        // Este método sería exactamente igual que en su clase padre Persona.
        // No hsría falta "rescribirlo". Podemos "heredarlo" directamente.
        
}
